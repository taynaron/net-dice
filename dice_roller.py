"""
The dice_roller module interprets DnD style dice rolls and returns a history of
how it was evaluated
"""
from random import randint
from inspect import stack
import re
from copy import deepcopy
from math import ceil


# debug_level
N0 = 1 << 0 # normal
T1 = 1 << 1 # token summary
T2 = 1 << 2 # token detail
H1 = 1 << 3 # history
H2 = 1 << 4 # history with crit/fail propagation


# Token Generator, the default type
def token(value, type_, depth):
	"""token implements a factory for creating token dictionaries"""
	out = {}
	out["value"] = value # array, string, int, or float
	out["type"] = type_  # type of token, from the machine key
	out["stack"] = []
	out["depth"] = depth # depth tracking
	return out

# Propagate certain flags from one object to another
# currently supports crit, fail, double
# src     = token to receive values
# mask    = token to send values
# exclude = list of traits to not pass forward
def propagate(src, mask, exclude=None):
	"""propagate implements moving properties from token to token"""
	exclude = exclude if isinstance(exclude, list) else []
	for a in ["crit", "fail", "double"]:
		if a in mask and mask[a] and a not in exclude:
			src[a] = True
	return src

# Defines a step from one state to another
def step(step_name,
				 trigger,
				 cluster=None,
				 can_close=False,
				 depth_mod=0,
				 override=None):
	"""step implements a factory for creating state machine steps"""
	cluster = cluster if isinstance(cluster, list) else []
	out = {}
	out["step"] = step_name      # map of current state
	out["trigger"] = trigger     # possible moves to the current state ("step")
	out["cluster"] = cluster     # append if the current state matches the cluster
	out["override"] = override   # convert the state to another state
	out["can_close"] = can_close # an endable for the equation
	out["depth_mod"] = depth_mod # for (), to set scope for comma focus
	return out

class Tokenizer(object):
	"""
	Tokenizer represents the current state of a state machine and houses that
	machine
	"""
	__machine = {}
	__states = {}
	#  c -> h -> end
	#    -> l -> end
	#    -> r -> i -> t -> end
	#    -> e -> i -> l -> end
	#  d -> end
	#    -> % -> end
	#  e -> x -> end
	#  f -> a -> i -> l -> end
	#    -> l -> o -> o -> r -> end
	#  r -> e -> g -> e -> end
	#            l -> e -> end
	#  m -> a -> x -> end
	#       i -> n -> end

	# The different states and what's an acceptable trigger to that state
	__states["unknown"]        = __unk    = step("", "")
	__states["space"]          = __space  = step("space", " ", can_close=True)
	__states["comma"]          = __comma  = step("comma", ",")
	__states["paren_open"]     = __par_op = step("paren_open", "(", depth_mod=1)
	__states["paren_close"]    = __par_cl = step("paren_close",
																							 ")",
																							 can_close=True, depth_mod=-1)

	__states["digit"]          = __digit  = step("digit",
																							 "0123456789",
																							 ["digit"],
																							 can_close=True)
	__states["period"]         = __period = step("period",
																							 ".",
																							 ["digit"],
																							 override="digit")
	__states["float"]          = __float  = step("float",
																							 "0123456789",
																							 ["period", "float"],
																							 can_close=True,
																							 override="digit")
	__states["negative"]       = __neg    = step("negative", "-")

	__states["variable_open"]  = __var_op = step("variable_open", "[")
	__states["variable"]       = __var    = step("variable",
																							 "abcdefghijklmnopqrstuvwxyz"\
																							 "0123456789 (),+-*/_!@#$%^&"\
																							 "*.\\{}:;\"'=~`",
																							 ["variable"],
																							 can_close=True)
	__states["variable_close"] = __var_cl = step("variable_close",
																							 "]",
																							 can_close=True)
	__states["op"]             = __op     = step("op", "+-%*/")

	__states["lt"]             = __lt     = step("lt", "<")
	__states["le"]             = __le     = step("le", "=", ["lt"])
	__states["gt"]             = __gt     = step("gt", ">")
	__states["ge"]             = __ge     = step("ge", "=", ["gt"])
	__states["eq_"]            = __eq_    = step("eq_", "=")
	__states["eq"]             = __eq     = step("eq", "=", ["eq_"])
	__states["not_"]           = __not_   = step("not_", "!")
	__states["not"]            = __not    = step("not", "=", ["not_"])
	# C tree
	__states["c"]              = __c      = step("c", "c")
	__states["ch"]             = __ch     = step("ch", "h", ["c"])
	__states["cl"]             = __cl     = step("cl", "l", ["c"])
	__states["cr"]             = __cr     = step("cr", "r", ["c"])
	__states["cri"]            = __cri    = step("cri", "i", ["cr"])
	__states["crit"]           = __crit   = step("crit",
																							 "t",
																							 ["cri"],
																							 can_close=True)
	__states["ce"]             = __ce     = step("ce", "e", ["c"])
	__states["cei"]            = __cei    = step("cei", "i", ["ce"])
	__states["ceil"]           = __ceil   = step("ceil",
																							 "l",
																							 ["cei"],
																							 can_close=True)
	# D tree
	__states["d"]              = __d      = step("d", "d", can_close=True)
	__states["d_mod"]          = __d_mod  = step("d_mod",
																							 "%",
																							 ["d"],
																							 can_close=True)
	# E tree
	__states["e"]              = __e      = step("e", "e")
	__states["ex"]             = __ex     = step("ex", "x", ["e"], can_close=True)
	# F tree
	__states["f"]              = __f      = step("f", "f")
	__states["fa"]             = __fa     = step("fa", "a", ["f"])
	__states["fai"]            = __fai    = step("fai", "i", ["fa"])
	__states["fail"]           = __fail   = step("fail",
																							 "l",
																							 ["fai"],
																							 can_close=True)
	__states["fl"]             = __fl     = step("fl", "l", ["f"])
	__states["flo"]            = __flo    = step("flo", "o", ["fl"])
	__states["floo"]           = __floo   = step("floo", "o", ["flo"])
	__states["floor"]          = __floor  = step("floor",
																							 "r",
																							 ["floo"],
																							 can_close=True)
	# R tree
	__states["r"]              = __r      = step("r", "r")
	__states["re"]             = __re     = step("re", "e", ["r"])
	__states["reg"]            = __reg    = step("reg", "g", ["re"])
	__states["rege"]           = __rege   = step("rege",
																							 "e",
																							 ["reg"],
																							 can_close=True)
	__states["rel"]            = __rel    = step("rel", "l", ["re"])
	__states["rele"]           = __rele   = step("rele",
																							 "e",
																							 ["rel"],
																							 can_close=True)
	# M tree
	__states["m"]              = __m      = step("m", "m")
	__states["mi"]             = __mi     = step("mi", "i", ["m"])
	__states["min"]            = __min    = step("min", "n", ["mi"])
	__states["ma"]             = __ma     = step("ma", "a", ["m"])
	__states["max"]            = __max    = step("max", "x", ["ma"])

	# The state machine and what's an acceptable state to go to given your
	# current state Based on the triggers for a given state create specific paths
	# and allowable equations
	__machine["unknown"]        = [__digit, __c, __d, __e, __f, __r, __m, __space, __neg, __var_op, __par_op]
	__machine["negative"]       = [__digit, __space, __par_op, __var_op]
	__machine["digit"]          = [__digit, __c, __d, __e, __f, __r, __m, __space, __op, __par_cl, __comma, __var_op, __period, __lt, __gt, __eq_, __not_]
	__machine["period"]         = [__float]
	__machine["float"]          = [__float, __c, __d, __e, __f, __r, __m, __space, __op, __par_cl, __comma, __var_op, __lt, __gt, __eq_, __not_]
	__machine["space"]          = [__digit, __c, __d, __e, __f, __r, __m, __space, __op, __par_op, __par_cl, __comma, __var_op, __neg, __lt, __gt, __eq_, __not_]
	__machine["comma"]          = [__digit, __c, __d, __e, __f, __r, __m, __space, __par_op, __var_op, __neg]

	__machine["op"]             = [__digit, __c, __d, __e, __f, __r, __m, __space, __neg, __par_op, __var_op]
	__machine["lt"]             = [__digit, __c, __d, __e, __f, __r, __m, __space, __neg, __par_op, __var_op, __le]
	__machine["le"]             = [__digit, __c, __d, __e, __f, __r, __m, __space, __neg, __par_op, __var_op]
	__machine["gt"]             = [__digit, __c, __d, __e, __f, __r, __m, __space, __neg, __par_op, __var_op, __ge]
	__machine["ge"]             = [__digit, __c, __d, __e, __f, __r, __m, __space, __neg, __par_op, __var_op]
	__machine["eq_"]            = [__eq ]
	__machine["eq"]             = [__digit, __c, __d, __e, __f, __r, __m, __space, __neg, __par_op, __var_op]
	__machine["not_"]           = [__not ]
	__machine["not"]            = [__digit, __c, __d, __e, __f, __r, __m, __space, __neg, __par_op, __var_op]

	__machine["c"]              = [__ch, __cl, __cr, __ce ]
	__machine["ch"]             = [__c, __digit, __space, __op, __par_op, __comma, __var_op, __lt, __gt, __eq_, __not_]
	__machine["cl"]             = [__c, __digit, __space, __op, __par_op, __comma, __var_op, __lt, __gt, __eq_, __not_]
	__machine["cr"]             = [__cri ]
	__machine["cri"]            = [__crit ]
	__machine["crit"]           = [__f, __digit, __space, __op, __par_op, __comma, __var_op, __lt, __gt, __eq_, __not_]
	__machine["ce"]             = [__cei ]
	__machine["cei"]            = [__ceil ]
	__machine["ceil"]           = [__digit, __space, __op, __par_op, __par_cl, __comma, __var_op, __lt, __gt, __eq_, __not_]
	__machine["d"]              = [__d, __d_mod, __e, __c, __f, __digit, __space, __op, __par_op, __par_cl, __comma, __var_op, __lt, __gt, __eq_, __not_]
	__machine["d_mod"]          = [__c, __f, __space, __op, __par_op, __par_cl, __comma, __var_op, __lt, __gt, __eq_, __not_]
	__machine["e"]              = [__ex ]
	__machine["ex"]             = [__digit, __space, __op, __par_op, __par_cl, __comma, __var_op, __lt, __gt, __eq_, __not_]
	__machine["f"]              = [__fa, __fl ]
	__machine["fa"]             = [__fai ]
	__machine["fai"]            = [__fail ]
	__machine["fail"]           = [__digit, __space, __op, __par_op, __par_cl, __comma, __var_op, __lt, __gt, __eq_, __not_]
	__machine["fl"]             = [__flo ]
	__machine["flo"]            = [__floo ]
	__machine["floo"]           = [__floor ]
	__machine["floor"]          = [__digit, __space, __op, __par_op, __par_cl, __comma, __var_op, __lt, __gt, __eq_, __not_]
	__machine["r"]              = [__re ]
	__machine["re"]             = [__reg, __rel ]
	__machine["reg"]            = [__rege ]
	__machine["rege"]           = [__digit, __space, __op, __par_op, __par_cl, __comma, __var_op, __lt, __gt, __eq_, __not_]
	__machine["rel"]            = [__rele ]
	__machine["rele"]           = [__digit, __space, __op, __par_op, __par_cl, __comma, __var_op, __lt, __gt, __eq_, __not_]
	__machine["m"]              = [__mi, __ma ]
	__machine["mi"]             = [__min ]
	__machine["min"]            = [__digit, __space, __op, __par_op, __par_cl, __comma, __var_op, __lt, __gt, __eq_, __not_]
	__machine["ma"]             = [__max ]
	__machine["max"]            = [__digit, __space, __op, __par_op, __par_cl, __comma, __var_op, __lt, __gt, __eq_, __not_]

	__machine["paren_open"]     = [__digit, __c, __d, __e, __f, __r, __m, __space, __neg, __var_op, __par_op]
	__machine["paren_close"]    = [__c, __d, __e, __f, __r, __m, __space, __op, __comma, __var_op, __par_cl, __lt, __gt, __eq_, __not_]

	__machine["variable_open"]  = [ __var ]
	__machine["variable"]       = [ __var, __var_cl ]
	__machine["variable_close"] = [ __digit, __c, __d, __e, __f, __r, __m, __space, __op, __var_op, __par_cl, __comma, __lt, __gt, __eq_, __not_]


	def __init__(self, debug_level=N0):
		"""Creates an initial state"""
		# Need to be able to track the state as you walk through an equation
		self.state = "unknown"
		# Tokens that require multiple characters you need a place to temporarily
		# store the partial value
		self.value = None
		# Output variable
		self.parsed = []
		# Tracks the depth of expressions inside (), only needed to correctly comma
		# expand
		self.depth = 0
		self.debug_level = debug_level


	# Take the equation one character at a time
	#
	# The gist of the algorithm is to record a token when the state changes from
	# the previous state note that you need to do a final check outside this
	# function to grab the last character and perform some validation
	#
	# State is the previous state and feed is the new state
	def _handle(self, index, feed):
		"""_handle implements evaluating what to do with a token"""
		for a in self.__machine[self.state]:
			if str.lower(feed) in a["trigger"]:
				if str.lower(feed) == self.__space["trigger"] and self.state != "variable":
					break
				if self.state == "unknown": # Starter
					self.value = feed
				elif self.state in a["cluster"]: # Continue existing token
					self.value = self.value + feed
				else: # Changing tokens
					if self.value == ".":
						toss("'{}' at character {}".format(feed, index))
					self_state = self.__states[self.state]["override"]
					state = self.state if self_state == None else self_state
					self.parsed.append(token(self.value, state, self.depth))
					self.value = feed

				# Modify the depth
				if a["step"] == "paren_open":
					self.depth += self.__states[a["step"]]["depth_mod"]
				elif self.state == "paren_close":
					self.depth += self.__states[self.state]["depth_mod"]
				# Update the state
				self.state = a["step"]

				break # the for loop
		else:
			toss("'{}' at character {}".format(feed, index))


	def validate(self, depth):
		"""
		validate implements processing the final results to see if it is valid
		"""
		if self.depth != depth:
			toss("depth issue {} != {}".format(self.depth, depth))
		can_close = not self.__states[self.state]["can_close"]
		if can_close or self.parsed[-1]["value"] == ".":
			toss("can't end on '{}'".format(self.parsed[-1]["value"]))


	def tokenize_helper(a):
		return "{{:{}}}".format(len(str(a["value"]))).format(a["depth"])


	def Tokenize(self, equation, depth=0):
		"""Tokenize implements evaluating the given equation"""
		self.depth = depth
		for i, a in enumerate(equation):
			self._handle(i, a)
		if self.value != None:
			self.parsed.append(token(self.value, self.state if self.__states[self.state]["override"] == None else self.__states[self.state]["override"], self.depth))
			self.depth += self.__states[self.state]["depth_mod"]

		if self.debug_level & T2:
			print(" ".join([a["value"] for a in self.parsed]))
			print(" ".join([tokenize_helper(a) for a in self.parsed]))

		whitelist = ["variable_open", "variable_close", "space"]
		result = [a for a in self.parsed if (not (a["type"] in whitelist))]
		self.validate(depth)
		return result


def make_inc_if_dirty(func):
	"""make_inc_if_dirty flags the function to step forward in the tokens"""
	func.incIfDirty = True
	return func


def make_not_save(func):
	"""makeNotSave flags the function to not save any results to history"""
	func.doNotSave = True
	return func


class Interpreter(object):
	"""Interpreter represents the equations state and history"""

	def __init__(self, tokens, variables, debug_level=N0):
		"""creates a new interpreter"""
		self.tokens     = tokens     # Local copies of the parameters for the equation
		self._variables = variables
		self.history    = []         # Output variable
		self.debug_level = debug_level
		self.find_comma


	def __pick_one(self, left, right, func):
		"""__pick_one implements propagating a value from one token or another"""
		if func(left, right):
			result = token(float(left["value"]), "digit", left["depth"])
			result = propagate(result, left)
			return result
		else:
			result = token(float(right["value"]), "digit", left["depth"])
			result = propagate(result, right)
			return result


	def float_to_int(self, val):
		"""float_to_int implements converting a token value to a float"""
		return int(float(val))


	def is_float(self, x):
		"""is_float implements determining if a token value is a float"""
		return isinstance(x, float) or (isinstance(x, str) and "." in x)


	def is_sig_float(self, x):
		"""
		is_sig_float implements determing if the float representation has any values
		to the right of the decimal
		"""
		return self.is_float(x) and float(x) != float(int(float(x)))


	def __get_type(self, x):
		"""__get_type implements getting the type of the token"""
		return x["type"]


	def get_pattern(self, i, pattern):
		"""
		get_pattern implements checking whether the current tokens are in a
		particular sequence
		"""
		if 0 <= i <= i + len(pattern) <= len(self.tokens):
			test_pattern = [self.__get_type(a) for a in self.tokens[i: i + len(pattern)]]
			return test_pattern == pattern
		return False


	def breakout(self, i, pattern, default_x=None, default_y=None):
		"""
		breakout implements taking a pattern and returning all the tokens in the
		pattern
		"""
		adjust_l = 0
		if (i < 0 or (i >= 0 and self.tokens[i]["type"] not in ["digit", "array", "paren_close"])) and default_x is not None:
			adjust_l = 1
		adjust_r = 0
		if (i + len(pattern) - 1 >= len(self.tokens) or (i + len(pattern) - 1 < len(self.tokens) and self.tokens[i + len(pattern) - 1]["type"] not in ["digit", "paren_open"])) and default_y is not None:
			adjust_r = 1

		break_l, break_r = i + adjust_l, i + len(pattern) - adjust_r
		l, m, r = self.tokens[0 : break_l], self.tokens[break_l : break_r], self.tokens[break_r : ]

		if adjust_l > 0:
			m = [default_x] + m
		if adjust_r > 0:
			m = m + [default_y]
		return l, m, r


	@make_not_save
	def handler_purge_identities(self, i, v):
		"""
		handler_purge_identities implements removing noop portions of the equations
		"""
		if self.get_pattern(i - 1, ["digit", "op", "digit"]):
			[x, opr, y] = self.tokens[i - 1: i + 2]
			if opr["value"] == "+" and (float(x["value"]) == 0 or float(y["value"]) == 0):
				return self._add(i, v)
			if opr["value"] == "-" and float(y["value"]) == 0:
				return self._sub(i, v)
			if opr["value"] == "*" and (float(x["value"]) == 1 or float(y["value"]) == 1):
				return self._mul(i, v)
			if opr["value"] == "/" and float(y["value"]) == 1:
				return self._div(i, v)
		return False

	def handler_comma_expansions(self, i, v):
		"""
		handler_comma_expansions implements fanning out the equation so that commas
		are at the top level
		"""
		if self.get_pattern(i, ["comma"]):
			return self.handle_comma_expansions(i, v)
		return False

	#One
	@make_inc_if_dirty
	def handler_variable_substitution(self, i, v):
		"""
		handler_variable_substitution implements swapping variables with their
		associated equation
		"""
		dirty = False
		if self.get_pattern(i, ["variable"]):
			left, [num0], right = self.breakout(i, ["variable"])
			tokenizer = Tokenizer(self.debug_level)
			middle = tokenizer.Tokenize(str(self._variables[num0["value"]]), num0["depth"] + 1)
			for middle_i, middle_v in enumerate(middle):
				if middle[middle_i]["type"] == "variable" and middle[middle_i]["value"] in v["stack"]:
					break
				elif middle_v["type"] == "variable":
					middle[middle_i]["stack"] = num0["stack"] + [num0["value"]]
			else:
				self.tokens = left + [token("(", "paren_open", num0["depth"] + 1)] + middle + [token(")", "paren_close", num0["depth"] + 1)] + right
				dirty = True
		return dirty

	#One
	def handler_standalone_arrays(self, i, v):
		"""
		handler_standalone_arrays implements evaluating an array to a single
		interpreted number
		"""
		if self.get_pattern(i, ["array"]):
			left, [num0], right = self.breakout(i, ["array"])
			middle = token(str(sum(num0["value"])), "digit", v["depth"])
			middle = propagate(middle, v)
			self.tokens = left + [middle] + right
			return True
		return False

	#Two
	def handler_negative_sign(self, i, v):
		"""
		handler_negative_sign implements converting a separate sign and number
		token to single negative number
		"""
		if self.get_pattern(i, ["negative", "digit"]):
			left, [_, num0], right = self.breakout(i, ["negative", "digit"])
			middle = token(str(-1 * float(num0["value"])), "digit", num0["depth"])
			self.tokens = left + [middle] + right
			return True
		return False

	def prep_d_mod(self, i, v):
		"""prep_d_mod implements converting floats to ints for DMod operations"""
		if self.get_pattern(i, ["d_mod"]):
			left, [x, d_mod], right = self.breakout(i - 1, ["x", "d_mod"], token("1", "digit", v["depth"]))
			if self.is_sig_float(x["value"]):
				middle = token(self.float_to_int(x["value"]), "digit", x["depth"])
				self.tokens = left + [middle, d_mod] + right
				return True
		return False

	#Two #Array
	def handler_d_mod(self, i, v):
		"""handler_d_mod implements solving the d% operator"""
		if self.get_pattern(i, ["d_mod"]):
			left, [x, _], right = self.breakout(i - 1, ["x", "d_mod"], token("1", "digit", v["depth"]))
			middle = token(sorted([randint(0, 99) for _ in range(0, self.float_to_int(x["value"]))]), "array", v["depth"])
			middle["range"] = (0, 99)
			middle["fail"] = middle["value"][0] == 99
			middle["double"] = middle["value"][0] % 11 == 0
			middle["crit"] = middle["value"][0] == 0
			self.tokens = left + [middle] + right
			return True
		return False

	def prep_d(self, i, v):
		"""prep_d implements converting floats to ints for D operations"""
		if self.get_pattern(i, ["d"]):
			left, [x, d, y], right = self.breakout(i - 1, ["x", "d", "y"], token("1", "digit", v["depth"]), token("1", "digit", v["depth"]))
			if self.is_sig_float(x["value"]) or self.is_sig_float(y["value"]):
				mid_x = token(self.float_to_int(x["value"]), "digit", x["depth"])
				mid_y = token(self.float_to_int(y["value"]), "digit", y["depth"])
				self.tokens = left + [mid_x, d, mid_y] + right
				return True
		return False

	#Three #Array #ImplicitY
	def handler_d(self, i, v):
		"""handler_d implements solving the d operator"""
		if self.get_pattern(i, ["d"]):
			left, [x, _, y], right = self.breakout(i - 1, ["x", "d", "y"], token("1", "digit", v["depth"]), token("1", "digit", v["depth"]))
			if x["type"] == "digit" and y["type"] == "digit":
				x = self.float_to_int(x["value"])
				y = self.float_to_int(y["value"])
				middle = token(sorted([randint(1, y) for _ in range(0, x)]), "array", v["depth"])
				middle["range"] = (x, y)
				if middle["range"] == (1, 20):
					middle["fail"] = middle["value"][0] == 1
					middle["crit"] = middle["value"][0] == 20
				self.tokens = left + [middle] + right
				return True
		return False

	def prep_ch(self, i, v):
		"""prep_ch implements converting floats to ints for ch operations"""
		if self.get_pattern(i - 1, ["array", "ch"]):
			left, [x, chcl, y], right = self.breakout(i - 1, ["x", "chcl", "y"], None, token("1", "digit", v["depth"]))
			if self.is_sig_float(y["value"]):
				mid_y = token(self.float_to_int(y["value"]), "digit", y["depth"])
				self.tokens = left + [x, chcl, mid_y] + right
				return True
		return False

	def prep_cl(self, i, v):
		"""prep_cl implements converting floats to ints for cl operations"""
		if self.get_pattern(i - 1, ["array", "cl"]):
			left, [x, chcl, y], right = self.breakout(i - 1, ["x", "chcl", "y"], None, token("1", "digit", v["depth"]))
			if self.is_sig_float(y["value"]):
				mid_y = token(self.float_to_int(y["value"]), "digit", y["depth"])
				self.tokens = left + [x, chcl, mid_y] + right
				return True
		return False

	def handler_ch_cl(self, i, v, rang):
		"""handler_ch_cl implements solving the choose operators"""
		left, [x, _, y], right = self.breakout(i - 1, ["x", "chcl", "y"], None, token("1", "digit", v["depth"]))
		middle = token(rang(x["value"], self.float_to_int(y["value"])), "array", v["depth"])
		middle["range"] = x["range"]
		self.tokens = left + [middle] + right
		return True

	#Three #Array #ImplicitY
	def handler_ch(self, i, v):
		"""handler_ch implements solving the choose high operator"""
		if self.get_pattern(i - 1, ["array", "ch"]):
			return self.handler_ch_cl(i, v, lambda x, y: x[-y:])
		return False

	#Three #Array #ImplicitY
	def handler_cl(self, i, v):
		"""handler_cl implements solving the choose low operator"""
		if self.get_pattern(i - 1, ["array", "cl"]):
			return self.handler_ch_cl(i, v, lambda x, y: x[:y])
		return False

	def prep_crit(self, i, v):
		"""prep_crit implements converting floats to ints for crit operations"""
		if self.get_pattern(i - 1, ["array", "crit"]):
			left, [x, critfail, y], right = self.breakout(i - 1, ["x", "critfail", "y"], None, token(str(self.tokens[i - 1]["range"][1]), "digit", v["depth"]))
			if self.is_sig_float(y):
				mid_y = token(self.float_to_int(y["value"]), "digit", y["depth"])
				self.tokens = left + [x, critfail, mid_y] + right
				return True
		return False

	def prep_fail(self, i, v):
		"""prep_fail implements converting floats to ints for fail operations"""
		if self.get_pattern(i - 1, ["array", "fail"]):
			left, [x, critfail, y], right = self.breakout(i - 1, ["x", "critfail", "y"], None, token(str(1), "digit", v["depth"]))
			if self.is_sig_float(y):
				mid_y = token(self.float_to_int(y["value"]), "digit", y["depth"])
				self.tokens = left + [x, critfail, mid_y] + right
				return True
		return False

	def handler_crit_fail(self, i, v, name, other_name, default, comp):
		"""handler_crit_fail implements solving the critical or failure operator"""
		left, [x, _, y], right = self.breakout(i - 1, ["x", "critfail", "y"], None, token(str(default), "digit", v["depth"]))
		if not (0 < self.float_to_int(y["value"]) <= x["range"][1]):
			raise NameError(stack()[1][3] + ": Crit/Fail range out of bounds")
		middle = token(x["value"], "array", v["depth"])
		if x["range"][0] == 1 or len(x["value"]) == 1:
			middle[name] = comp(int(x["value"][-1]), int(y["value"]))
			if other_name in self.tokens[i - 1]:
				middle[other_name] = x[other_name]
		middle["range"] = x["range"]
		self.tokens = left + [middle] + right
		return True

	#Three #Array #ImplicitY
	def handler_crit(self, i, v):
		"""handler_crit implements solving the critical operator"""
		if self.get_pattern(i - 1, ["array", "crit"]):
			return self.handler_crit_fail(i, v, "crit", "fail", self.tokens[i - 1]["range"][1], lambda x, y: x >= y)
		return False

	#Three #Array #ImplicitY
	def handler_fail(self, i, v):
		"""handler_fail implements solving the failure operator"""
		if self.get_pattern(i - 1, ["array", "fail"]):
			return self.handler_crit_fail(i, v, "fail", "crit", 1, lambda x, y: x <= y)
		return False

	def handler_re(self, i, v, comp, rando):
		"""handler_re implements solving the reroll operators"""
		left, [x, _, y], right = self.breakout(i - 1, ["x", "re", "y"], None, token("1", "digit", v["depth"]))
		for value_i, value_v in enumerate(x["value"]):
			if comp(int(value_v), int(y["value"])):
				x["value"][value_i] = rando(1, int(y["value"]), int(x["range"][1]))
		middle = {"value": sorted(x["value"]), "type": "array", "range": x["range"], "depth": v["depth"]}
		self.tokens = left + [middle] + right
		return True

	#Three #Array #ImplicitY
	def handler_rele(self, i, v):
		"""
		handler_rele implements solving the reroll if less than or equal to
		operators
		"""
		if self.get_pattern(i - 1, ["array", "rele"]):
			return self.handler_re(i, v, lambda x, y: x <= y, lambda a, b, c: randint(min(b + 1, c-1), c))
		return False

	#Three #Array #ImplicitY
	def handler_rege(self, i, v):
		"""
		handler_rege implements solving the reroll if greater than or equal to
		operators
		"""
		if self.get_pattern(i - 1, ["array", "rege"]):
			return self.handler_re(i, v, lambda x, y: x >= y, lambda a, b, c: randint(a, max(b - 1, 1)))
		return False

	def handler_e(self, range_, x, work, y, depth):
		"""handler_e implements solving the explode operator recursively"""
		to_explode = [a for a in work if a >= y]
		if len(to_explode) > 0 and depth < 100:
			new_work = [randint(1, range_[1]) for a in to_explode]
			return self.handler_e(range_, x + work, new_work, y, depth + 1)
		elif len(to_explode) > 0 and depth >= 100:
			# raise NameError(stack()[1][3] + ": Excessive explosions")
			pass
		return x + work

	#Three #Array #ImplicitY
	def handler_explode(self, i, v):
		"""handler_explode implements solving the explode operator"""
		if self.get_pattern(i - 1, ["array", "ex"]):
			left, [x, _, y], right = self.breakout(i - 1, ["x", "ex", "y"], None, token(str(self.tokens[i - 1]["range"][1]), "digit", v["depth"]))
			mid_x = self.handler_e(x["range"], [], x["value"], int(y["value"]), 0)
			middle = {"value": sorted(mid_x), "type": "array", "range": x["range"], "depth": v["depth"]}
			self.tokens = left + [middle] + right
			return True
		return False

	#Three
	@make_not_save
	def handler_number_in_parenthesis(self, i, v):
		"""
		handler_number_in_parenthesis implements solving for a single number in
		parenthesis
		"""
		_ = v
		if ((i - 2 >= 0 and self.tokens[i - 2]["type"] not in ["floor", "ceil"]) or i - 2 < 0) and self.get_pattern(i - 1, ["paren_open", "digit", "paren_close"]):
			left, [_, num0, _], right = self.breakout(i - 1, ["paren_open", "digit", "paren_close"])
			middle = deepcopy(num0)
			middle["depth"] -= 1
			self.tokens = left + [middle] + right
			return True
		return False

	def _op(self, i, v, o):
		"""_op implements solving for built in python operations"""
		if v["value"] == o and self.get_pattern(i - 1, ["digit", "op", "digit"]):
			left, [x, opr, y], right = self.breakout(i - 1, ["digit", "op", "digit"])
			middle = token(eval(str(float(x["value"])) + opr["value"] + str(float(y["value"]))), "digit", v["depth"])
			middle = propagate(middle, x)
			middle = propagate(middle, y)
			self.tokens = left + [middle] + right
			return True
		return False

	#Three
	@make_not_save
	def _add(self, i, v):
		"""_add implements solving for the addition operation"""
		return self._op(i, v, "+")

	#Three
	@make_not_save
	def _sub(self, i, v):
		"""_sub implements solving for the subtraction operation"""
		return self._op(i, v, "-")

	#Three
	@make_not_save
	def _mod(self, i, v):
		"""_mod implements solving for the modulus operation"""
		return self._op(i, v, "%")

	#Three
	@make_not_save
	def _mul(self, i, v):
		"""_mul implements solving for the multiplication operation"""
		return self._op(i, v, "*")

	#Three
	@make_not_save
	def _div(self, i, v):
		"""_div implements solving for the division operation"""
		return self._op(i, v, "/")

	def _comp(self, i, v, type_):
		"""_comp implements solving for the comparison operations"""
		if self.get_pattern(i - 1, ["digit", type_, "digit"]):
			left, [x, opr, y], right = self.breakout(i - 1, ["digit", "op", "digit"])
			middle = token(1 if eval(str(float(x["value"])) + opr["value"] + str(float(y["value"]))) else 0, "digit", v["depth"])
			middle = propagate(middle, x)
			middle = propagate(middle, y)
			self.tokens = left + [middle] + right
			return True
		return False

	#Three
	def _lt(self, i, v):
		"""_lt implements solving for the less than operator"""
		return self._comp(i, v, "lt")

	#Three
	def _le(self, i, v):
		"""_le implements solving for the less than or equal to operator"""
		return self._comp(i, v, "le")

	#Three
	def _gt(self, i, v):
		"""_gt implements solving for the greater than operator"""
		return self._comp(i, v, "gt")

	#Three
	def _ge(self, i, v):
		"""_ge implements solving for the greater than or equal to operator"""
		return self._comp(i, v, "ge")

	#Three
	def _eq(self, i, v):
		"""_eq implements solving for the equal to operator"""
		return self._comp(i, v, "eq")

	#Three
	def _not(self, i, v):
		"""_not implements solving for the not operator"""
		return self._comp(i, v, "not")

	#Four
	def handler_floor(self, i, v):
		"""handler_floor implements solving for the floor operator"""
		if self.get_pattern(i, ["floor", "paren_open", "digit", "paren_close"]):
			left, [_, _, num0, _], right = self.breakout(i, ["floor", "paren_open", "digit", "paren_close"])
			middle = token(self.float_to_int(num0["value"]), "digit", v["depth"])
			middle = propagate(middle, num0)
			self.tokens = left + [middle] + right
			return True
		return False

	#Four
	def handler_ceil(self, i, v):
		"""handler_ceil implements solving for the ceiling operator"""
		if self.get_pattern(i, ["ceil", "paren_open", "digit", "paren_close"]):
			left, [_, _, num0, _], right = self.breakout(i, ["ceil", "paren_open", "digit", "paren_close"])
			middle = token(int(ceil(float(num0["value"]))), "digit", v["depth"])
			middle = propagate(middle, num0)
			self.tokens = left + [middle] + right
			return True
		return False

	#Six
	@make_not_save
	def handler_min_max(self, i, v):
		"""handler_min_max implements solving for the minimum or maximum operator"""
		if self.get_pattern(i, ["max", "paren_open", "digit", "comma", "digit", "paren_close"]) or self.get_pattern(i, ["min", "paren_open", "digit", "comma", "digit", "paren_close"]):
			left, [_, _, num0, _, num1, _], right = self.breakout(i, ["max", "paren_open", "digit", "comma", "digit", "paren_close"])
			if v["value"] == "max":
				middle = self.__pick_one(num0, num1, lambda x, y: float(x["value"]) >= float(y["value"]))
				self.tokens = left + [middle] + right
			else:
				middle = self.__pick_one(num0, num1, lambda x, y: float(x["value"]) <= float(y["value"]))
				self.tokens = left + [middle] + right
			return True
		return False


	# Designed to iterate over the elements looking for a specific item to
	# evaluate
	def whileeach(self, func):
		"""
		whileeach implements iterating over tokens looking for patterns to evaluate
		"""
		dirty = False
		i = 0
		while i < len(self.tokens):
			temp_dirty = func(i, self.tokens[i])
			if temp_dirty and hasattr(func, 'incIfDirty'):
				i += 1
			dirty = temp_dirty or dirty
			i += 1
		if not hasattr(func, 'doNotSave') and self.tokens != self.history[-1]:
			self.history.append(self.tokens)
		return dirty


	# This algorithm loops over the equation handling different cases from left
	# to right and attempting to maximize the number of operations that can be
	# evaluated per pass For the most part, simple math isn't captured
	#
	# Each grouping of handling attempts to solve the equation until it can't
	# anymore
	def scan(self):
		"""
		scan will cycle through the tokens opportunistically solving the equation
		when it can, stopping when nothing is changing
		"""
		self.history.append(self.tokens)
		dirty = False

		while True:
			dirty = False
			for a in [self.handler_purge_identities, self.handler_comma_expansions, self.handler_negative_sign, self.handler_variable_substitution]:
				dirty = dirty or self.whileeach(a)
			if not dirty:
				break

		while True:
			dirty = False or self.whileeach(self.handler_number_in_parenthesis)
			if not dirty:
				break

		# This loops is meant for making passes through the equation since solving
		# the equation can't be handled in one step loops inside are meant to
		# handle the precedence of different operators
		while True:
			dirty = False
			for a in [self.handler_comma_expansions,
								self.handler_negative_sign,
								self.handler_number_in_parenthesis,
								self.handler_min_max]:
				dirty = dirty or self.whileeach(a)
			for a in [self.prep_d,
								self.prep_d_mod,
								self.prep_ch, self.prep_cl, self.prep_crit, self.prep_fail]:
				dirty = dirty or self.whileeach(a)
			for a in [self.handler_d,
								self.handler_d_mod,
								self.handler_ch,
								self.handler_cl,
								self.handler_crit,
								self.handler_fail,
								self.handler_rele,
								self.handler_rege,
								self.handler_explode]:
				dirty = dirty or self.whileeach(a)
			for a in [self._mul, self._div, self._mod, self._add, self._sub]:
				dirty = dirty or self.whileeach(a)
			for a in [self._lt, self._le, self._gt, self._ge, self._eq, self._not]:
				dirty = dirty or self.whileeach(a)
			for a in [self.handler_floor,
								self.handler_ceil,
								self.handler_standalone_arrays]:
				dirty = dirty or self.whileeach(a)
			if not dirty:
				break

		# In DnD, everything is handled with integers in the end, therefore we need
		# to convert all floats into ints
		converted = False
		for i, a in enumerate(self.history[-1]):
			if self.is_float(self.history[-1][i]["value"]):
				new_tokens = token(self.float_to_int(self.tokens[i]["value"]), "digit", self.tokens[i]["depth"])
				new_tokens = propagate(new_tokens, self.tokens[i])
				self.tokens = self.tokens[:i] + [new_tokens] + self.tokens[i+1:]
				converted = True
		if converted:
			self.history.append(self.tokens)
		return self.history

	# The algorithm for comma expansion takes the expression with commas between
	# parenthesis (assuming the deepest level) and then splits equation into 5
	# parts
	# 1 - the equation from beginning to the comma at the 2nd deepest point
	# 2 - the equation from the comma at the 2nd deepest point to the left
	#			parenthesis
	# 3 - the expression in the parenthesis with the deepest commas
	# 4 - the equation from the right parenthesis to the comma at the 2nd deepest
	#			point
	# 5 - the equation from the comma at the 2nd deepest point to the end
	#
	# Example: 1, 2 + (3, 4) + 5, 6 > [1], [2 +], [3, 4], [+ 5], [6]
	# part 1: is only repeated once
	# part 2: is repeated for each comma separated item in part 3
	# part 3: is split by comma then iterated over and concatenated to part 2 and
	#					part 4
	# part 4: is repeated for each comma separated item in part 3
	# part 5: is only repeated once
	def handle_comma_expansions(self, i, v):
		"""
		handle_comma_expansions implements creating an equivalent equation with
		commas not nested in parenthesis
		"""
		if v["depth"] < max([z["depth"] for z in self.tokens if z["type"] == "comma"]):
			return False
		if v["depth"] <= 0:
			return False
		if self.tokens[i - 1]["value"] == ")" and self.tokens[i + 1]["value"] == "(":
			return False
		left, right = self.tokens[0: i], self.tokens[i + 1:]
		leftmost = rightmost = leftcomma = rightcomma = i

		def search(items, index, value, condition):
			"""
			search implements scanning the tokens for a particular set of criteria
			"""
			for i, v in enumerate(items):
				if condition(v, value):
					return i
			return index

		left.reverse()
		for left_i, left_v in enumerate(left):
			if left_v["type"] == "paren_open":
				if left_i + 1 < len(left) and left[left_i + 1]["value"] in ["max", "min", "floor", "ceil"]:
					return False
		leftmost = search(left, i, v, lambda x, y: x["depth"] < y["depth"])
		leftcomma = search(left, i, v, lambda x, y: x["type"] in ["paren_open", "comma"] and x["depth"] == y["depth"] - 1)
		left.reverse()
		rightmost = search(right, len(self.tokens) - i - 1, v, lambda x, y: x["depth"] < y["depth"])
		rightcomma = search(right, len(self.tokens) - 1, v, lambda x, y: x["type"] in ["paren_close", "comma"] and x["depth"] == y["depth"] - 1)

		leftcommon = self.tokens[0: i - leftcomma]
		left = self.tokens[i - leftcomma: i - leftmost]
		middle = self.split_by_comma(self.tokens[i - leftmost + 1: i + rightmost])
		right = self.tokens[i + rightmost + 1: i + rightcomma + 1]
		rightcommon = self.tokens[i + rightcomma + 1:]
		# print("searching away from ',' at", i, ":")
		# print("^ to ,", i - leftcomma, ":", leftcommon)
		# print(", to (", i - leftmost, ":", left)
		# print("( to )", i, ":", middle)
		# print(") to ,", i + rightmost, ":", right)
		# print(", to $", i + rightcomma, ":", rightcommon)
		# print

		buff = []
		buff = buff + leftcommon
		for a in middle:
			buff = buff + left + [token("(", "paren_open", v["depth"])] + a + [token(")", "paren_close", v["depth"])] + right + [token(",", "comma", v["depth"] - 1)]
		buff = buff[:-1] + rightcommon
		# print("".join(map(lambda x: x["value"], buff)))
		# print(buff)

		self.tokens = buff
		return True


	# Since we have custom dicts, we need a custom search function to search on a
	# particular property
	def find_comma(self, to_search):
		"""find_comma implements looking for a comma"""
		for i, v in enumerate(to_search):
			if v["type"] == "comma":
				return i
		return -1


	# Since we have custom dicts, we need to split on a particular property
	def split_by_comma(self, middle):
		"""
		split_by_comma implements taking an array and splitting by tokens that are
		commas
		"""
		index = self.find_comma(middle)
		if index == -1:
			return [middle]
		return [middle[: index]] + self.split_by_comma(middle[index + 1:])


def toss(args):
	"""toss handles raising a new exception"""
	raise NameError(stack()[1][3] + ": ran into bad token: " + str(args))


################################################################################


class DiceRoller(object):
	"""DiceRoller represents the full set of tokenizer and interpreter"""
	def __init__(self, debug_level=N0):
		"""creates a new DiceRoller object"""
		self._contains_square_brackets = re.compile(r'(\[[^\[\]]*\])')
		self.debug_level = debug_level


	# used as a method of reducing the number of widgets needed to solve an
	# equation and lighten the network load
	def getNeededWidgets(self, expr, widgets, callee_stack="|"):
		"""
		getNeededWidgets implements looking up all the widget references in the
		equation
		"""
		compress_dict = {}
		for a in self._contains_square_brackets.findall(expr):
			a_str = str(a)
			compress_dict[a[1: -1]] = widgets[a_str[1: -1]]
			compress_dict.update(self.getNeededWidgets(compress_dict[a_str[1: -1]], widgets, callee_stack + a_str[1: -1] + "|"))
		return compress_dict


	def _append(self, c, keys):
		"""Debug output"""
		return "".join(["->{}".format(key) if key in c and c[key] else "" for key in keys])


	def evalRoll(self, equation="1d20", widgets=None):
		"""Get the results of trying to roll the equation"""
		widgets = widgets if isinstance(widgets, dict) else {}
		tokenizer = Tokenizer(self.debug_level)
		parsed = tokenizer.Tokenize(equation)
		if self.debug_level & T1:
			print(["'{}'->{}".format(a["value"], a["type"]) for a in parsed])
		current_tokens = Interpreter(parsed, widgets, self.debug_level)
		result = current_tokens.scan()
		if self.debug_level & (H1 | H2):
			for b in result:
				if self.debug_level & H1:
					print(" ".join([str(c["value"]) for c in b]))
				elif self.debug_level & H2:
					print(" ".join([(str(c["value"]) + self._append(c, ["crit", "fail", "double"])) for c in b]))
		return result


	def validate(self, tokens):
		"""Post process final result to see if it's only numbers"""
		return all(a["type"] in ["digit", "comma"] for a in tokens[-1])
