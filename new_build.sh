#!/bin/bash

# must put a var with spaces in quotes: "$DB"
# if you want to extend the var: "${DB}"
DB=~/Dropbox\ \(Personal\)/NetDice/

cd
pushd Documents/DnD/New_Rolls
pushd net_dice/Net_Dice
textutil -convert html ui/help_text.rtf
rm -rf build/* dist/*
pyinstaller net_dice.spec

if [[ $1 = "--dist" ]];
then
	pushd dist
	zip -9r net_dice_mac.zip NetDice.app
	mv net_dice_mac.zip "$DB"
	popd
	mv dist/NetDice.app ../
	rm -rf build/* dist/*

	mkdir ../../net_dice_code/
	find -X . -name '*.py' | cpio -p -d ../../net_dice_code/
	find -X . -name '*.rtf' | cpio -p -d ../../net_dice_code/
	find -X . -name '*.html' | cpio -p -d ../../net_dice_code/
	find -X . -name '*.ui' | cpio -p -d ../../net_dice_code/
	find -X . -name '*.spec' | cpio -p -d ../../net_dice_code/
	find -X . -name '*.json' | cpio -p -d ../../net_dice_code/
	rm -r ../../net_dice_code/deprecated

	popd

	zip -9r net_dice_code.zip net_dice_code
	rm -r net_dice_code
	cp "net_dice_code.zip" "$DB"
	mv net_dice_code.zip "old/net_dice_"`date +%Y%m%d`".zip"

	if [[ $2 = "--win" ]];
	then
		mv "${DB}"net_dice_win_tmp.zip "${DB}"net_dice_win.zip
	fi

	open "${DB}"net_dice_versions.txt
fi
