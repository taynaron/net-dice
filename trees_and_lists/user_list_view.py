import os

__QtLib__ = os.environ['QT_API']
if __QtLib__ == 'pyqt':
    from PyQt5.QtCore import pyqtSignal as Signal, Qt
    from PyQt5.QtWidgets import QMenu, QAction, QHeaderView
else:
    from PySide.QtCore import Signal, Qt
    from PySide.QtGui import QMenu, QAction, QHeaderView

from .list_tree import ListView
from .user_list_gm_delegate import GMRadioDelegate




class UserView(ListView):
    removeUsers = Signal(list)

    def __init__(self, parent=None):
        ListView.__init__(self, parent)

        self.setContextMenuPolicy(Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.treeContextMenu)
        self.ui = None

        self.gm_radio_delegate = GMRadioDelegate(self)

        self.gm_col = 2
        self.setItemDelegateForColumn(self.gm_col, self.gm_radio_delegate)
        self.base_model = None


    def getUI(self, UI):
        self.ui = UI
        self._setup_connections()


    def setModel(self, userModel):
        ListView.setModel(self, userModel)

        # it's pretty hackey to set the column widths here
        # but we can't do it before we get the model
        # TODO: come up with a better way later
        head = self.header()
        if __QtLib__ == 'pyqt':
            head.setSectionsMovable(False)
        else:
            head.setMovable(False)
        head.setStretchLastSection(False)
        if __QtLib__ == 'pyqt':
            head.setSectionResizeMode(0, QHeaderView.Stretch)
        else:
            head.setResizeMode(0, QHeaderView.Stretch)

        self.base_model = self.model().sourceModel()
        self.base_model.syncLocalGMPrivs[bool].connect(self.setLocalGMPrivs)

        num_cols = head.count()
        for i in range(1, num_cols):
            if __QtLib__ == 'pyqt':
                head.setSectionResizeMode(i, QHeaderView.ResizeToContents)
            else:
                head.setResizeMode(i, QHeaderView.ResizeToContents)

        self.set_buttons_text()


    def _setup_connections(self):
        self.ui.muteUserButton.clicked.connect(self.mute_users)
        self.ui.kickUserButton.clicked.connect(self.kick_users)


    def _set_num_users(self):
        num_users = self.base_model.rowCount()
        user_str = str(num_users) + (" User" if num_users == 1 else " Users")
        self.ui.usersLabel.setText(user_str)


    def keyPressEvent(self, event):
        key = event.key()
        if key == Qt.Key_M:
            self.mute_users()

        self._set_mute_button_text()


    def mousePressEvent(self, event):
        ListView.mousePressEvent(self, event)
        self.set_buttons_text()


    def set_buttons_text(self):
        self._set_mute_button_text()
        self._set_kick_button_text()


    def setLocalGMPrivs(self, has_privs):
        if has_privs:
            self.ui.kickUserButton.show()
        else:
            self.ui.kickUserButton.hide()


    def _set_mute_button_text(self):
        selected = self._selected_users()
        multi_select = len(selected) > 1
        muted = False

        if selected:
            self.ui.muteUserButton.setEnabled(True)
            if selected[0].data() == self.base_model.local_user.name and multi_select:
                muted = self.base_model.is_muted(selected[1])
            else:
                muted = self.base_model.is_muted(selected[0])
        else:
            self.ui.muteUserButton.setEnabled(False)

        self.ui.muteUserButton.setText(("Unm" if muted else "M")+"ute User"+("s" if multi_select else ""))
        return (muted, multi_select)


    def _set_kick_button_text(self):
        selected = self._selected_users()
        multi_select = len(selected) > 1

        if selected:
            self.ui.kickUserButton.setEnabled(True)
        else:
            self.ui.kickUserButton.setEnabled(False)

        self.ui.kickUserButton.setText("Kick User"+("s" if multi_select else ""))
        return multi_select


    def mute_users(self, selected=None):
        if not selected:
            selected = self._selected_users()

        if selected:
            new_mute = not(self.base_model.is_muted(selected[0]))

            for item in selected:
                try:
                    self.base_model.mute_user(item, new_mute)
                except ValueError as e:
                    self.ui.statusBar.showMessage(e)

        self._set_mute_button_text()


    def _selected_users(self):
        selected = self.selectedIndexes()
        sel_out = []
        for ind in selected:
            if ind.column() == 0:
                sel_out.append(ind)
        return sel_out


    def kick_users(self, selected=None):
        if not selected:
            selected = self._selected_users()

        name_list = []
        gm = self.base_model.get_gm_name()
        serv = self.base_model.get_server_name()

        for item in selected:
            name = item.data()  # selected_users only returns name column indexes
            if name == gm:
                self.ui.statusBar.showMessage("Can't kick GM user.")
            elif name == serv:
                self.ui.statusBar.showMessage("Can't kick Server user.")
            else:
                name_list.append(name)
        self.removeUsers[list].emit(name_list)

        self._set_kick_button_text()
        self._set_mute_button_text()


    def set_gm(self, selected=None):
        if not selected:
            selected = self._selected_users()

        user = selected[0]

        if len(self._selected_users()) != 1 or user.data() == self.base_model.get_gm_name():
            return

        self.base_model.setData(user.sibling(user.row(), self.gm_col), True, role=Qt.CheckStateRole)


    def treeContextMenu(self, pos):
        globalPos = self.mapToGlobal(pos)  # QPoint

        menu = QMenu()
        (muted, multi) = self._set_mute_button_text()
        mute_txt = ("Un&m" if muted else "&M")+"ute Selected"
        mute_action = QAction(mute_txt, self)
        mute_action.setShortcut(Qt.Key_M)
        menu.addAction(mute_action)

        kick_txt = "&Kick Selected"
        kick_action = QAction(kick_txt, self)
        if self.base_model.local_gm_privs():
            menu.addAction(kick_action)

        gm_txt = "Grant &GM Privileges"
        gm_action = QAction(gm_txt, self)
        if self.base_model.local_gm_privs() and len(self._selected_users()) == 1:
            menu.addAction(gm_action)

        selectedItem = menu.exec_(globalPos)  # QAction

        if selectedItem:
            if selectedItem.text() == mute_txt:
                self.mute_users()
            elif selectedItem.text() == kick_txt:
                self.kick_users()
            elif selectedItem.text() == gm_txt:
                self.set_gm()
