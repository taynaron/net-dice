from .__init__ import *

from urllib import request

import net_dice_helpers as nd_helpers




'''
Checks a website to find your external IP and if it's proxied
'''
class IPThread(QThread):
    IPDiscovered = Signal(str)
    threadError = Signal(str, Exception)

    def __init__(self, parent = None):
        QThread.__init__(self, parent)


    def get_IP(self):
        logging.info("Starting to grab the hosts IP address")
        self.start()


    def run(self):
        url = "http://www.cyberciti.biz/files/what-is-my-ip-address.php"
        logging.info("Pinging the site: " + url)
        IP = "Connection Error"

        try:
            resp = request.urlopen(url, timeout=10, cafile=nd_helpers.support_file_path("network/cacert.pem"))
            for line in resp.readlines():
                # decode the bytes object into a string.
                line = line.decode("utf-8")

                if "IP address" not in line:
                    continue

                ip_start = line.find(':', 1) + 2
                ip_end = line.find("</", 1)
                IP = line[ip_start:ip_end]
                if "via" in IP:
                    ip, proxy = IP.split("(via")
                    ip = ip.split(',')[0].strip()
                    proxy = proxy.strip()[:-1]
                    IP = ip
                    if ip != proxy:
                        IP += " (Proxied: {0})".format(proxy)
                break
        except Exception as e:
            self.threadError.emit("IPThread", e)
        self.IPDiscovered.emit(IP)


    def __del__(self):
        self.wait()
