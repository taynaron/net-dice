# This number is used for version checking and is displayed in the menu bar
# The OS X version reports this version number correctly (through a seperate script, not in this code)
# The Windows version generates version info from this string through a windows-side script run at build time
VERSION_NUMBER = '0.9.0'


#######################

# User modes. Can be local, client, server.
# This state is duplicated many times in the code and synced using signals.
# There's probably an easier way of doing this.
AE_MODE = 'app_engine'
CLIENT_MODE = 'client'
SERVER_MODE = 'server'
EMAIL_MODE = 'email'
LOCAL_MODE = 'local'

CLIENT_STATES = [AE_MODE, CLIENT_MODE]
ONLINE_STATES = [AE_MODE, CLIENT_MODE, SERVER_MODE]
VALID_STATES = [AE_MODE, CLIENT_MODE, SERVER_MODE, EMAIL_MODE, LOCAL_MODE]


#######################

# website URL
website = 'https://bitbucket.org/taynaron/net-dice/wiki/Home'
