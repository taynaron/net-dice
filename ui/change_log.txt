== v0.9.0 ==
01/15/2017
Added exploding dice (For ShadowRun-style rulesets)
    ex: 3d6ex
Deprecated Email Mode - email netdiceproject@gmail.com to ask for it back
Changed file format of widgets to JSON, preferences to binary
Added icon to windows application
Fixed multiple roller edge-case bugs
Fixed window changing size on startup
Fixed server/client disconnect if trying to set UI mode multiple times
Internals: Updated to PyQt5, Python 3


== v0.8.11 ==
3/23/2015
Update commands that use SSL
Fixed roller edge-case with expanded rolls and negative numbers
Updated underlying libraries


== v0.8.10 ==
10/23/2014
Fixed version checking on OS X
Better updating error help
Updated underlying libraries


== v0.8.9 ==
9/9/2014
Fixed some floating point bugs
Added < > comparison operators, % (modulo operator)


== v0.8.8 ==
8/3/2014
Internet Mode (serverless networking) - in beta
Roller supports floating point operations once again
crit highlighting now works across min/max


== v0.8.5 ==
7/29/2013
Tons of bug fixes for bugs new and old
Testing framework so bugs don't get through in the future


== v0.8.1 ==
7/20/2013
Fixed networked rolling
Fixes for new User List


== v0.8.0 ==
7/11/2013
Separated GM and server roles - any player can be server
added 'gm', 'server', and 'muted' columns to the user list
Removed Roll Builder mode - let me know if you want it back
Added Edit menu: cut/copy/paste/select all
Reworked the roller integration code


== v0.7.2 ==
5/5/2013
Added option to use either PySide or PyQt
d% crit potentials (doubles) detection added


== v0.7.0 ==
4/1/2013
Email Mode added
Client/Server code stabilized
Roller code:
    added d% as shortcut for d100
    more fixes


== v0.6.5 ==
3/20/2013
Self-update fix (broken in 0.6.1-0.6.3 - manually update via link in help)
minor mute color fix
Better error logging


== v0.6.3 ==
3/19/2013
Widget collapsing arrow fix
Color selection bug fix
Minor roller code fix


== v0.6.2 ==
3/13/2012
Fixed bug where dark-background clients could not roll widgets
"Roll Privately" Checkbox remembers state
Linux update checking
Roller code fixes


== v0.6.1 ==
2/27/2012
Minor roller code fixes


== v0.6.0 ==
2/20/2012
Clients will stop disconnecting from server during periods of inactivity (keepalives)
Better critical coloring based on user color
Player names and colors properly update in the client connection list
Fully implemented drag/drop
Better-looking chat window
functional hyperlinks
Chat window word-wrap
Widget edit window (hit 'e' while on a widget)
Collapsible UI elements
Fixed roll-builder mode
'Mute User' works
New roller code:
    supports min(x,y) and max(x,y)
    always returns an integer - no need for floor() or //
    supports chaining crit and fail: d20crit18fail2


== v0.4.0 ==
9/6/2012
Bug fix for multiple users behind the same subnet
Slightly better IP detection
User list should finally work correctly!
Server can kick clients
Muting works again (Sends only to server and sending client)
Clients now named correctly (no more 'Name 1')


== v0.3.5 ==
8/25/2012
Chat window text changes based on user background color
Better default user color
Drag-and-drop within the program strips formatting
Windows version can actually open the help file ("help->about NetDice")
Hyperlinks highlighted (still not clickable)


== v0.3.4 ==
7/8/2012
Works in OS X
Faster boot times in Windows
Change log dialog box pops up


== v0.3.3 ==
3/18/2012
Looking to fix the issue with private roles not syncing correctly
Updated PyQT, PyInstaller
Added a unified logging interface
Added rerolling, ROLLregeY = reroll if a roll >= Y, ROLLreleY = reroll if a roll <= Y
Added widgets expanding into a set of ()
No more console on Windows
Faster load times on MacOS


== v0.3.2 ==
5/20/2011
Reworked roller code
'crit' reserved word and crit detection
Line wrap


== v0.2.5 ==
New networking layer
Improved network reliability
Improved network speed for larger rolls


== v0.2.4 ==
Nesting, moving, deleting widgets works (much better)
Updated Qt (4.7.1), PyQt (4.8.2), Twisted (10.1) libraries
More debugging code for networking
Clearer IP proxy text in server mode
Simplified roll history, stripped some pointless steps and statements
Much less likely to drop rolls


== v0.2.2 ==
Deleting grouped widgets works
Can import widgets from .csv format (assumes excel conventions, tabbed cells == groups)
- thing1, 4d6,
- group1, ,
- , nested1, 34
- , nested2, 2d[thing1]
- thing2
