import copy
import dice_roller
import json
import os
import time
import webapp2
from google.appengine.api import channel
from google.appengine.api import users
from google.appengine.ext import ndb
from google.appengine.ext.webapp import template


class WSHandleChannel(webapp2.RequestHandler):
	def post(self):
		self.response.headers['Content-Type'] = 'application/json'
		key = self.request.get('key')
		channel.send_message(key, json.dumps({"meaning of life": 42}))
		self.response.write(json.dumps({"error": "not implemented"}))

	def get(self):
		self.response.headers['Content-Type'] = 'application/json'
		key = self.request.get('key')
		channel_token = channel.create_channel(key)
		self.response.write(json.dumps({"token": channel_token}))
