import logging, os, re, sys

import net_dice_globals as NDG

__QtLib__ = os.environ['QT_API']
if __QtLib__ == 'pyqt':
    from PyQt5.QtGui import QColor
else:
    from PySide.QtGui import QColor


html_check = re.compile(r'\<.*?\>', re.IGNORECASE)
nbsp_check = re.compile(r'&nbsp;', re.IGNORECASE)




def chat_wrap(chat_name):
    return '<b>' + chat_name + '</b>:: '


def strip_html(html_str):
    return nbsp_check.sub(' ', html_check.sub('', str(html_str)))


def validate_roll_name(roll_txt):
    roll_name = str(roll_txt).strip()
    if roll_name == '':
        return 'roll'

    # This line doesn't do anything
    roll_name.replace('[', '(').replace(']', ')').replace('{', '(').replace('}', ')')

    for bad_char in ":=<>":
        roll_name = roll_name.replace(bad_char, "_")

    return roll_name


'''
Base assumption that the version numbers are x.y.z
Return c-style comparisons (1<2: -1, 1==1: 0, 2>1: 1)
'''
def _compare_versions(one, two):
    logging.info("Comparing versions")
    one_vers_nums = [int(x) for x in one.split('.')]
    two_vers_nums = [int(x) for x in two.split('.')]

    if one_vers_nums < two_vers_nums:
        return -1
    elif one_vers_nums > two_vers_nums:
        return 1
    return 0


def newer_version_available(other_vers):
        logging.info("UpdateThread: Checking version current: " + str(NDG.VERSION_NUMBER) + " against " + str(other_vers))
        return _compare_versions(NDG.VERSION_NUMBER, other_vers) < 0


def check_older_version(other_vers):
        logging.info("UpdateThread: Checking version current: " + str(NDG.VERSION_NUMBER) + " against " + str(other_vers))
        return _compare_versions(NDG.VERSION_NUMBER, other_vers) > 0


def equal_versions(other_vers):
        logging.info("UpdateThread: Checking version current: " + str(NDG.VERSION_NUMBER) + " against " + str(other_vers))
        return _compare_versions(NDG.VERSION_NUMBER, other_vers) == 0


# ---color helpers

def color_text(text, color='#000000'):
    if color != '#000000':
        return '<FONT COLOR="{color}">{text}</FONT>'.format(color=color, text=text)
    return text


def separate_hues(color1, color2):
    # determines if two colors are 'similar' in hue. used for crit rolls
    # whether to switch to alt color
    c1 = QColor(color1)
    c2 = QColor(color2)

    c1h = c1.hue()
    c1s = c1.saturation()
    c1v = c1.value()
    c2h = c2.hue()
    c2s = c2.saturation()
    c2v = c2.value()

    # print "h: ", c1h, c1s, c1v, ' : ', c2h, c2s, c2v, ' : ', abs(c1v-c2v), abs(c1s-c2s), abs(c1h-c2h)
    if (abs(c1h -c2h) < 50 or abs(c1h -c2h) > 310) and abs(c1v -c2v) < 40 and abs(c1s -c2s) < 60:

        if abs(c1h -c2h) < 30 or abs(c1h -c2h) > 330:
            c1h = (c2h+41)%360

        if abs(c1v-c2v) < 80:
            #print c1v, c2v, (c1v+40)%255, (c1v-40)%255
            c1v = (c1v+31)%255 if c1v > c2v else (c1v-31)%255
            while c1v < 80 or abs(c1v-c2v) < 80:
                c1v = (c1v-37)%255

        c1.setHsv(c1h, c1s, c1v)

        # probably should log this...
        logging.info("separate_hues: "+str(c1h)+", "+str(c1s)+", "+str(c1v)+", "+str(c1.name()))

    return str(c1.name())
    # remember that, by default, Qt returns things as QStrings, not strings.
    # This can lead to errors, like throwing 'join' errors



def separate_vals(color1, color2):
    # determines if two colors are 'similar' in value (brightness). used for hidden text
    # whether to switch to alt color
    c1 = QColor(color1)
    c2 = QColor(color2)

    c1h = c1.hue()
    c1s = c1.saturation()
    c1v = c1.value()
    # c2h = c2.hue()
    # c2s = c2.saturation()
    c2v = c2.value()

    # print "s: ", c1h, c1s, c1v, ' : ', c2h, c2s, c2v, ' : ', abs(c1v-c2v), abs(c1s-c2s), abs(c1h-c2h)
    if abs(c1v-c2v) < 60:
        c1v = 255 - c1v

        if abs(c1v-c2v) < 60:
            c1v = (c1v+80)%255

        c1.setHsv(c1h, c1s, c1v)

    return str(c1.name())



# ---file openers

def open_support_file(path):
    f = support_file_path(path)
    f = open(f, 'r')
    return f.read()


def support_file_path(rel_path):
    logging.info("Trying to open " + rel_path)
    file = rel_path.split('\\')[-1].split('/')[-1]

    '''
    when we bundle the app we tend to collapse the file structure and place
    all support files in one location.
    This function should handle how py2app, py2exe, and pyinstaller package support files.
    '''
    try:
        resc_path = _resource_path(file)
        if os.path.isfile(resc_path):
            return resc_path
    except:
        pass

    if hasattr(sys, 'frozen'):
        frozen_path = os.path.join(os.path.dirname(sys.executable), file)
        if os.path.isfile(frozen_path):
            logging.info("Found frozen path: " + frozen_path)
            return frozen_path

        frozen_path = os.path.join(os.path.dirname(os.path.dirname(sys.executable)), 'Resources', file)
        if os.path.isfile(frozen_path):
            logging.info("Found frozen path: " + frozen_path)
            return frozen_path

        if os.path.isfile(file):
            logging.info("Found file: " + file)
            return file
    else:
        if os.path.isfile(rel_path):
            logging.info("Found path: " + rel_path)
            return rel_path

        prev_path = os.path.join("../", rel_path)
        if os.path.isfile(prev_path):
            logging.info("Found path: " + prev_path)
            return prev_path

    logging.error("Found no file!")
    return None


def _resource_path(relative):
    import subprocess

    if sys.platform.startswith('win'):
        command = 'echo %_MEIPASS2%'
    else:
        command = 'echo $_MEIPASS2'

    proc = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
    proc.wait()
    stdout, stderr = proc.communicate()

    meipass = stdout.strip()

    # win32 fix
    if meipass.startswith(r'%'):
        meipass = ''

    return os.path.join(sys._MEIPASS, relative)
