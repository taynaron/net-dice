import logging, os, re, time

__QtLib__ = os.environ['QT_API']
if __QtLib__ == 'pyqt':
    from PyQt5.QtCore import Qt, QModelIndex, QUrl
    from PyQt5.QtWidgets import QApplication
else:
    from PySide.QtCore import Qt, QModelIndex, QUrl
    from PySide.QtGui import QApplication

from trees_and_lists.base_tree_model import TreeModel
import net_dice_helpers as nd_helpers

from ui import drag_drop as dd

from text_roller import TextRoller




def _hyper(txt_str):
    link = txt_str.group(0)
    if "://" not in link:
        link = "http://" + link
    return '<a href="{link}">{txt}</a>'.format(link=link, txt=txt_str.group(0))


def _add_name(user_info):
    if 'Name' in user_info:
        return nd_helpers.chat_wrap(user_info['Name'])
    return ''



# The handler for the chat window
# Stores all info in a tree structure
class DiceModel(TreeModel):
    def __init__(self, ui, headers=['Rolls'], parent=None):
        TreeModel.__init__(self, headers, editable=False, item_colors=True, parent=parent)
        # self.rootItem = DiceItem(headers)
        self.base_text_color = '#000000'
        self.base_hidden_color_html = '#777777'
        self.base_crit_color = '#00BB00'
        self.base_crit_fail_color = '#FF1111'
        self.base_crit_poss_color = '#FF9900'

        self.color = {}
        self.color['text'] = self.base_text_color
        self.color['crit'] = self.base_crit_color
        self.color['fail'] = self.base_crit_fail_color
        self.color['possible'] = self.base_crit_poss_color
        self.last_message = None

        self._hyperlink_check = re.compile(r'\b(?:(?:https?|ftp|file)://|www\.|ftp\.)(?:\([-A-Z0-9+&@#/%=~_|$?!:,.]*\)|[-A-Z0-9+&@#/%=~_|$?!:,.])*(?:\([-A-Z0-9+&@#/%=~_|$?!:,.]*\)|[A-Z0-9+&@#/%=~_|$])', re.IGNORECASE)
        # web links

        self.roller = TextRoller()
        self.ui = ui
        self.beeping = False

        self._setup_connections()


    def _setup_connections(self):
        self.ui.beepCheck.clicked.connect(self.set_beep)


    def _get_colors(self, user_dict):
        # returns the foreground and background colors depending on the current status.
        # foreground is HTML, background is QColor.
        roll_color = user_dict['Color'] if 'Color' in user_dict else None

        self.color['text'] = self.base_hidden_color_html if 'Hide Rolls' in user_dict and user_dict['Hide Rolls'] is True else self.base_text_color

        if roll_color:
            self.color['text'] = nd_helpers.separate_vals(self.color['text'], roll_color)

            self.color['crit'] = nd_helpers.separate_hues(self.base_crit_color, roll_color)
            self.color['fail'] = nd_helpers.separate_hues(self.base_crit_fail_color, roll_color)
            self.color['possible'] = nd_helpers.separate_hues(self.base_crit_poss_color, roll_color)
        else:
            self.color['text'] = self.base_text_color
            self.color['crit'] = self.base_crit_color
            self.color['fail'] = self.base_crit_fail_color
            self.color['possible'] = self.base_crit_poss_color

        return roll_color


    def get_prefs(self):
        return {'Annoying Beep': self.beeping}


    def set_prefs(self, prefs_dict):
        if 'Annoying Beep' in prefs_dict:
            self.set_beep(prefs_dict['Annoying Beep'])
            self.ui.beepCheck.setChecked(self.beeping)
        else:
            self.set_beep(False)


    def set_beep(self, beep):
        self.beeping = beep


    def print_tree(self, print_hist=False):
        print(self.text_tree(print_hist))


    def text_tree(self, print_hist=False, node=None, level=0):
        roll_dict = self.dict_tree(node, level)
        if isinstance(roll_dict, str):
            return roll_dict

        assert isinstance(roll_dict, dict)
        roll_list = roll_dict['Rolls']
        txt_list = []

        for roll in roll_list:
            if isinstance(roll, str):
                txt_list.append(roll)
            elif isinstance(roll, dict):
                for instance in roll:
                    txt_list.append(instance)

                    if print_hist:
                        for step in roll[instance]:
                            txt_list.append("    " + step)

            if print_hist:
                txt_list.append('')

        return "\n".join(txt_list)


    def dict_tree(self, node=None, level=0):
        if not node:
            node = self.rootItem

        base_txt = nd_helpers.strip_html(str(node.data(0))).strip()
        children = [self.dict_tree(node.child(kid), level + 1) for kid in range(node.child_count())]
        if children:
            return {base_txt: children}
        else:
            return base_txt


    # Add a roll to the tree. Could be a single roll or a MultiRoll.
    # This function figures out which it is and calls the appropriate function
    def add_rolls(self, new_rolls, roll_name, user_info={}):
        if 'Name' in user_info:
            if self._is_muted(user_info['Name']):
                return ''

        txt = self.roller.lineToText(new_rolls[-1])
        success = self._add_roll(new_rolls, roll_name, user_info)
        if not success:
            return ''

        if self.beeping:
            QApplication.beep()
        return txt


    def _make_hist_list(self, new_roll):
        hist_list = []
        for step in new_roll:
            # remember, each step has to be in an array, or the tree will split the sring by chars
            hist_list.append([self.roller.lineToHtml(step, self.color)])
        return hist_list


    def _is_muted(self, user_name):
        try:
            user_model = self.ui.usersView.base_model  # should really not use pseudo-globals.
            return user_model.is_muted(user_name)
        except:
            logging.error("Couldn't get user model")
            return False


    # Add a single roll.
    def _add_roll(self, new_roll, roll_name, user_info={}, parent=QModelIndex()):
        roll_color = self._get_colors(user_info)

        roll_txt = _add_name(user_info) + roll_name
        roll_str = self.roller.lineToText(new_roll[0])

        if roll_name:
            roll_str = ' (' + roll_str + ')'

        roll_end = ' = ' + self.roller.resultToHtml(new_roll, self.color, color_base=False)
        roll_add = ''
        if len(roll_txt + roll_str) > 30:
            roll_add = roll_end
        else:
            roll_add = roll_str + roll_end
            roll_str = None
            roll_txt += ': '

        roll_txt += roll_add
        roll_txt = nd_helpers.color_text(roll_txt, self.color['text'])

        self._add_spacer()

        add_test = self.insert_row_to_end([roll_txt], bg_color=roll_color, parent=parent)
        roll_index = TreeModel.index(self, TreeModel.getItem(self, parent).child_count() - 1, 0, parent=parent)
        hist_list = self._make_hist_list(new_roll)

        if not roll_str:
                hist_list = hist_list[1:] if len(hist_list) > 1 else None

        if hist_list:
            self.insert_rows(0, hist_list, bg_color=roll_color, parent=roll_index)
        return add_test


    def _user_added(self, user_name):
        self.add_broadcast('  # User "' + user_name.strip() + '" has joined.')


    def _user_removed(self, user_name):
        self.add_broadcast('  # User "' + user_name.strip() + '" has left.')


    def _disconnected(self):
        self.add_broadcast('  # Disconnected from server.')


    # Add a chat line
    def add_broadcast(self, text, user_dict={}, htmlInput=False):
        roll_color = self._get_colors(user_dict)

        if not htmlInput:
            text = self._hyperlink_check.sub(_hyper, text)

        chat_txt = ''
        if 'Name' in user_dict:
            chat_txt = _add_name(user_dict['Name'])
        chat_txt += text
        chat_txt = nd_helpers.color_text(chat_txt, self.color['text'])

        self._add_spacer()
        add_test = self.insert_row_to_end([chat_txt], bg_color=roll_color)
        if self.beeping and add_test:
            QApplication.beep()
        return add_test


    # formatting helpers

    # Add a blank (spacer) line if there's been a conversation gap
    def _add_spacer(self):
        now = time.time()
        if self.last_message and (now - self.last_message) > 45:
            self.insert_row_to_end([''])
        self.last_message = now


    def _clear_rows(self):
        self.last_message = None
        TreeModel._clear_rows(self)


    # --- Drag and Drop Functions ---
    # Qt::DropActions DragDropListModel::supportedDropActions() const
    def supportedDropActions(self):
        return Qt.CopyAction | Qt.MoveAction


    def flags(self, index):
        return Qt.ItemIsDropEnabled | Qt.ItemIsDragEnabled | TreeModel.flags(self, index)


    # QMimeData *DragDropListModel::mimeData(const QModelIndexList &indexes) const
    def mimeData(self, indexes):
        mimeData = dd.DiceMimeData()

        widgetModel = self.ui.widgetView.model().sourceModel()

        chat_text = []
        chat_links = []

        # see ui/drag_drop for rolls format
        rolls_list = []  # the rolls we want to roll
        subrolls_dict = {}  # the necessary subrolls to roll the above

        for index in indexes:
            if index.isValid() and index.column() == 0:
                is_top_level = index.parent() == self.index(0, 0).parent()

                if index.internalPointer().child_count() > 0:  # then it's a roll
                    roll_dict = self._extract_roll_info(index)

                    if roll_dict:
                        rolls_list.append(roll_dict)
                        subrolls_dict.update(self.roller.getNeededWidgets(roll_dict["Roll"], widgetModel.get_widget_dict(flatten=True)))
                    # a low-overhead way of combining dictionaries

                elif not is_top_level:  # it's a subroll
                    roll_dict = self._extract_roll_info(index.parent())
                    if roll_dict:
                        roll_dict["Roll"] = nd_helpers.strip_html(index.data())
                        dupe = False
                        for roll in rolls_list:
                            if roll_dict["Roll Name"] == roll["Roll Name"]:
                                dupe = True
                                break

                        if not dupe:
                            rolls_list.append(roll_dict)
                            if '[' in roll_dict['Roll']:
                                try:
                                    subrolls_dict.update(self.roller.getNeededWidgets(roll_dict["Roll"], widgetModel.get_widget_dict(flatten=True)))
                                except KeyError as e:
                                    logging.info("dice_tree_model:supportedDropActions: " + str(roll_dict["Roll"]) + str(e))
                                    # need to do some useful widget checking here
                                    continue

                else:  # it's a chat
                    txt = str(index.data())
                    links = self._hyperlink_check.findall(txt)
                    links = links[::2]  # get rid of all even-indexed entries; they're duplicates
                    chat_text.append(nd_helpers.strip_html(txt))
                    for url in links:
                        chat_links.append(QUrl(url))

        if rolls_list:
            mimeData.setRolls(rolls_list)

        if subrolls_dict:
            mimeData.setSubRolls(subrolls_dict)

        if chat_links:
            mimeData.setUrls(chat_links)

        if chat_text:
            mimeData.setText(", ".join(chat_text))
        else:
            mimeData.setTextFromRolls()

        return mimeData


    def _extract_roll_info(self, index):
        text = nd_helpers.strip_html(index.data())

        if text is None or text == '':
            return {}

        username = ''
        roll_txt = ''
        roll_name = ''
        roll_result = ''

        name_pos = text.find(':: ')
        name_found = name_pos > -1
        name_pos = max(name_pos, 0)

        if name_found:
            username = text[:name_pos].strip()

        roll_name_pos = text.find(': ', (name_pos + 3 * name_found))
        equal_pos = text.rfind(' = ', (name_pos + 3 * name_found))

        if roll_name_pos == -1:
            roll_name_pos = equal_pos
        else:
            roll_txt = text[roll_name_pos+3 : equal_pos]
            roll_txt = roll_txt[1:-1]  # remove outer parentheses

        roll_name = text[(name_pos+3*name_found) : roll_name_pos].strip()
        roll_result = text[equal_pos+3 :].strip()

        if not roll_txt:
            roll_txt = nd_helpers.strip_html(index.child(0, 0).data()).strip()

        roll_dict = {}

        if username:
            roll_dict["User Name"] = username

        if roll_name:
            roll_dict["Roll Name"] = roll_name

        if roll_txt:
            roll_dict["Roll"] = roll_txt

        if roll_result:
            roll_dict["Result"] = roll_result

        return roll_dict
