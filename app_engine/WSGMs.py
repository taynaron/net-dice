import json
import logging
from google.appengine.ext import ndb

from WSHandler import WSHandleGM_base
from CommonFunctions import *
from GMModel import *
from UserModel import *


"""
Handler for a collection of GMs
"""
class WSHandleGMs(WSHandleGM_base):
	"""
	Do nothing for now
	"""
	@WSHandleGM_base._try_decorator
	def post(self):
		self._errorOut("Cannot create new GM", 404)

	"""
	Do nothing for now
	"""
	@WSHandleGM_base._try_decorator
	def get(self):
		self._errorOut("Cannot get GMs", 404)

	"""
	Do nothing for now
	"""
	@WSHandleGM_base._try_decorator
	def put(self):
		self._errorOut("Cannot change GMs", 404)

	"""
	Do nothing for now
	"""
	@WSHandleGM_base._try_decorator
	def delete(self):
		self._errorOut("Cannot delete GMs", 404)
	

"""
Handler for a specific of GM
"""
class WSHandleGM(WSHandleGM_base):
	"""
	Do nothing
	"""
	@WSHandleGM_base._try_decorator
	def post(self, query):
		self._errorOut("Cannot create new GM", 404)

	"""
	For the user in the session get the GM
	"""
	@WSHandleGM_base._try_decorator
	def get(self, session):
		out = self.__setup__()

		gm_guid = GetQuery("gm_" + str(session), "SELECT * FROM GMModel WHERE session = :1", (session,))
		gm_user = GetQuery("gm_user_" + str(gm_guid.guid) + "_" + str(session), "SELECT * FROM UserModel WHERE guid = :1 and session = :2", (gm_guid.guid, session,))
		user = GetQuery("user_" + str(out["guid"]) ,"SELECT * FROM UserModel WHERE guid = :1 AND session = :2", (out["guid"], session,))

		if user and gm_user:
			AppendLog(session, user.guid, "", "system", json.dumps({"GM": gm_user.name}))
			SetMemcache(self._key_users(out), None)

			self._writejson({"GM": gm_user.name})
		else:
			self._errorOut("Not authorized to get GM", 403)

	"""
	For given a session, set the new gm
	"""
	@WSHandleGM_base._try_decorator
	def put(self, session):
		out = self.__setup__()
		logging.info("WSHandleGM.put: out={0}".format(out))

		gm = GetQuery("gm_" + str(session), "SELECT * FROM GMModel WHERE session = :1", (session,))
		logging.info("WSHandleGM.put: gm={0}".format(gm))

		if gm and gm.guid == out["guid"]:
			gm.guid = out["new_gm"]
			PutQuery("gm_" + str(session), gm)

			user = GetQuery("user_" + str(out["new_gm"]) ,"SELECT * FROM UserModel WHERE guid = :1 AND session = :2", (out["new_gm"], session,))
			logging.info("WSHandleGM.put: user={0}".format(user))
			AppendLog(session, "*", "", user.name, json.dumps({"Updated GM": user.name}))
			AppendLog(session, user.name, "", "system", json.dumps({"dev_message": "You are the new GM", "Is GM": True}))
			SetMemcache(self._key_users(out), None)

			self._writejson({"message": "message received", "Is GM": out["guid"] == user.guid})
		else:
			self._errorOut("Not authorized to change GM", 403)

	"""
	Do nothing
	"""
	@WSHandleGM_base._try_decorator
	def delete(self, query):
		self._errorOut("Cannot delete this GM", 404)
