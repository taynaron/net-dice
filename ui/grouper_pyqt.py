# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui/grouper.ui'
#
# Created by: PyQt5 UI code generator 5.7
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_grouperDialog(object):
    def setupUi(self, grouperDialog):
        grouperDialog.setObjectName("grouperDialog")
        grouperDialog.setWindowModality(QtCore.Qt.WindowModal)
        grouperDialog.resize(250, 90)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(grouperDialog.sizePolicy().hasHeightForWidth())
        grouperDialog.setSizePolicy(sizePolicy)
        grouperDialog.setMaximumSize(QtCore.QSize(1000, 16777215))
        grouperDialog.setSizeGripEnabled(False)
        grouperDialog.setModal(True)
        self.verticalLayout = QtWidgets.QVBoxLayout(grouperDialog)
        self.verticalLayout.setContentsMargins(3, 6, 3, 3)
        self.verticalLayout.setSpacing(3)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(grouperDialog)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.groupEdit = QtWidgets.QLineEdit(grouperDialog)
        self.groupEdit.setObjectName("groupEdit")
        self.verticalLayout.addWidget(self.groupEdit)
        self.groupButtonBox = QtWidgets.QDialogButtonBox(grouperDialog)
        self.groupButtonBox.setOrientation(QtCore.Qt.Horizontal)
        self.groupButtonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.groupButtonBox.setObjectName("groupButtonBox")
        self.verticalLayout.addWidget(self.groupButtonBox)

        self.retranslateUi(grouperDialog)
        self.groupButtonBox.accepted.connect(grouperDialog.accept)
        self.groupButtonBox.rejected.connect(grouperDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(grouperDialog)

    def retranslateUi(self, grouperDialog):
        _translate = QtCore.QCoreApplication.translate
        grouperDialog.setWindowTitle(_translate("grouperDialog", "Name This Grouping"))
        self.label.setText(_translate("grouperDialog", "Name This Grouping:"))

