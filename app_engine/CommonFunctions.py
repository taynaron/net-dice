import logging
from google.appengine.api import memcache
from google.appengine.ext import ndb

from CounterModel import CounterModel
from LogModel import LogModel

"""
Function to encapsulate writing to the chat log as this will be a common
operation for all the web services to keep everyone in sync
"""
def AppendLog(session, white, black, is_from, data):
	_key_counter = lambda x: "counter_model_{0}".format(x)
	_key_log = lambda x, y: "log_{0}_{1}".format(x, y)

	counter_model = GetQuery(_key_counter(session), "SELECT * FROM CounterModel WHERE session = :1", (session,))

	if not counter_model:
		counter_model = CounterModel(counter=0, session=session)
	else:
		counter_model.counter += 1
	PutQuery(_key_counter(session), counter_model)

	counter = counter_model.counter
	new_log = LogModel(counter=counter, white=white, black=black, message=data, session=session, is_from=is_from)
	PutQuery(_key_log(session, counter), new_log)

def SetMemcache(key, data):
	logging.info("set caching ({0}, {1})".format(key, data))
	memcache.set(key, data, 7200) #default to 2 hours

def _BaseQuery(key, query, args, func):
	cached_data = memcache.get(key)
	if cached_data is not None:
		return cached_data
	else:
		_query = ndb.gql(query)
		_query = _query.bind(*args)
		if func == "fetch":
			data = _query.fetch()
		else:
			data = _query.get()
		SetMemcache(key, data)
		return data

def FetchQuery(key, query, args):
	return _BaseQuery(key, query, args, "fetch")

def GetQuery(key, query, args):
	return _BaseQuery(key, query, args, "get")

def PutQuery(key, data):
	if data:
		logging.info("set DB ({0}, {1})".format(key, data))
		data.put()
	SetMemcache(key, data)
