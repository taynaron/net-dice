import logging, os

__QtLib__ = os.environ['QT_API']
if __QtLib__ == 'pyqt':
    from PyQt5.QtCore import Qt
    from PyQt5.QtWidgets import QStyledItemDelegate, QRadioButton, QStyle, QStyleOptionButton
else:
    from PySide.QtCore import Qt
    from PySide.QtGui import QStyledItemDelegate, QRadioButton, QStyle, QStyleOptionButton



class GMRadioDelegate(QStyledItemDelegate):
    def __init__(self, parent):
        QStyledItemDelegate.__init__(self, parent)
        self.dummy = QRadioButton()


    def paint(self, painter, option, index):
        QStyledItemDelegate.paint(self, painter, option, index)

        use_radio = index.data(role=Qt.UserRole)
        if use_radio:
            is_on = use_radio > 1
            radioOption = QStyleOptionButton()
            radioOption.direction = option.direction
            radioOption.fontMetrics = option.fontMetrics
            radioOption.palette = option.palette
            radioOption.rect = option.rect
            radioOption.state = option.state
            if is_on:
                radioOption.state = radioOption.state | QStyle.State_On

            style = option.widget.style()
            style.drawControl(QStyle.CE_RadioButton, radioOption, painter)


    def sizeHint(self, option, index):
        return self.dummy.sizeHint()


    def editorEvent(self, event, model, option, index):
        new_state = model.data(index, role=Qt.UserRole)
        if not new_state:
            return False

        if new_state == 2:
            return True

        new_state -= 1
        new_state = not new_state
        model.setData(index, new_state, role=Qt.CheckStateRole)
        return True


    # I haven't seen a need for these functions, but they're here as reminders
    # if they end up being needed
    '''def createEditor(self, parent, option, index):
        editor = QRadioButton(parent)
        editor.setChecked(index.data(role=Qt.UserRole) > 1)
        return editor


    def setEditorData(self, editor, index):
        editor.setChecked(index.model().data(index, role=Qt.UserRole) > 1)


    def setModelData(self, editor, model, index):
        value = editor.isChecked()
        model.setData(index, value, Qt.CheckStateRole)


    def updateEditorGeometry(self, editor, option, index):
        size = editor.sizeHint()
        editor.setMinimumHeight(size.height())
        editor.setMinimumWidth(size.width())
        editor.setGeometry(option.rect)
        print size, option


    def eventFilter(self, editor, event):
        print 'eventFilter'
    '''
