from dice_roller import DiceRoller

import net_dice_helpers as nd_helpers





# The dice roller is object based
# this function takes the objects and converts them to the equivalent strings
class TextRoller(DiceRoller):
    def __init__(self):
        DiceRoller.__init__(self)
        self.base_text_color = '#000000'
        self.base_crit_color = '#00BB00'
        self.base_fail_color = '#FF1111'
        self.base_double_color = '#FF9900'


    def lineToText(self, line):
        if isinstance(line, dict):
            return self._line_to_str([line])
        elif isinstance(line, list):
            return self._line_to_str(line)
        raise TypeError


    def _line_to_str(self, in_list):
        return ''.join(map(self._value_to_str, in_list))


    def _value_to_str(self, t):
        if t["type"] in ["array"]:
            out = ["{" + ", ".join([str(a) for a in t["value"]]) + "}"]
        else:
            out = [str(t["value"])]

        if t["type"] in ["variable"]:
            out = ["["] + out + ["]"]
        elif t["type"] in ["comma"]:
            out = out + [" "]
        elif "double" in t and t["double"]:
            out = out + ["'double'"]
        elif "crit" in t and t["crit"]:
            out = out + ["'crit'"]
        elif "fail" in t and t["fail"]:
            out = out + ["'crit fail'"]
        return "".join(out)


    def resultToHtml(self, result, color_dict={}, color_base=True):
        return self._line_to_html(result[-1], color_dict, color_base)


    def lineToHtml(self, line, color_dict={}, color_base=True):
        if isinstance(line, dict):
            return self._line_to_html([line], color_dict, color_base)
        elif isinstance(line, list):
            return self._line_to_html(line, color_dict, color_base)
        raise TypeError


    def _make_list(self, roll):
        # quick helper function to  make a list that is #rolls long
        num_rolls = 1
        for step in roll[-1]:
            if step['type'] == 'comma':
                num_rolls += 1

        return [None for i in range(num_rolls)]


    def _line_to_html(self, in_list, color_dict={}, color_base=True):
        out_txt = self._value_to_html(in_list, color_dict=color_dict)

        if color_base:
            return nd_helpers.color_text(out_txt, self._get_text_color(color_dict))

        return out_txt


    def _value_to_html(self, value, force_crit=None, color_dict={}):
        if force_crit:
            base = self._line_to_str(value)
            if force_crit == 'crit':
                return nd_helpers.color_text(base, self._get_crit_color(color_dict))
            elif force_crit == 'fail':
                return nd_helpers.color_text(base, self._get_fail_color(color_dict))
            elif force_crit == 'double':
                return nd_helpers.color_text(base, self._get_double_color(color_dict))

        html_map = []
        esc_chars = '<>&'
        for token in value:
            html_map += self._map_html(token, color_dict)
            tkn = str(token['value'])
            for esc in esc_chars:
                if esc in tkn:
                    token['value'] = tkn.replace(esc, '\\' + esc)

        return ''.join(html_map)


    def _map_html(self, t, color_dict={}):
        if t["type"] in ["array"]:
            out = ["{" + ", ".join([str(a) for a in t["value"]]) + "}"]
        else:
            out = [str(t["value"])]

        if t["type"] in ["variable"]:
            out = ["["] + out + ["]"]
        elif t["type"] in ["comma"]:
            out = out + [" "]
        elif "double" in t and t["double"]:
            out = [nd_helpers.color_text(str(e), self._get_double_color(color_dict)) for e in out]
        elif "crit" in t and t["crit"]:
            out = [nd_helpers.color_text(str(e), self._get_crit_color(color_dict)) for e in out]
        elif "fail" in t and t["fail"]:
            out = [nd_helpers.color_text(str(e), self._get_fail_color(color_dict)) for e in out]
        return "".join(out)


    def _get_text_color(self, color_dict={}):
        return color_dict['text'] if 'text' in color_dict else self.base_text_color


    def _get_crit_color(self, color_dict={}):
        return color_dict['crit'] if 'crit' in color_dict else self.base_crit_color


    def _get_fail_color(self, color_dict={}):
        return color_dict['fail'] if 'fail' in color_dict else self.base_fail_color


    def _get_double_color(self, color_dict={}):
        # return color_dict['double'] if 'double' in color_dict else \
        #     (color_dict['poss'] if 'poss' in color_dict else \
        #     (color_dict['possible'] if 'possible' in color_dict else self.base_double_color))
        if 'double' in color_dict:
            return color_dict['double']
        if 'poss' in color_dict:
            return color_dict['poss']
        if 'possible' in color_dict:
            return color_dict['possible']
        return self.base_double_color
