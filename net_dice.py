'''
NetDice
Created by Ben Sutherland
Network and Roller code by Preston Yee
PyQt/Twisted
'''

# This program requires python 2.7 to run.

# This is the 'main' file. Note the 'if name == main' at the bottom. that's the main function.
# Sets up the main window and application, and handles the prefs, signals, and startup tasks.

import logging, sys, os
import network.Logger

from ui import sip_setup

# import syslog
# syslog.openlog("Python")




def init_curr_path():
    # pyinstaller and py2app have different cwds. This unifies.
    prefs_path = os.path.realpath(sys.path[0])
    # syslog.syslog(syslog.LOG_ALERT, "pre: " + prefs_path)

    pos = -1
    if sys.platform.startswith('win'):
        if prefs_path.endswith(r'NetDice\base_library.zip'):
            os.chdir('\\'.join(prefs_path.split('\\')[:-2]))
            return

    # macos or linux
    if '.app/' in prefs_path:
        dir_array = prefs_path.split('/')
        for dir_pos in range(len(dir_array)-1, -1, -1):
            if '.app' in dir_array[dir_pos]:
                pos = dir_pos
                break
        if pos > -1:
            prefs_path = '/'.join(dir_array[:pos])

    elif prefs_path.endswith(r'net_dice/base_library.zip'):  # windows file is run from a .zip with all core files
        prefs_path = '/'.join(prefs_path.split('/')[:-2])  # rm the file at the end of the directory

    if prefs_path is not os.getcwd():
        os.chdir(prefs_path)

    # syslog.syslog(syslog.LOG_ALERT, "post: " + prefs_path)
    # syslog.syslog(syslog.LOG_ALERT, "post: " + os.getcwd())


def run_app(variant, testing_mode=False):

    if variant == 'PySide':
        os.environ['QT_API'] = 'pyside'
        from PySide.QtGui import QApplication

    elif variant == 'PyQt':
        os.environ['QT_API'] = 'pyqt'
        sip_setup.SIPsetup()
        from PyQt5.QtWidgets import QApplication

    else:
        raise ImportError("Python Variant not specified")

    from net_dice_win import DiceWin

    # need to set the current path to a sane location: using the same dir as the app for now
    # otherwise defaults to /, which is no good for writing to
    init_curr_path()
    network.Logger.SetupLog()
    logging.info('QT_API: ' + str(os.environ['QT_API']))

    app = QApplication(sys.argv)
    win = DiceWin(app)
    if testing_mode:
        return (app, win)
    else:
        # Without these lines the app quits as soon as it stops receiving commands.
        # Handy for unit testing.
        app.lastWindowClosed.connect(app.quit)
        sys.exit(app.exec_())




if __name__ == "__main__":
    # variant = ''

    # env_api = os.environ.get('QT_API', 'pyqt')
    # if '--pyside' in sys.argv:
    #     variant = 'PySide'
    # elif '--pyqt' in sys.argv:
    #     variant = 'PyQt'
    # elif env_api == 'pyside':
    #     variant = 'PySide'
    # elif env_api == 'pyqt':
    #     variant = 'PyQt'
    variant = 'PyQt'

    run_app(variant)
