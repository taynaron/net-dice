from .__init__ import *

import smtplib
# import oauth2client
from email.mime.text import MIMEText




'''
Sends an email
'''
class EmailThread(QThread):
    emailSent = Signal()
    emailSendError = Signal(str, Exception)

    def __init__(self, parent=None):
        QThread.__init__(self, parent)
        self.player_name = ''
        self.user_name = ''
        self.from_email = ''
        self.user_pass = ''
        self.to_list = []
        self.to_string = ''
        self.subject = ''
        self.text = ''
        # self.email_service = ''
        self.is_bcc = False


    def send_email(self, playername, username, userpass, to_list, to_string, subj, text, service, is_bcc=False):
        logging.info("Starting to send an email")

        assert isinstance(playername, str) and isinstance(username, str) and isinstance(userpass, str) \
            and isinstance(to_list, list) and isinstance(to_string, str) and isinstance(subj, str) \
            and isinstance(text, str) and isinstance(service, str) and isinstance(is_bcc, bool)

        assert username
        assert service
        assert userpass

        self.player_name = playername
        self.user_name = username
        self.email_service = service
        # self.from_email = "{name}@{domain}".format(name=self.user_name, domain=self.email_service)
        self.user_pass = userpass
        self.to_list = to_list
        self.to_string = to_string
        self.subject = subj
        self.text = text
        self.is_bcc = is_bcc

        self.start()


    def run(self):
        # try:
        msg = MIMEText(self.text)

        msg['From'] = self.player_name

        msg['To'] = "NetDice players" if self.is_bcc else self.to_string

        msg['Subject'] = self.subject

        if self.email_service == 'gmail.com':
            # OAUTH2
            # https://developers.google.com/api-client-library/python/auth/installed-app
            pass
        else:
            # standard SMTP email
            mail_server = smtplib.SMTP("smtp.gmail.com", 587)
            mail_server.ehlo()
            mail_server.starttls()
            mail_server.ehlo()
            # print(self.user_name, self.user_pass)
            mail_server.login(self.user_name, self.user_pass)

            msg = mail_server.sendmail(self.user_name, self.to_list, msg.as_string())
            logging.info(str(msg))
            mail_server.quit()
        self.emailSent.emit()
        # except Exception as e:
        #     self.emailSendError[str, Exception].emit("EmailThread", e)


    def __del__(self):
        self.wait()
