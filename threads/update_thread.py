from .__init__ import *

import platform, sys
from urllib import request

import net_dice_helpers as nd_helpers




'''
Checks a Dropbox text file for the current version
Brings up a download dialog if a newer version exists
'''
class UpdateThread(QThread):
    # dropbox base link for getting a specific file with a public URL
    # raw arg means 'download file immediately'
    dropBoxURL = "https://www.dropbox.com/s/{0}?raw=1"

    NewVersion = Signal(str, str)
    threadError = Signal(str, Exception)

    def __init__(self, parent = None):
        QThread.__init__(self, parent)


    def check_new_version(self):
        logging.info("UpdateThread: Starting to grab the updated version")
        self.start()


    def run(self):
        logging.info("UpdateThread: Grabbing the updated version")
        os_type = sys.platform

        # tuple with 3 (macos) or 4 (win) args.
        os_version = platform.mac_ver() if os_type == "darwin" else platform.win32_ver()

        logging.info("UpdateThread: os_type: " + os_type)

        try:
            # File format is as follows
            # OS X = x.y.z
            # Windows = x.y.z

            # Consider adding a third "column" that states which version to grab
            # OSX 10.7, 0.6.5, net_dice_mac.zip
            # Windows, 0.6.5, net_dice_win.zip
            # Linux, 0.6.5, net_dice_code.zip
            dlURL = self.dropBoxURL.format("xq1p6oanleltf4y/net_dice_versions.txt")

            # cafile lets us ensure that we're actually contacting dropbox
            resp = request.urlopen(dlURL, cafile=nd_helpers.support_file_path("network/cacert.pem"))
            for line in resp.readlines():
                # decode the bytes object into a string. The version file is plain text, so decode with UTF8.
                line = line.decode("utf-8")

                vers = [item.strip() for item in line.split('=')]
                logging.info("UpdateThread: line: " + str(vers))
                logging.info("UpdateThread: os_vers: " + str(os_version))

                if os_type == "darwin":
                    major, minor = os_version[0].split('.')[:2]
                    major = int(major)
                    minor = int(minor)

                    if major >= 10 and minor >= 7:
                        self.grab_if_newer_version(vers[1], "{type} {vers}".format(type=os_type, vers=os_version), "sqotkg66khol6bm/net_dice_mac.zip")
                        break
                    else:
                        raise OSError("UpdateThread: Need macOS 10.7 or later")

                elif os_type.startswith("win") and vers[0] == "Windows":
                    self.grab_if_newer_version(vers[1], str(os_type), "ktixlaxff4gueit/net_dice_win.zip")
                    break

                elif os_type.startswith("linux"):
                    self.grab_if_newer_version(vers[1], str(os_type), "rv5qdbw4pz965q3/net_dice_code.zip")
                    break

            else:  # if not found
                raise OSError("UpdateThread: could not find valid OS")

        except Exception as e:
            self.threadError.emit("UpdateThread", e)


    def grab_if_newer_version(self, version, log_info, file_name):
        logging.info("UpdateThread: " + log_info + " found")
        if nd_helpers.newer_version_available(version):
            logging.info("UpdateThread: " + log_info + " update found")
            url = self.dropBoxURL.format(file_name)
            self.NewVersion.emit(version, url)


    def __del__(self):
        self.wait()
