import logging, os

__QtLib__ = os.environ['QT_API']
if __QtLib__ == 'pyqt':
    from PyQt5.QtCore import Qt
    from PyQt5.QtGui import QColor, QFont
else:
    from PySide.QtCore import Qt
    from PySide.QtGui import QColor, QFont


"""
Qt.ItemDataRole - state examples

The general purpose roles are:
Qt.DisplayRole                   0    The key data to be rendered (usually text).
Qt.DecorationRole                1    The data to be rendered as a decoration (usually an icon).
Qt.EditRole                      2    The data in a form suitable for editing in an editor.

Roles describing appearance and meta data:

Qt.FontRole                      6    The font used for items rendered with the default delegate.
Qt.TextAlignmentRole             7    The alignment of the text for items rendered with the default delegate.
Qt.BackgroundRole                8    The background brush used for items rendered with the default delegate.
Qt.BackgroundColorRole           8    This role is obsolete. Use BackgroundRole instead.
Qt.ForegroundRole                9    The foreground brush (text color, typically) used for items rendered with the default delegate.
Qt.TextColorRole                 9    This role is obsolete. Use ForegroundRole instead.
Qt.CheckStateRole               10    This role is used to obtain the checked state of an item (see Qt.CheckState).

Accessibility roles:
Qt.AccessibleTextRole           11    The text to be used by accessibility extensions and plugins, such as screen readers.
Qt.AccessibleDescriptionRole    12    A description of the item for accessibility purposes.

User roles:
Qt.UserRole                     32    The first role that can be used for application-specific purposes.
"""



# The base Tree item, model, and view classes
# Here there be Dragons


class TreeItem(object):
    def __init__(self, data, parent=None):
        self.parentItem = parent
        self.itemData = data
        self.childItems = []

        # these are here to allow us to set the 'bold' and 'inactive' view states
        self.fontRole = QFont()
        self.fontColor = QColor(0, 0, 0)
        self.bgColor = QColor(255, 255, 255)


    def child(self, row):
        return self.childItems[row]


    def child_count(self):
        return len(self.childItems)


    def has_children(self):
        return True if self.childItems else False


    def child_number(self):
        if self.parentItem is not None:
            return self.parentItem.childItems.index(self)
        return 0


    def columnCount(self):
        return len(self.itemData)


    def data(self, column, role=Qt.DisplayRole):
        # reimplemented from treeitem to send display info for FontRole and ForegroundRole
        if role == Qt.FontRole:
            return self.fontRole
        elif role == Qt.ForegroundRole:
            return self.fontColor
        elif role == Qt.BackgroundRole:
            return self.bgColor
        else:
            return self.itemData[column]


    def all_data(self):
        # should only be used for non-modifying purposes
        return self.itemData


    def insert_children(self, position, items):
        if position < 0 or position > self.child_count():
            return False

        for row in items:
            item = TreeItem(row, self)
            self.childItems.insert(position, item)
            position += 1
        return True


    def insert_child(self, position, item):
        self.insert_children(position, [item])


    def insert_child_to_end(self, item):
        self.insert_child(self.child_count(), item)


    def insert_child_items(self, position, items):
        if position < 0 or position > self.child_count():
            logging.info("TreeItem: New item's column count is incorrect")
            return False

        for item in items:
            item.parentItem = self
            # turns out you have to manually set the parent item when you move something. DUH.
            self.childItems.insert(position, item)
            position += 1
        return True


    def insert_child_item(self, position, item):
        self.insert_child_items(position, [item])


    def insert_child_item_to_end(self, item):
        self.insert_child_item(self.child_count(), item)


    def parent(self):
        return self.parentItem


    def remove_children(self, position, count):
        if position < 0 or position + count > len(self.childItems):
            return False

        for row in range(count):
            self.childItems.pop(position)
        return True


    def remove_child(self, position):
        return self.remove_children(position, 1)


    def setData(self, column, value):
        if column < 0 or column >= len(self.itemData):
            return False

        self.itemData[column] = value
        return True


    def setAllData(self, values):
        if len(values) != len(self.itemData):
            return False

        self.itemData = values
        return True


    # helper functions for the item's state - could be more elegant, but it's conceptually sound
    # inactive sets the alpha down - dims the text
    # this doesn't affect the view when an item is selected, maybe there's a better way?
    def set_background(self, color):
        self.bgColor = color


    def set_foreground(self, color):
        self.fontColor = color
