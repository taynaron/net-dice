#!/usr/bin/env python
from google.appengine.ext import ndb

"""
DataModel that stores the bulk of the network traffic
"""
class LogModel(ndb.Model):
	"""
	State variable to determine who sent the message, meant for displaying
	"""
	is_from = ndb.StringProperty(indexed=False)

	"""
	State variable to order the messages, since NDB by default doesn't order
	the messages
	"""
	counter = ndb.IntegerProperty()

	"""
	State variable to hold who this message is intended for

	* > means everyone
	semi-colon separate values > for specific people, matches the users name
		or the users guid
	"""
	white = ndb.StringProperty(indexed=False)

	"""
	State variable to hold who this message is intended for

	semi-colon separate values > for specific people, matches the users name
		or the users guid
	"""
	black = ndb.StringProperty(indexed=False)

	"""
	State variable for tracking when the messages was sent
	TODO, currently unused, but could be usable for timestamping messages
	"""
	#time = ndb.StringProperty()

	"""
	State variable for the actual message
	NOTE doesn't assume that the message states who it's from
	"""
	message = ndb.JsonProperty(indexed=False)

	"""
	State variable for associating this message with a particular session
	"""
	session = ndb.StringProperty()
