import logging, os

__QtLib__ = os.environ['QT_API']
if __QtLib__ == 'pyqt':
    from PyQt5.QtCore import pyqtSignal as Signal, Qt
    from PyQt5.QtWidgets import QMenu, QAction
else:
    from PySide.QtCore import Signal, Qt
    from PySide.QtGui import QMenu, QAction

from trees_and_lists.base_tree_view import TreeView

from ui import drag_drop as dd
import net_dice_globals as NDG

from dice_roller import DiceRoller
from .chat_tree_element import LinkItemDelegate




# Display code for the above model.
class DiceView(TreeView):
    sendRoll = Signal(dict)
    sendRolls = Signal(dict)

    def __init__(self, parent=None):
        TreeView.__init__(self, parent)

        self.wrap_delegate = LinkItemDelegate(self)
        self.setItemDelegate(self.wrap_delegate)
        self.wrap_delegate.width = self.size().width()

        self.connection_mode = None
        self.expand_rolls = 1

        self.roller = DiceRoller()

        # this is by far the easiest way to create a right-click menu for an object.
        self.setContextMenuPolicy(Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.treeContextMenu)

        self._setup_connections()

        self.setAcceptDrops(True)
        self.setDropIndicatorShown(True)


    def _setup_connections(self):
        self.expanded.connect(self.resizeCols)
        self.collapsed.connect(self.resizeCols)


    def set_conn_mode(self, conn_mode):
        self.connection_mode = conn_mode


    def setModel(self, model):
        TreeView.setModel(self, model)
        self.model().rowsInserted.connect(self.scroll)


    def resizeCols(self, index):
        TreeView.minimize_cols(self)


    # --- Scrolling and Expanding ---
    def expand_all_rolls(self):
        self.set_expand_rolls(2)

    def expand_multi_rolls(self):
        self.set_expand_rolls(1)

    def expand_no_rolls(self):
        self.set_expand_rolls(0)


    def set_expand_rolls(self, level):
        self.expand_rolls = level

    def get_expand_rolls(self):
        return self.expand_rolls


    # Ensures that we're at the bottom of the screen whenever a new roll/chat is received
    # rowsInserted ( const QModelIndex & parent, int start, int end )
    def scroll(self, parent, start, end):
        if self.expand_rolls == 1:
            self.scrollTo(parent)
        elif self.expand_rolls == 2:
            self.scrollTo(parent.child(end, 0))
        self.scrollToBottom()


    def resizeEvent(self, event):
        # We need this function to force word wrap on a window resize.
        self.wrap_delegate.width = self.size().width()
        self.setColumnWidth(0, self.width() - 15)

        model = self.model()

        # definitely hackey, but it's the only way I found
        # to force the row heights to be recalculated
        rows = model.rowCount()
        if rows > 0:
            model.layoutChanged.emit()
            self.scroll(model.index(0, 0).parent(), 0, rows - 2)



    # --- Drag and Drop ---
    def dragEnterEvent(self, event):
        if event.source() != self:
            if event.mimeData().hasText() or event.mimeData().hasUrls() or event.mimeData().hasFormat(dd.DD_MIME_ROLLS):
                event.acceptProposedAction()


    def dragMoveEvent(self, event):
        self.dragEnterEvent(event)


    def dropEvent(self, event):
        drop = event.mimeData()
        logging.info("Chat Window Drop Event")

        if drop.hasFormat(dd.DD_MIME_ROLLS):
            rolls = drop.rolls()
            subrolls = drop.subRolls()
            if self.connection_mode not in NDG.ONLINE_STATES:
                for roll in rolls:
                    try:
                        end_roll = self.roller.evalRoll(roll['Roll'], subrolls)
                    except:
                        return False
                    self.model().add_rolls(end_roll, roll['Roll Name'])
            else:
                for a_roll in rolls:
                    a_roll['Roll Widgets'] = subrolls
                send_list = {'Rolls': rolls}
                self.sendRolls[dict].emit(send_list)
            event.acceptProposedAction()

        elif drop.hasText():
            text = str(drop.text())
            if self.connection_mode not in NDG.ONLINE_STATES:
                self.model().add_broadcast(text)
            else:
                self.sendRoll[dict].emit({'Chat': text})
            event.acceptProposedAction()

        # elif drop.hasUrls():
        # implement later if we care
        else:
            return False


    # Right click popup menu
    def treeContextMenu(self, pos):
        globalPos = self.mapToGlobal(pos)  # QPoint

        # this should get all the selected rows, or the row under the mouse if none are selected
        # unneeded because a right-click will select the item underneath
        # selected = self._get_selected_list(globalPos)

        menu = QMenu()
        open_txt = "&Open All"
        open_action = QAction(open_txt, self)
        menu.addAction(open_action)

        coll_txt = "&Collapse All"
        coll_action = QAction(coll_txt, self)
        menu.addAction(coll_action)

        selectedItem = menu.exec_(globalPos)  # QAction

        if selectedItem:
            if selectedItem.text() == open_txt:
                self.expandAll()
            elif selectedItem.text() == coll_txt:
                self.collapseAll()
