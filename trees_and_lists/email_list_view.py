import logging, os

__QtLib__ = os.environ['QT_API']
if __QtLib__ == 'pyqt':
    from PyQt5.QtCore import Qt, QPoint
    from PyQt5.QtWidgets import QMenu, QAction, QAbstractItemDelegate
else:
    from PySide.QtCore import Qt, QPoint
    from PySide.QtGui import QMenu, QAction, QAbstractItemDelegate

from .list_tree import ListView




class EmailView(ListView):
    def __init__(self, parent = None):
        ListView.__init__(self, parent)

        self.setContextMenuPolicy(Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.treeContextMenu)


    def moveCursor(self, cursorAction, modifiers):
        model = self.model().sourceModel()

        curr_index = self.currentIndex()
        cols = model.columnCount()-1 #to account for the checkbox
        rows = model.rowCount()
        if cursorAction == ListView.MoveRight or cursorAction == ListView.MoveNext:
            new_index = curr_index.sibling(curr_index.row(), curr_index.column()+1)
            if not (new_index.isValid() and new_index.column() < cols):
                new_index = curr_index.sibling(curr_index.row()+1, 0)
                if not (new_index.isValid() and new_index.row() < rows):
                    new_index = curr_index.sibling(0, 0)
            return new_index
        elif cursorAction == ListView.MoveLeft or cursorAction == ListView.MovePrevious:
            new_index = curr_index.sibling(curr_index.row(), curr_index.column()-1)
            if not new_index.isValid():
                new_index = curr_index.sibling(curr_index.row()-1, 0)
                if not new_index.isValid():
                    new_index = curr_index.sibling(rows-1, cols-1)
            return new_index
        elif cursorAction == ListView.MoveUp:
            new_index = curr_index.sibling(curr_index.row()-1, curr_index.column())
            if not new_index.isValid():
                new_index = curr_index.sibling(rows-1, curr_index.column())
            return new_index
        elif cursorAction == ListView.MoveDown:
            new_index = curr_index.sibling(curr_index.row()+1, curr_index.column())
            if not new_index.isValid():
                new_index = curr_index.sibling(0, curr_index.column())
            return new_index
        else:
            return ListView.moveCursor(self, cursorAction, modifiers)


    def closeEditor(self, editor, hint):
        #removes any non-edited items when we lose edit focus of an item
        #for a reason other than to tab around
        if not (hint == QAbstractItemDelegate.EditNextItem
                    or hint == QAbstractItemDelegate.EditPreviousItem):
            self._clear_empties()

        ListView.closeEditor(self, editor, hint)


    def mousePressEvent(self, event):
        self._clear_empties()
        pos = event.pos()
        index = self.indexAt(pos)

        if not index.isValid():
            model = self.model().sourceModel()
            last_index = self.indexAt(QPoint(pos.x(), pos.y()-30))
            last_index = last_index.sibling(last_index.row(), 0)
            if model.rowCount() == 0 or last_index.isValid():
                find_item = ['', '', '*']
                if not model.get_item_index(find_item).isValid():
                    new_item = ['', '', '']
                    added = model.add_item(new_item)
                    if added:
                        new_index = model.get_item_index(new_item)
                        model.setData(new_index.sibling(new_index.row(), model.check_col), 2, role=Qt.CheckStateRole)
                        proxy_index = self.model().mapFromSource(new_index)

                        self.setCurrentIndex(proxy_index)
                        self.edit(proxy_index)
                        return

        ListView.mousePressEvent(self, event)


    def _clear_empties(self):
        model = self.model().sourceModel()
        new_item = ['', '', '*']
        item = model.get_item_index(new_item)
        while item.isValid():
            proxy_index = self.model().mapFromSource(item)
            model.remove_row(proxy_index.row())
            item = model.get_item_index(new_item)


    def keyPressEvent(self, event):
        key = event.key()
        if key == Qt.Key_E:
            self.edit_emails('enable')
        elif key == Qt.Key_D:
            self.edit_emails('disable')
        elif key == Qt.Key_Delete:
            self._clear_empties()
            self.edit_emails('remove')
        elif key == Qt.Key_Escape:
            self._clear_empties()
        else:
            ListView.keyPressEvent(self, event)


    def make_selected_list(self, selected = None):
        if not selected:
            return self.selectedIndexes()
        return selected


    def edit_emails(self, function, selected = None):
        model = self.model().sourceModel()

        if not selected:
            selected = self.make_selected_list()

        try:
            if function == 'enable':
                model.enable_emails(selected, True)
            elif function == 'disable':
                model.enable_emails(selected, False)
            elif function == 'remove':
                model.remove_emails(selected)
            else:
                raise ValueError("Function '"+function+"' does not exist")
        except ValueError as e:
            logging.error(e)


    def treeContextMenu(self, pos):
        globalPos = self.mapToGlobal(pos) #QPoint

        menu = QMenu()
        enable_txt = "&Enable Selected"
        enable_action = QAction(enable_txt, self)
        enable_action.setShortcut(Qt.Key_E)
        menu.addAction(enable_action)

        disable_txt = "&Disable Selected"
        disable_action = QAction(disable_txt, self)
        disable_action.setShortcut(Qt.Key_D)
        menu.addAction(disable_action)

        rm_txt = "&Remove Selected"
        rm_action = QAction(rm_txt, self)
        menu.addAction(rm_action)

        selectedItem = menu.exec_(globalPos) #QAction

        if selectedItem:
            if selectedItem.text() == enable_txt:
                self.edit_emails('enable')
            elif selectedItem.text() == disable_txt:
                self.edit_emails('disable')
            elif selectedItem.text() == rm_txt:
                self.edit_emails('remove')
