import logging, os

__QtLib__ = os.environ['QT_API']
if __QtLib__ == 'pyqt':
    from PyQt5.QtCore import Qt, QAbstractItemModel, QModelIndex, QPersistentModelIndex, QSortFilterProxyModel
else:
    from PySide.QtCore import Qt, QAbstractItemModel, QModelIndex, QPersistentModelIndex
    from PySide.QtGui import QSortFilterProxyModel

from .base_tree_item import TreeItem


"""
Qt.ItemDataRole - state examples

The general purpose roles are:
Qt.DisplayRole                 0    The key data to be rendered (usually text).
Qt.DecorationRole              1    The data to be rendered as a decoration (usually an icon).
Qt.EditRole                    2    The data in a form suitable for editing in an editor.

Roles describing appearance and meta data:

Qt.FontRole                    6    The font used for items rendered with the default delegate.
Qt.TextAlignmentRole           7    The alignment of the text for items rendered with the default delegate.
Qt.BackgroundRole              8    The background brush used for items rendered with the default delegate.
Qt.BackgroundColorRole         8    This role is obsolete. Use BackgroundRole instead.
Qt.ForegroundRole              9    The foreground brush (text color, typically) used for items rendered with the default delegate.
Qt.TextColorRole               9    This role is obsolete. Use ForegroundRole instead.
Qt.CheckStateRole             10    This role is used to obtain the checked state of an item (see Qt.CheckState).

Accessibility roles:
Qt.AccessibleTextRole         11    The text to be used by accessibility extensions and plugins, such as screen readers.
Qt.AccessibleDescriptionRole  12    A description of the item for accessibility purposes.

User roles:
Qt.UserRole                   32    The first role that can be used for application-specific purposes.
"""




class TreeModel(QAbstractItemModel):
    # speed things up with:
    # http://www.slideshare.net/mariusbu/qt-item-views-in-depth - slide 80
    def __init__(self, headers, editable=False, item_colors=False, parent=None):
        super(TreeModel, self).__init__(parent)

        rootData = [header for header in headers]
        self.rootItem = TreeItem(rootData)

        self.editable = editable
        self.item_colors = item_colors


    def columnCount(self, parent=QModelIndex()):
        return self.rootItem.columnCount()


    def data(self, index, role):
        # reimplementing from TreeView to allow us to fiddle with the font
        if not (index.isValid() and index.column() < self.columnCount()):
            return None
        if not (role == Qt.DisplayRole or role == Qt.EditRole or role == Qt.ToolTipRole or role == Qt.FontRole or self.item_colors and (role == Qt.ForegroundRole or role == Qt.BackgroundRole)):
            return None
        item = self.getItem(index)
        return item.data(index.column(), role)


    def flags(self, index):
        if not index.isValid():
            return None
        return (self.editable and Qt.ItemIsEditable) | Qt.ItemIsEnabled | Qt.ItemIsSelectable


    def getItem(self, index):
        if index.isValid():
            if isinstance(index, QPersistentModelIndex):
                index = QModelIndex(index)
            item = index.internalPointer()
            # item = self.index(index.row(), index.column(), index.parent()).internalPointer()
            if item:
                return item
        return self.rootItem


    def headerData(self, section, orientation, role=Qt.DisplayRole):
        # not sure why we need the DisplayRole check, but it seems to be required
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return self.rootItem.data(section)
        return None


    # item = is the search "term", where it looks to match each row
    #        a "*" passed into the column means do a wildcard search
    # give_index = returns the index instead of the item
    # substring_ok = do a partial search, note it applies to the entire
    #        array of items
    def _item_exists(self, item, substring_ok=False, parent=QModelIndex()):
        cols = self.rootItem.columnCount()

        if isinstance(item, str):
            logging.info("WARNING: item {0} was passed in as a string".format(item))

        if len(item) == cols:
            parent_item = self.getItem(parent)
            for line_num in range(parent_item.child_count()):
                row = parent_item.child(line_num)
                col_match = 0
                for col in range(cols):
                    this_data = parent_item.child(line_num).data(col)
                    ic = str(item[col])
                    if ic == '*' or ic == this_data or substring_ok and ic in this_data:
                        col_match += 1
                    else:
                        break
                if col_match == cols:
                        return self.index(line_num, 0, parent)
                elif row.child_count() > 0:
                    found_child = self._item_exists(item, substring_ok, self.index(line_num, 0, parent))
                    if found_child.isValid():
                        return found_child

        return QModelIndex()


    def item_exists(self, item, substring_ok=False, parent=QModelIndex()):
        return self._item_exists(item, substring_ok=substring_ok, parent=parent).isValid()


    def get_item_index(self, item, substring_ok=False, parent=QModelIndex()):
        return self._item_exists(item, substring_ok=substring_ok, parent=parent)


    def substring_replace(self, old_str, new_str, parent=QModelIndex()):
        cols = self.rootItem.columnCount()
        parent_item = self.getItem(parent)
        matches = 0
        for line_num in range(parent_item.child_count()):
            row = parent_item.child(line_num)

            for col in range(cols):
                this_data = str(parent_item.child(line_num).data(col))
                if old_str in this_data:
                    new_data = this_data.replace(old_str, new_str)
                    if self.setData(self.index(line_num, col, parent), new_data):
                        matches += 1

            if row.child_count():
                new_parent_index = self.index(line_num, 0, parent)
                matches += self.substring_replace(old_str, new_str, parent=new_parent_index)

        return matches


    def index(self, row, column, parent=QModelIndex()):
        if parent.isValid() and parent.column() != 0:
            logging.info("index: not valid")
            return QModelIndex()

        try:
            parentItem = self.getItem(parent)
            childItem = parentItem.child(row)
            if childItem:
                return self.createIndex(row, column, childItem)
            else:
                return QModelIndex()
        except:
            return QModelIndex()


    def insert_rows(self, position, row_list, fg_color=None, bg_color=None, parent=QModelIndex()):
        num_rows = len(row_list)
        parentItem = self.getItem(parent)
        self.beginInsertRows(parent, position, position + num_rows - 1)
        success = parentItem.insert_children(position, row_list)
        if bg_color:
            for i in range(len(row_list)):
                parentItem.child(position + i).set_background(bg_color)
        if fg_color:
            for i in range(len(row_list)):
                parentItem.child(position + i).set_foreground(fg_color)
        self.endInsertRows()
        return success


    def insert_row(self, position, row, fg_color=None, bg_color=None, parent=QModelIndex()):
        return self.insert_rows(position, [row], fg_color, bg_color, parent)


    def insert_row_to_end(self, row, fg_color=None, bg_color=None, parent=QModelIndex()):
        return self.insert_row(TreeModel.getItem(self, parent).child_count(), row, fg_color, bg_color, parent)


    def move_indexes(self, index_list, new_parent_index):
        # num_rows = len(index_list)
        new_parent_item = self.getItem(new_parent_index)
        for index in index_list:
            parent = self.parent(index)
            parent_item = self.getItem(parent)

            if new_parent_item == parent_item:
                continue

            num_new_children = new_parent_item.child_count()
            item = self.getItem(index)


            self.beginMoveRows(parent, index.row(), index.row(), new_parent_index, num_new_children)
            parent_item.remove_child(index.row())
            new_parent_item.insert_child_item_to_end(item)
            self.endMoveRows()

            '''self.beginRemoveRows(parent, index.row(), index.row())
            parent_item.remove_child(index.row())
            self.endRemoveRows()
            self.beginInsertRows(new_parent_index, num_new_children, num_new_children)
            new_parent_item.insert_child_item_to_end(item)
            self.endInsertRows()
            print num_new_children, new_parent_item.child_count()'''


    def move_index(self, index, new_parent_index):
        return self.move_indexes([index], new_parent_index)


    def insert_empty_rows(self, position, rows, parent=QModelIndex()):
        parentItem = self.getItem(parent)
        self.beginInsertRows(parent, position, position + rows - 1)
        success = parentItem.insert_children(position, rows, self.rootItem.columnCount())
        self.endInsertRows()
        return success


    def insert_empty_row(self, position, parent=QModelIndex()):
        return self.insert_empty_rows(position, 1, parent)


    def parent(self, index):
        if not index.isValid():
            return QModelIndex()

        childItem = self.getItem(index)
        parentItem = childItem.parent()

        if parentItem == self.rootItem:
            return QModelIndex()
        return self.createIndex(parentItem.child_number(), 0, parentItem)


    def remove_rows(self, position, rows, parent=QModelIndex()):
        parentItem = self.getItem(parent)

        self.beginRemoveRows(parent, position, position + rows - 1)
        success = parentItem.remove_children(position, rows)
        self.endRemoveRows()
        return success


    def remove_row(self, position, parent=QModelIndex()):
        return TreeModel.remove_rows(self, position, 1, parent)


    def clear_rows(self):
        rows = self.rootItem.child_count()
        if rows:
            return TreeModel.remove_rows(self, 0, rows)
        return False


    def rowCount(self, parent=QModelIndex()):
        return self.getItem(parent).child_count()


    def setData(self, index, value, role=Qt.EditRole):
        if role != Qt.EditRole:
            return False

        item = self.getItem(index)
        result = item.setData(index.column(), value)

        if result:
            self.dataChanged.emit(index, index)
        return result


    def setRowData(self, index, values, role=Qt.EditRole):
        if role != Qt.EditRole:
            return False

        item = self.getItem(index)
        result = item.setAllData(values)
        row = index.row()
        cols = item.columnCount()

        row_start = self.index(row, 0, index.parent())
        row_end = self.index(row, cols, index.parent())

        if result:
            self.dataChanged.emit(row_start, row_end)
        return result


    def setHeaderData(self, section, orientation, value, role=Qt.EditRole):
        if role != Qt.EditRole or orientation != Qt.Horizontal:
            return False

        result = self.rootItem.setData(section, value)
        if result:
            self.headerDataChanged.emit(orientation, section, section)
        return result



class TreeSort(QSortFilterProxyModel):
     # Implementing sort filters seems the best way to sort data without modifying it
    def __init__(self, source_model=None, parent=None):
        QSortFilterProxyModel.__init__(self, parent)
        if source_model:
            QSortFilterProxyModel.setSourceModel(self, source_model)


    # you must reimplement filterAcceptsRow and lessThan to subclass QSortFilterProxyModel
    # filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent)
    def filterAcceptsRow(self, sourceRow, sourceParent):
        return True


    def lessThan(self, left, right):
        return left.data() < right.data()


    # other functions
    def getItem(self, proxyModelIndex):
        return self.sourceModel().getItem(self.mapToSource(proxyModelIndex))
