#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import os
import webapp2
from google.appengine.ext.webapp import template

from WSCommLink import *
from WSGMs import *
from WSRolls import *
from WSUsers import *
from WSWidgets import *


'''
						POST		GET			PUT				DELETE
/collection				make new	get all		bulk update		delete all
/collection/item		none		get one		update/error	delete one
'''

class MainHandler(webapp2.RequestHandler):
    def get(self):
		# In general, it's better to serve static content directly
		path = os.path.join(os.path.dirname(__file__), 'templates/test.html')
		self.response.write(template.render(path, {}))

################################################################################

#def handle_errors(request, response, exception):
#	response.headers['Content-Type'] = 'application/json'
#	#response.write(json.dumps({"error": "danger will robinson"}))
#	response.write("Danger will robinson")
#	response.set_status(exception.code)

################################################################################

app = webapp2.WSGIApplication([
	# ('/ws/v1/widgets.json', WSHandleWidgets),
	# ('/ws/v1/widgets.json/(.*)/roll.json', WSHandleRolls),
	# ('/ws/v1/widgets.json/(.*)', WSHandleWidget),

	#('/ws/v1/gms.json', WSHandleGMs),
	('/ws/v1/gms.json/(.*)', WSHandleGM),

	('/ws/v1/users.json', WSHandleUsers),
	('/ws/v1/users.json/(.*)', WSHandleUser),

	# ('/ws/v1/chat.json', WSHandleChat),
	('/ws/v1/roll.json', WSHandleRollsOneOff),
	('/ws/v1/commlink.json', WSCommLink),

	# ('/channel', WSHandleChannel),

	# catchall, probably best to add as a 404
    ('/', MainHandler)
], debug=True)

#app.error_handlers[404] = handle_errors
#app.error_handlers[500] = handle_errors
