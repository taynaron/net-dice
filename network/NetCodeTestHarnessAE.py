#!/usr/bin/python -O
import Queue, thread, time, sys, pprint
from NetCodeAE import NetCodeAE, pyUser
from Logger import SetupLog

quitmain = Queue.Queue()
quitmain.put(False)

def IOLoop(NetCode, swarm):
	while True:
		enteredText=raw_input()

		try:
			# Kill the event loop
			if enteredText == "/exit":
				for a in swarm:
					try:
						print a.StopClient()
					except Exception as e:
						pass
				try:
					print NetCode.StopClient()
				except Exception as e:
					print e
				finally:
					try:
						print NetCode.StopServer()
					except Exception as e:
						print e
					finally:
						print "Exiting"
				quitmain.put(True)
				break

			elif enteredText == "/spam":
				for a in range(500):
					NetCode.ClientSend({"spam": a})

			elif enteredText == "/rollgm":
				print NetCode.RollGM("roll_name", "1d20+[foo]", {"foo": "5"})

			elif enteredText == "/roll":
				print NetCode.Roll("roll_name", "1d20+[foo]", {"foo": "5"}, "*", "")

			elif enteredText == "/list":
				NetCode.GetUsers()

			elif enteredText == "/getgm":
				NetCode.GetGM()

			elif enteredText[:8] == "/session":
				print NetCode.SetSession(enteredText[9:])

			elif enteredText[:5] == "/kick":
				print NetCode.Kick(enteredText[6:])

			elif enteredText[:6] == "/setgm":
				print NetCode.SetGM(enteredText[7:])

			# start the client
			elif enteredText == "/startclient":
				print NetCode.StartClient()

			# kill the client
			elif enteredText == "/stopclient":
				print NetCode.StopClient()

			# how to set the name
			elif enteredText[:5] == "/name":
				#reference_user = NetCode.GetUser()
				#reference_user.name = enteredText[6:]
				print NetCode.SetName(enteredText[6:])

			# how to set the color
			elif enteredText[:6] == "/color":
				#reference_user = NetCode.GetUser()
				#reference_user.color_str = enteredText[7:]
				print NetCode.SetColor(enterText[7:])

			# host/admin function to kick a user
			elif enteredText[:5] == "/kick":
				NetCode.Kick(enteredText[6:])

			# check to see what your name is set to (hopefully to prevent name syncing issues)
			elif enteredText == "/whoami":
				print NetCode.GetUser().name

			elif enteredText == "/amigm":
				print NetCode.GetUser().is_gm

			elif enteredText == "/gambit":
				print NetCode.StartClient()
				time.sleep(.09)
				NetCode.ClientSend({"some_text": "starting"})
				time.sleep(.09)
				NetCode.GetUsers()
				time.sleep(.09)
				NetCode.SetName("foobar")
				time.sleep(.09)
				NetCode.GetUsers()
				time.sleep(.09)
				NetCode.SetName("foobar")
				time.sleep(.09)
				NetCode.GetUsers()
				time.sleep(.09)
				NetCode.SetName("foobar")
				time.sleep(.09)
				NetCode.SetColor("blue")
				time.sleep(.09)
				NetCode.SetColor("red")
				time.sleep(.09)
				NetCode.GetGM()
				time.sleep(.09)
				NetCode.GetUsers()
				time.sleep(.09)
				print NetCode.Roll("roll_name", "1d20+[foo]", {"foo": "5"}, "*", "")
				time.sleep(.09)
				NetCode.SetSession("derfderf")
				time.sleep(.09)
				print NetCode.StopClient()
				print "*** End of Gambit ***"

			elif enteredText == "/startzerg":
				for a in swarm:
					a.StartClient()

			elif enteredText == "/stopzerg":
				for a in swarm:
					a.StopClient()

			# send a message by default
			else:
				NetCode.ClientSend({"some_text": enteredText})
		except Exception as e:
			print "@@@", e.message, e
			#raise e
		time.sleep(.01)

'''
Sample function that polls NetCode.clientInbox for messages and let's the
client respond and process the message, note it can pass objects, not just
strings
'''
def StdOutLoop(NetCode):
	cout = pprint.PrettyPrinter()
	while True:
		if not NetCode.clientInbox.empty():
			cout.pprint(NetCode.clientInbox.get())
		else:
			time.sleep(.01)

def main():
	SetupLog()

	if len(sys.argv) == 3:
		override_user = pyUser(sys.argv[1], "red")
		NetCode = NetCodeAE(override_user, sys.argv[2], session="new_session", host='http://localhost:8080')
	elif len(sys.argv) == 2:
		ae_user = pyUser("foobar", "red")
		NetCode = NetCodeAE(ae_user, session="new_session", host='http://net-dice.appspot.com')
	else:
		local_user = pyUser("foobar", "red")
		NetCode = NetCodeAE(local_user, session="new_session", host='http://localhost:8080')

	swarm = []
	for a in range(5):
		swarm.append(NetCodeAE(
			pyUser=pyUser("swarm_bot_{0}".format(a), "swarm_bot_color_{0}".format(a)),
			guid="swarm_bot_guid_{0}".format(a),
			session="new_session",
			host="http://localhost:8080"))

	# There's a command line option for reading and writing in data
	thread.start_new_thread(IOLoop, (NetCode, swarm))
	thread.start_new_thread(StdOutLoop, (NetCode, ))

	# Start a thread with the reactor that'll handle the netcode
	thread.start_new_thread(NetCode.StartReactor,(False,))
	print "Started Reactor"
	
	while True:
		if quitmain.get():
			break
		quitmain.put(False)
		time.sleep(NetCode.SLEEP_INCREMENT)

if __name__ == "__main__":
	main()
