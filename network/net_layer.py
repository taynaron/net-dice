import logging, os, time, traceback

__QtLib__ = os.environ['QT_API']
if __QtLib__ == 'pyqt':
    from PyQt5.QtCore import pyqtSignal as Signal, QObject, QThread
else:
    from PySide.QtCore import Signal, QObject, QThread

from trees_and_lists.user_list import UserList
import net_dice_helpers as nd_helpers
import net_dice_globals as NDG

from . import NetCode




#two threads: StartReactor and something to read from the queue
class NetLayer(QObject):
    serverStarted = Signal(dict)
    clientStartFailed = Signal(dict)
    serverStartFailed = Signal(dict)
    clientDisconnected = Signal()
    serverDisconnected = Signal()
    rollResultReceived = Signal(dict)
    chatReceived = Signal(dict)

    def __init__(self, ui, local_user, user_list):
        QObject.__init__(self)
        self.ui = ui

        self.local_user = local_user
        logging.info("init: " + str(self.local_user.name))

        # Set up the User List (only applicable in client/server modes)
        self.user_list = user_list

        self.io_thread = IOThread()
        self.reactor_thread = ConnectionThread(self.local_user)
        self.net_connection = False

        self._setup_connections()


    def _setup_connections(self):
        self.io_thread.messageReceived[dict].connect(self.received_message)

        self.ui.usersView.removeUsers[list].connect(self.kick_users)

        self.clientDisconnected.connect(self.user_list.clear)
        self.clientDisconnected.connect(self.ui.chatTreeView.model()._disconnected)


    # the 'start' functions could have more descriptive failure reasons, but this is sufficient to determine whose fault it is.
    def start_client(self):
        try: #finding and preparing the IP and port
            ip = self.local_user.connection_IP.split(':')
            if len(ip) > 2 or len(ip) == 0:
                assert False
            elif len(ip) == 1:
                ip.append(1234)
            else:
                ip[1] = int(ip[1])

            status = self.reactor_thread.connect_to_server(ip[0], ip[1])
            if status:
                self.io_thread.listen()
                self.net_connection = True
            else:
                self.clientStartFailed[dict].emit({'Reason':'NetCode failure'})

            self._reset_user_model()
        except Exception as e:
            traceback.print_exc()
            self.clientStartFailed[dict].emit({'Reason':str(e)})


    def start_server(self):
        try:
            status = self.reactor_thread.setup_server()
            if status:
                self._reset_user_model()

                self.io_thread.listen()
                self.net_connection = True
                self.serverStarted[dict].emit({})
            else:
                self.serverStartFailed[dict].emit({'Reason':'NetCode failure'})
        except Exception as e:
            traceback.print_exc()
            self.serverStartFailed[dict].emit({'Reason':str(e)})


    def _reset_user_model(self):
        self.user_list.reset_user_model(self.local_user)
        self.reactor_thread.set_user_list(self.user_list.user_list)



    def _stop_thing(self):
        if self.net_connection:
            self.reactor_thread.stop_connection()
            self.io_thread.stop()
            self.user_list.clear()
            self.net_connection = False


    def disconnect_from_server(self):
        logging.info("disconnecting from server")
        self._stop_thing()
        self.clientDisconnected.emit()


    def stop_server(self):
        logging.info("stopping server")
        self._stop_thing()
        self.serverDisconnected.emit()


    def stop_all(self):
        self._stop_thing()
        self.clientDisconnected.emit()
        self.serverDisconnected.emit()



    # --- User Info ---
    def kick_users(self, user_list):
        if self.local_user.name in user_list:
            user_list.remove(self.local_user.name)
        for user in user_list:
            self.reactor_thread.kick_user(user)



    # --- Roller Code ---
    def send_roll_to_server(self, roll_dict):
        #use sendRoll to send chats too
        send_dict = {'User Info':{'Name':self.local_user.name, 'Hide Rolls':self.local_user.hidden_rolls}}
        if 'Roll' in roll_dict:
            send_dict['Roll'] = roll_dict
        elif 'Chat' in roll_dict:
            send_dict['Chat'] = nd_helpers.chat_wrap(self.local_user.name) + roll_dict['Chat']
        #send to server
        logging.info("sending roll to server" + str(send_dict))
        self.reactor_thread.client_send_out(send_dict)


    def send_rolls_to_server(self, roll_dict):
        logging.info("sending rolls to server:")
        if 'Rolls' in roll_dict:
            for roll in roll_dict['Rolls']:
                send_dict = {'Roll':roll, 'User Info':{'Name':self.local_user.name, 'Hide Rolls':self.local_user.hidden_rolls}}
                self.reactor_thread.client_send_out(send_dict)


    def set_message_color(self, message):
        if message['User Info']['Name'] == self.local_user.name:
            message['User Info']['Color'] = self.local_user.color
        else:
            user_color = self.user_list.get_user_color(message['User Info']['Name'])
            if user_color:
                message['User Info']['Color'] = user_color
        return message


    # --- Decode Incoming Signals ---
    def received_message(self, roll_dict):
        #Client-side message handling. No error-checking here, that's all handled by NetCode and the server.
        logging.info('received_message: ' + str(roll_dict))

        if isinstance(roll_dict, dict):
            if 'User List' in roll_dict:
                self.user_list.add_users(roll_dict['User List'])
                self.user_list.add_user(self.local_user.get_user_info())
            elif 'New User' in roll_dict:
                self.user_list.add_user(roll_dict['New User'])
            elif 'Edit User' in roll_dict:
                if 'New Name' in roll_dict['Edit User']:
                    self.user_list.change_user_name(roll_dict['Edit User']['Old Name'], roll_dict['Edit User']['New Name'])
                elif 'New Color' in roll_dict['Edit User']:
                    self.user_list.change_user_color(roll_dict['Edit User']['Name'], roll_dict['Edit User']['New Color'])
                elif 'GM' in roll_dict['Edit User']:
                    self.user_list.set_gm(roll_dict['Edit User']['Name'])
            # This is to ensure status information for clients is updated correctly by identifying the user who disconnected
            elif 'Dead User' in roll_dict:
                self.user_list.remove_user(roll_dict['Dead User']['Name'])
                if roll_dict['Dead User']['Name'] == self.local_user.name:
                    self.ui.statusBar.showMessage("You got kicked! Reason: "+str(roll_dict['Dead User']['Reason']))
                    self.disconnect_from_server()
            elif 'Roll' in roll_dict:
                roll_dict = self.set_message_color(roll_dict)
                self.rollResultReceived[dict].emit(roll_dict)
            elif 'Chat' in roll_dict:
                roll_dict = self.set_message_color(roll_dict)
                self.chatReceived[dict].emit(roll_dict)
            else:
                logging.info("received_message: unused input: " + str(roll_dict))
        else:
            logging.info("received_message: malformed input")




from dice_roller import DiceRoller

class ConnectionThread(QThread):
    clientStarted = Signal(dict)
    clientStopped = Signal(dict)

    def __init__(self, local_user, parent = None):
        QThread.__init__(self, parent)

        self.roller = DiceRoller()

        self.local_user = local_user
        self.old_name = self.local_user.name

        self.user_list = None

        self.make_server = False
        self.exiting = False


    def _setup_connections(self):
        self.local_user.nameChanged.connect(self.update_name)
        self.local_user.colorChanged.connect(self.update_color)
        self.user_list.gmChanged.connect(self.update_gm)


    # --- Setup Stuff ---

    def set_user_list(self, user_list):
        self.user_list = user_list
        self._setup_connections()


    def setup_server(self):
        self.make_server = True
        status = NetCode.StartServer(server_port=self.local_user.server_port)
        logging.info("server status: " + str(status))
        if status:
            self.start()
            return self.connect_to_server('localhost', self.local_user.server_port)
        return False


    def connect_to_server(self, ip, port):
        logging.info("connecting to client: " + str(ip) + ":" + str(port))
        status = NetCode.StartClient(host=ip, server_port=port)
        logging.info("client status: " + str(status))
        if status: #this doesn't actually mean everything is set up.
            self.start()
            return True
        return False


    def stop_connection(self):
        # TODO replace with kick code
        cli = NetCode.StopClient()
        serv = True
        if self.make_server:
            serv = NetCode.StopServer()
        self.stop()
        return cli and serv


    def update_name(self, force = False):
        # NetCode handles the duplicate names, so we don't need to duplicate that here
        if NetCode.isClientRunning():
            if force or self.local_user.name != self.old_name:
                NetCode.SetName(self.local_user.name) #needed for syncing things up with the roller code
        else:
            self.old_name = self.local_user.name


    def update_color(self):
        if NetCode.isClientRunning():
            self.client_send_out({'Edit User':{'Name':self.local_user.name, 'New Color':self.local_user.color}})


    def update_gm(self, new_gm_name):
        if NetCode.isClientRunning():
            self.client_send_out({'Edit User':{'Name':new_gm_name, 'GM':True}})


    def client_send_out(self, send_dict):
        if NetCode.isClientRunning():
            NetCode.ClientSend(send_dict)


    def server_send_out(self, send_dict, command='all', name=''):
        # command could be all, only, not
        # name means nothing if command is all
        if NetCode.isServerRunning():
            NetCode.ServerSend(command, name, send_dict)


    def kick_user(self, user_name, reason = "I felt like it"):
        if NetCode.isServerRunning() and self.make_server and user_name and user_name != self.local_user.name:
            logging.info("Server Kicked "+user_name+" : "+str(reason))
            self.server_send_out({'Dead User':{'Name':user_name, 'Reason':reason}})


    def exit(self):
        self.stop_connection()
        self.exiting = True


    # --- Message Handling ---
    def roll_from_signal(self, dice_dict):
        # we assume that, since the roll was checked locally, it's fine here.
        # we therefore only do minimal error checking

        roll_str = dice_dict['Roll']
        roll_widgets = dice_dict['Roll Widgets']
        try:
            result = self.roller.evalRoll(roll_str, roll_widgets)

            if not result:
                raise Exception("roll_from_signal failed; roller returned no reason")

            return {'Roll':result, 'Roll Name':dice_dict['Roll Name']}
        except:
            logging.warn("Server failed to roll: " + str(dice_dict))
            return False



    def ServerReceiverHandler(self, object, results):
        # handles all the server-side message-passing functionality
        # determines what gets sent to clients

        '''print "object ", object.getName()
        print "clients ",
        for client in object.factory.clientProtocols:
            print client.getName(),
        print

        print results'''

        # Because objects can get passed through, it's safer to test for the type
        if isinstance(results,dict):
            if 'Roll' in results:
                results.update( self.roll_from_signal(results['Roll']) )

            elif 'User' in results:
                if not ('App Version' in results['User'] and \
                        nd_helpers.equal_versions(results['User']['App Version'])):
                    logging.info(results['User']['Name']+" refused: version mismatch")
                    self.kick_user(results['User']['Name'], reason="Client Version Too Old")
                else:
                    # a new user might initially send us a name change dictionary if the server edited
                    # their name before net_layer noticed them. We account for that here.
                    user_info = results['User']
                    if 'Old Name' in user_info and str(object.getName()) == user_info['New Name']:
                        logging.info("ServerReceiverHandler: old "+str(user_info['Old Name'])+"/ new "+str(user_info['New Name']))

                        if self.user_list.user_exists(user_info['New Name']):
                            logging.warn("ServerReceiverHandler: New Name exists - shouldn't happen")
                            return
                        elif self.user_list.user_exists(user_info['Old Name']):
                            logging.info("ServerReceiverHandler: old exists")
                            self.server_send_out({'Edit User':user_info})
                        else:
                            logging.info("ServerReceiverHandler: new user: "+str(user_info['New Name']))
                            user_info['Name'] = user_info['New Name']
                            del user_info['Old Name']
                            del user_info['New Name']

                            self.server_send_out({'New User':user_info})
                            self.send_user_list(object.getName())

                    else:
                        if 'Name' in user_info and self.user_list.user_exists(user_info['Name']):
                            self.server_send_out({'Edit User':user_info})
                        else:
                            if self.user_list.is_empty():
                                logging.info("ServerReceiverHandler: Set GM User")
                                user_info['GM'] = True
                                user_info['Server'] = True
                            self.server_send_out({'New User':user_info})
                            self.send_user_list(object.getName())

            if ('Roll' in results or 'Chat' in results) and results['User Info']['Hide Rolls']:
                self.server_send_out(results, command='only', name=results['User Info']['Name'])
                gm = self.user_list.get_gm_name()
                if gm and gm != results['User Info']['Name']:
                    self.server_send_out(results, command='only', name=gm)
            else:
                self.server_send_out(results)
        else:
            logging.warn("malformed input: " + str(results))


    def send_user_list(self, user_name):
        logging.info("send_user_list: " + str(user_name))
        if NetCode.isServerRunning():
            users = self.user_list.get_user_dict()
            if users != {}:
                self.server_send_out({'User List':users}, name=user_name, command='only')


    # --- Naming ---

    def ClientNameHandler(self, object, old_name):
        #When the NetCode server blesses the client's name, it sends the name here for syncing.
        #We then send it to the net_layer server for broadcasting
        new_name = str(object.getName())
        user_dict = self.local_user.get_user_info()
        user_dict['App Version'] = NDG.VERSION_NUMBER
        if new_name != self.old_name:
            if self.old_name and not old_name:
                old_name = self.old_name
            logging.info("ClientNameHandler: Set name from " + old_name + " to " + new_name)
            del user_dict['Name']
            user_dict['Old Name'] = old_name
            user_dict['New Name'] = new_name
            self.old_name = new_name

            if new_name != self.local_user.name:
                self.local_user._set_name( new_name )
                logging.info("ClientNameHandler: Name has been set to " + new_name)

        logging.info("ClientNameHandler: User dict sent: " + str(user_dict))
        self.client_send_out({'User':user_dict})


    # --- Connection Handling ---
    def ClientConnectHandler(self, object):
        #The server has responded to us and told us that our connection is set up properly
        logging.info("Client connected: " + str(object.getName()))

        #UI layer events
        self.clientStarted[dict].emit({})
        self.update_name(force=True)#on the initial connection we ensure we send a name


    def ClientDisconnectHandler(self, object, reason):
        logging.info("Client disconnected: " + str(object.getName()) + " : " + str(reason.getErrorMessage()))
        self.clientStopped[dict].emit({})


    def ServerConnectHandler(self, object):
        logging.info("Server got a connection: " + str(object.getName()))


    def ServerDisconnectHandler(self, object, reason):
        logging.info("Server lost a connection: " + str(object.getName()) + " : " + str(reason.getErrorMessage()))
        self.server_send_out({'Dead User':{'Name':object.getName()}})


    # --- Thread Controller Stuff ---
    def run(self):
        # Connect our functions to NetCode's watchers
        NetCode.clientConnectEvents.append(self.ClientConnectHandler)
        NetCode.clientDisconnectEvents.append(self.ClientDisconnectHandler)
        NetCode.clientNameEvents.append(self.ClientNameHandler)

        NetCode.serverConnectEvents.append(self.ServerConnectHandler)
        NetCode.serverDisconnectEvents.append(self.ServerDisconnectHandler)
        NetCode.serverReceiveEvents.append(self.ServerReceiverHandler)
        NetCode.StartReactor(inMain=False)

        while not self.exiting:
            time.sleep(NetCode.SLEEP_INCREMENT)


    def stop(self):
        self.exiting = True


    def __del__(self):
        self.exiting = True
        self.wait()




class IOThread(QThread):
    messageReceived = Signal(dict)

    def __init__(self, parent = None):
        QThread.__init__(self, parent)
        self.sleep_time = NetCode.SLEEP_INCREMENT
        self.exiting = False


    def listen(self):
        self.exiting = False
        self.start()


    def sort_messages(self):
        if not NetCode.clientInbox.empty():
            inboxitem = NetCode.clientInbox.get()
            self.messageReceived[dict].emit(inboxitem)


    def run(self):
        while not self.exiting:
            self.sort_messages()
            time.sleep(self.sleep_time)


    def stop(self):
        self.__del__()


    def __del__(self):
        self.exiting = True
        self.wait()
