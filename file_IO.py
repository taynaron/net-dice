import pickle, json, os
import logging

__QtLib__ = os.environ['QT_API']
if __QtLib__ == 'pyqt':
    from PyQt5.QtCore import QByteArray
else:
    from PySide.QtCore import QByteArray

'''
Helper functions for reading and saving files.
Used for prefs and widgets.
'''
def read_file(filename):
    # try:
    #     logging.info("read_file: Attempting to open file: " + str(filename))
    #     f = open(filename, 'r')
    # except Exception as e:
    #     logging.info("read_file: Failed to open file as text: " + str(filename))
    #     # return False

    with open(filename, 'r') as f:
        try:
            file_dict = json.load(f)
            return file_dict
        except Exception as e:
            logging.info("read_file: Couldn't open as JSON ({0}), trying pickle: {1}".format(e, filename))

    with open(filename, 'rb') as f:
        try:
            file_dict = pickle.load(f, encoding='latin1')

            return file_dict
        except Exception as e:
            logging.info("read_file: Couldn't open as pickled text ({0}), trying pickled bytes: {1}".format(e, filename))

    with open(filename, 'rb') as f:
        try:
            file_dict = pickle.load(f.decode())
            return file_dict
        except Exception as e:
            logging.info("read_file: Couldn't open as pickled bytes ({0}): {1}".format(e, filename))

    logging.info("read_file: Failed to read file: {0}".format(filename))
    return False


def attrs_to_bytes(in_dict, attrs):
    assert isinstance(attrs, list)

    # for converting from the python 2 style prefs
    for attr in attrs:
        if attr in in_dict and not isinstance(in_dict[attr], (bytes, bytearray, QByteArray)):
            # print(attr, in_dict[attr], type(in_dict[attr]))
            in_dict[attr] = bytes(in_dict[attr], encoding="latin1")


def save_file(filename, file_dict, serialize=False):
    try:
        logging.info("save_file: Attempting to save to file: {0}, {1}".format(filename, file_dict))
        # f = open(filename, 'wb')
        if serialize:
            with open(filename, 'wb') as f:
                pickle.dump(file_dict, f)
        else:
            with open(filename, 'w') as f:
                json.dump(file_dict, f, indent=4)
        return True
    except Exception as e:
        logging.info("save_file: Failed to save file ({0}): {1} ".format(e, filename))
        return False
