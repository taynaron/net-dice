# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui/widgetEdit.ui'
#
# Created by: PyQt5 UI code generator 5.7
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_widgetEditDialog(object):
    def setupUi(self, widgetEditDialog):
        widgetEditDialog.setObjectName("widgetEditDialog")
        widgetEditDialog.setWindowModality(QtCore.Qt.WindowModal)
        widgetEditDialog.resize(400, 150)
        widgetEditDialog.setSizeGripEnabled(True)
        widgetEditDialog.setModal(True)
        self.verticalLayout = QtWidgets.QVBoxLayout(widgetEditDialog)
        self.verticalLayout.setContentsMargins(6, 6, 6, 6)
        self.verticalLayout.setSpacing(3)
        self.verticalLayout.setObjectName("verticalLayout")
        self.widgetNameLabel = QtWidgets.QLabel(widgetEditDialog)
        self.widgetNameLabel.setObjectName("widgetNameLabel")
        self.verticalLayout.addWidget(self.widgetNameLabel)
        self.rollTextEdit = rollTextEdit(widgetEditDialog)
        self.rollTextEdit.setMinimumSize(QtCore.QSize(0, 27))
        self.rollTextEdit.setObjectName("rollTextEdit")
        self.verticalLayout.addWidget(self.rollTextEdit)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setSpacing(4)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.rollRevertButton = QtWidgets.QPushButton(widgetEditDialog)
        self.rollRevertButton.setObjectName("rollRevertButton")
        self.horizontalLayout.addWidget(self.rollRevertButton)
        self.rollValidLabel = QtWidgets.QLabel(widgetEditDialog)
        self.rollValidLabel.setText("")
        self.rollValidLabel.setScaledContents(True)
        self.rollValidLabel.setWordWrap(True)
        self.rollValidLabel.setObjectName("rollValidLabel")
        self.horizontalLayout.addWidget(self.rollValidLabel)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.buttonBox = QtWidgets.QDialogButtonBox(widgetEditDialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.horizontalLayout.addWidget(self.buttonBox)
        self.verticalLayout.addLayout(self.horizontalLayout)

        self.retranslateUi(widgetEditDialog)
        self.buttonBox.accepted.connect(widgetEditDialog.accept)
        self.buttonBox.rejected.connect(widgetEditDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(widgetEditDialog)

    def retranslateUi(self, widgetEditDialog):
        _translate = QtCore.QCoreApplication.translate
        widgetEditDialog.setWindowTitle(_translate("widgetEditDialog", "Edit Your Widget"))
        self.widgetNameLabel.setText(_translate("widgetEditDialog", "Edit Your Widget: "))
        self.rollRevertButton.setToolTip(_translate("widgetEditDialog", "Reverts to the last valid state entered"))
        self.rollRevertButton.setText(_translate("widgetEditDialog", "Revert"))

from .ui_mods.roll_text_edit import rollTextEdit
