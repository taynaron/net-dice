import os

__QtLib__ = os.environ['QT_API']
if __QtLib__ == 'pyqt':
    from PyQt5.QtCore import pyqtSignal as Signal, Qt
    from PyQt5.QtWidgets import QDialog

    from ui.emailEdit_pyqt import Ui_emailEditDialog
else:
    from PySide.QtCore import Signal, Qt
    from PySide.QtGui import QDialog

    from ui.emailEdit_pyside import Ui_emailEditDialog

import net_dice_helpers as nd_helpers




# Mostly sets up info for roll_text_edit
class emailEditDialog(QDialog):

    sendEmail = Signal()

    def __init__(self, recipients, email_txt, is_bcc, parent=None):
        # if we are passed a parent, this window can be a sheet. Neat!
        QDialog.__init__(self, parent, Qt.Sheet)

        self.ui = Ui_emailEditDialog()
        self.ui.setupUi(self)

        self.ui.emailTextEdit.setPlainText(email_txt)
        cursor = self.ui.emailTextEdit.textCursor()
        cursor.setPosition(len(email_txt) - 1)
        self.ui.emailTextEdit.setTextCursor(cursor)

        buttons = self.ui.sendButtonBox.buttons()
        for button in buttons:
            if button.text() == "OK":
                button.setText("Send")
                button.clicked.connect(self.email_check)

        senders_text = nd_helpers.color_text(recipients, "#666666") if is_bcc else recipients
        self.ui.sendToLabel.setText("Sending To: " + senders_text)
        self.ui.emailTextEdit.setFocus()


    def email_check(self):
        # validate before accepting dialog
        password = str(self.ui.passwordEdit.text())
        # TODO: make this put the error in a more sensible location, not close the message window
        if not password:
            # TODO: add an error message somewhere
            self.ui.passwordEdit.setStyleSheet("background-color: rgb(255, 180, 180);")
            self.ui.errorLabel.setText("Error: Password Required")
            return

        self.ui.passwordEdit.setStyleSheet("background-color: white;")
        self.ui.errorLabel.setText("Sending...")

        self.sendEmail.emit()

        # QDialog.accept(self)
