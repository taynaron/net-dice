import logging, os

__QtLib__ = os.environ['QT_API']
if __QtLib__ == 'pyqt':
    from PyQt5.QtCore import Qt, QObject
else:
    from PySide.QtCore import Qt, QObject

#from list_tree import ListModel, ListView
from .email_list_model import EmailModel
from .base_tree_model import TreeSort
from ui.ui_mods.email_edit_dialog import emailEditDialog

from threads.email_thread import EmailThread

import net_dice_globals as NDG


class EmailError(Exception):
    pass




FOOTER = '''

-----
Sent from NetDice, the free networkable dice roller: ''' + NDG.website

class EmailList(QObject):
    def __init__(self, ui):
        QObject.__init__(self)
        self.ui = ui
        self.email_list = EmailModel()
        self.email_sort = TreeSort(source_model=self.email_list)

        self.email_win = None

        self.sender = EmailThread()
        self.ui.emailView.setModel(self.email_sort)

        self._setup_connections()


    def _setup_connections(self):
        self.ui.sendButton.clicked.connect(self.open_email_dialog)
        self.sender.emailSent.connect(self._email_sent)
        self.sender.emailSendError[str, Exception].connect(self._email_error)


    def clear(self):
        self.email_list.clear_rows()


    def get_email_dict(self):
        email_dict = self.email_list.get_email_dict()
        email_dict["__sender_info__"] = {'Sender': str(self.ui.emailSenderEdit.text()),
                                         'Domain': self.ui.domainComboBox.currentIndex(),
                                         'History': bool(self.ui.historyCheckBox.checkState()),
                                         'BCC': bool(self.ui.bccCheckBox.checkState())}
        return email_dict


    def set_emails_from_dict(self, email_dict):
        self.clear()
        ui_info = email_dict["__sender_info__"]
        self.ui.emailSenderEdit.setText(ui_info['Sender'])
        self.ui.domainComboBox.setCurrentIndex(ui_info['Domain'])
        self.ui.historyCheckBox.setCheckState(Qt.Checked if ui_info['History'] else Qt.Unchecked)
        self.ui.bccCheckBox.setCheckState(Qt.Checked if ui_info['BCC'] else Qt.Unchecked)
        del email_dict["__sender_info__"]

        self.email_list.set_emails_from_dict(email_dict)


    def get_email_text(self):
        chat = self.ui.chatTreeView.model()
        email_txt = chat.text_tree(self.ui.historyCheckBox.checkState())
        if not email_txt or email_txt == "Rolls":
            raise EmailError("Can't send an empty E-Mail.")
        return email_txt


    def get_sender_text(self):
        sender_txt = str(self.ui.emailSenderEdit.text()).strip()
        if not sender_txt:
            raise EmailError("Must provide a sending email address.")

        return sender_txt + "@" + self.get_domain()


    def get_domain(self):
        return str(self.ui.domainComboBox.currentText())[1:]


    def get_recipients_list(self):
        recipients_dict = self.email_list.get_email_dict()
        return [email for email in recipients_dict if recipients_dict[email]['Enabled']]


    def get_recipients_txt(self):
        recipients_dict = self.email_list.get_email_dict()
        recipients_txt_list = []

        for email in recipients_dict:
            if recipients_dict[email]['Enabled']:
                recipients_txt_list.append("{name}({email})".format(name=recipients_dict[email]['Player Name'], email=email))
        recipients_txt = ", ".join(recipients_txt_list)

        if not recipients_txt:
            raise EmailError("Must send an email to at least one recipient.")

        return recipients_txt


    def open_email_dialog(self):
        try:
            email_txt = self.get_email_text()
            sender_txt = self.get_sender_text()

            domain = self.get_domain()

            recipients_list = self.get_recipients_list()
            recipients_txt = self.get_recipients_txt()
        except EmailError as e:
            self.ui.statusBar.showMessage(str(e))
            return

        self.email_win = emailEditDialog(recipients_txt, email_txt, self.ui.bccCheckBox.checkState(), self.ui.emailView)
        self.email_win.sendEmail.connect(self.send_email)
        self.email_win.open()


    def send_email(self):
        assert self.email_win, "Must call with valid emailEditDialog set to self.email_win."
        email_txt = str(self.email_win.ui.emailTextEdit.toPlainText()).strip()
        email_txt += FOOTER

        name = str(self.ui.userNameEdit.text())
        password = str(self.email_win.ui.passwordEdit.text())

        try:
            sender_txt = self.get_sender_text()
            domain = self.get_domain()

            recipients_list = self.get_recipients_list()
            recipients_txt = self.get_recipients_txt()
        except EmailError as e:
            self.ui.statusBar.showMessage(str(e))
            return

        subject = str(self.email_win.ui.emailSubject.text()).strip()
        if not subject:
            subject = str(self.email_win.ui.emailSubject.placeholderText())

        bcc = bool(self.ui.bccCheckBox.checkState())

        recipients_txt = recipients_txt.replace('(', '<')
        recipients_txt = recipients_txt.replace(')', '>')

        # self.email_win = None
        # self.version_thread.emailSendError[str, Exception].connect(self._update_error)

        self.sender.send_email(name, sender_txt, password,
                               recipients_list, recipients_txt,
                               subject, email_txt, domain, bcc)

        # self.email_win.ui.errorLabel.setText("Sending...")

        # self.email_win.emailSendError


    def email_sent(self, response):
        self.email_win = None
        self.email_win.accept()


    def _email_error(self, where, err):
        msg = where + ": " + str(err)
        # self.ui.statusBar.showMessage(msg)
        logging.error(msg)

        # print('str', str(err.smtp_error))
        # print(str(err.smtp_error).split('. ')[0])
        err_txt = str(err.smtp_error).split('. ')[0][8:]  # gets rid of a lot of human-unfriendly text.

        self.email_win.ui.errorLabel.setText("Error: {err}".format(err=err_txt))


    def _email_sent(self):
        msg = "Email Sent Successfully"
        self.ui.statusBar.showMessage(msg)
        logging.error(msg)
