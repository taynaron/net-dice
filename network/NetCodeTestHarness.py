#!/usr/bin/python -O
import queue, _thread, time
from . import NetCode

quitmain = queue.Queue()
quitmain.put(False)

def IOLoop():
	while True:
		enteredText=input()
		# Kill the event loop
		if enteredText == "/exit":
			print(NetCode.StopClient())
			print(NetCode.StopServer())
			quitmain.put(True)
			break

		# start both the server and the client (good for testing)
		elif enteredText == "/start":
			print(NetCode.StartServer())
			print(NetCode.StartClient())

		elif enteredText == "/metadata":
			NetCode.SetMetaData({"color": "255,255,255"})
			NetCode.RequestMetaData()

		elif enteredText == "/hop":
			NetCode.StopClient()
			NetCode.StartClient()

		elif enteredText == "/gambit":
			print(NetCode.StartServer())
			time.sleep(.75)
			print(NetCode.StartClient())
			time.sleep(.75)
			print(NetCode.userName)
			time.sleep(.75)
			NetCode.ClientSend({"name":"Foobar"})
			NetCode.ClientSend({"name":"Foobar"})
			time.sleep(.75)
			NetCode.ClientSend("Test")
			time.sleep(.75)
			NetCode.ClientSend("/list")
			time.sleep(.75)
			print(NetCode.isServerRunning())
			time.sleep(.75)
			NetCode.ClientSend({"host":""})
			time.sleep(.75)
			NetCode.serverfactory.kickClient("Foobar")
			time.sleep(.75)
			print(NetCode.StartClient())
			time.sleep(.75)
			print(NetCode.StopClient())
			time.sleep(.75)
			print(NetCode.StopServer())
			time.sleep(.75)
			quitmain.put(True)
			break

		# start the server
		elif enteredText == "/startserver":
			print(NetCode.StartServer())

		# start the client
		elif enteredText == "/startclient":
			print(NetCode.StartClient())

		# start the client connecting to Ben's IP address
		elif enteredText == "/startben":
			print(NetCode.StartClient("64.81.71.137"))

		# kill the server
		elif enteredText == "/stopserver":
			print(NetCode.StopServer())

		# kill the client
		elif enteredText == "/stopclient":
			print(NetCode.StopClient())

		# how to set the name
		elif enteredText[:5] == "/name":
			NetCode.SetName(enteredText[6:])

		# host/admin function to kick a user
		elif enteredText[:5] == "/kick":
			NetCode.serverfactory.kickClient(enteredText[6:])

		# test code to send a large block of text
		elif enteredText == "/max":
			NetCode.ClientSend("".join(["a" for a in range(10000)]))

		# check to see if the server is running (status message updates)
		elif enteredText == "/amihost":
			print(NetCode.isServerRunning())

		# check to see what your name is set to (hopefully to prevent name syncing issues)
		elif enteredText == "/whoami":
			print(NetCode.userName)

		# check to see who the host user is
		elif enteredText == "/host":
			NetCode.ClientSend({"host":""})

		# send a message by default
		else:
			NetCode.ClientSend(enteredText)
		time.sleep(.01)

'''
Sample function that polls NetCode.clientInbox for messages and let's the
client respond and process the message, note it can pass objects, not just
strings
'''
def StdOutLoop():
	while True:
		if not NetCode.clientInbox.empty():
			print(NetCode.clientInbox.get())
		else:
			time.sleep(.01)

'''
Sample function that responds to when the server gets a message and can act to
either send something over the wire or locally handle the message
'''
def ServerReceiverHandler(object, results):
	# Note that name is set by passing in a dictionary object with a name
	
	# Because objects can get passed through, it's safer to test for the 
	# type
	if isinstance(results, str):
		# Server side function to grab the list of users and return to sender
		if results == "/list":
			users = []
			for client in object.factory.clientProtocols:
				users.append(client.getName())
			NetCode.ServerSend("only", object.getName(), users)

		# whisper functions from 1:1
		elif results[:8] == "/whisper":
			name = results[9:].split(" ", 1)
			NetCode.ServerSend("only", name[0], object.getName()+">>>"+name[1])

		# send a large message from the server
		elif results == "/max_server":
			max_throughput = "".join(["a" for a in range(10000)])
			NetCode.ServerSend("all", object.getName(), max_throughput[:(len(max_throughput)*125)/128])

		# send lots of messages, to check if they come back in order
		elif results == "/spam":
			for a in range(10):
				NetCode.ServerSend("all", object.getName(), object.getName()+">>>"+a)

		elif results == "/spammetadata":
			object.factory.sendAllMetadata()

		# default to relaying to everyone
		else:
			NetCode.ServerSend("all", object.getName(), object.getName()+">>>"+results)

'''
Examples of hooks to respond to different events with the netcode
'''
def ClientConnectHandler(object):
	print("Client connected")

def ClientDisconnectHandler(object, reason):
	print("Client disconnected:", reason.getErrorMessage())

def ServerConnectHandler(object):
	print("Server got a connection")

def ServerDisconnectHandler(object, reason):
	print("Server lost a connection:", reason.getErrorMessage())
	
def ClientNameHandler(object, old_name):
	print("Set name to", object.getName(), "from", old_name)

def main():
	# There's a command line option for reading and writing in data
	_thread.start_new_thread(IOLoop, ())
	_thread.start_new_thread(StdOutLoop, ())

	# Register the call backs with the NetCode
	NetCode.clientConnectEvents.append(ClientConnectHandler)
	NetCode.clientDisconnectEvents.append(ClientDisconnectHandler)
	NetCode.clientNameEvents.append(ClientNameHandler)
	NetCode.serverConnectEvents.append(ServerConnectHandler)
	NetCode.serverDisconnectEvents.append(ServerDisconnectHandler)
	NetCode.serverReceiveEvents.append(ServerReceiverHandler)

	# Start a thread with the reactor that'll handle the netcode
	_thread.start_new_thread(NetCode.StartReactor,(False,))
	print("Started Reactor")
	
	while True:
		temp = quitmain.get()
		if temp:
			break
		quitmain.put(False)
		time.sleep(NetCode.SLEEP_INCREMENT)

if __name__ == "__main__":
	main()
