import logging, os

__QtLib__ = os.environ['QT_API']
if __QtLib__ == 'pyqt':
    from PyQt5.QtCore import QThread, pyqtSignal as Signal
else:
    from PySide.QtCore import QThread, Signal

__all__ = ['logging', 'os', 'QThread', 'Signal']
