import Queue
import copy
import json
import logging
import thread
import time
import urllib
import urllib2

# Instead of separate datamembers in NetCodeAE, it's better to group everything
# together as a class (maybe a struct?) and pass that in and out
class pyUser:
	def __init__(self, name="", color=""):
		self.name = name
		self.color_str = color
		self.is_gm = False

class NetCodeAE:
	def __init__(self, pyUser, guid=str(int(time.time())), session="default", host="http://net-dice.appspot.com"):
		self.base_url = host
		self.is_client_running = False

		# replace separate properties for a single user object
		self.pyUser = pyUser

		self.guid = guid
		self.session = session
		self.SLEEP_INCREMENT = 0.10
		self.clientInbox = Queue.Queue()
		# figure out if can deprecate
		self.clientOutbox = Queue.Queue()
		self.poll_thread = None
		self.log_marker = -1
		logging.info("Creating an instance of NetCodeAE")

	def __send__(self, method='GET', path="/ws/v1/commlink.json", msg={}):
		logging.info("Attempting to send: " + str(msg))
		url = self.base_url + path
		if method in ['GET', 'DELETE']:
			data = urllib.urlencode(msg)
			if len(data):
				url += '?' + urllib.urlencode(msg)
			request = urllib2.Request(url)
			request.get_method = lambda: method
		elif method in ['POST', 'PUT']:
			data = urllib.urlencode(msg)
			request = urllib2.Request(url, data=data)
			request.get_method = lambda: method
			request.add_header('Content-type', 'application/x-www-form-urlencoded')

		try:
			response = urllib2.urlopen(request)
			result = json.loads(response.read())
			result["Code"] = 200
			return result
		except urllib2.HTTPError as e:
			result = {"Code": e.getcode(), "message": e.read()}
			logging.info(result)
			return result
		except Exception as e:
			logging.info("Random exception")
			return {"Code": 999, "message": e.message}

	def __poll_thread__(self):
		while self.is_client_running:
			msg = {"guid": self.guid, "session": self.session, "log_marker": self.log_marker}
			result = self.__send__('GET', msg=msg)
			success = lambda res: res["Code"] == 200 and "Result" in res
			dev_msg = lambda res, msg: "dev_message" in res and res["dev_message"] == msg

			if success(result) and dev_msg(result["Result"], "user not present"):
				logging.info("Client was kicked by GM")
				self.is_client_running = False
				self.pyUser.is_gm = False # not always needed but this way is 100% safe
			elif success(result) and "dev_message" in result["Result"]:
				logging.info("Dev message: " + str(result["Result"]))
				if "log_marker" in result:
					self.log_marker = result["log_marker"]
				if "Is GM" in result["Result"]:
					self.pyUser.is_gm = result["Result"]["Is GM"]
			elif success(result) and not dev_msg(result["Result"], ""):
				logging.info("Received msg: " + str(result["Result"]))
				if "log_marker" in result:
					self.log_marker = result["log_marker"]
					del result["log_marker"]
				self.clientInbox.put(result)

	def isServerRunning(self):
		return False

	def isClientRunning(self):
		return self.is_client_running

	def StartServer(self):
		return True

	def StopServer(self):
		return True

	def _client_running(func):
		def w(self, *args, **kargs):
			if not self.isClientRunning():
				logging.info("Client is not running")
				raise AssertionError("Client not running")
			return func(self, *args, **kargs)
		return w

	def StartClient(self, name="", session=""):
		if name != "":
			self.pyUser.name = name
		if session != "":
			self.session = session

		if self.isClientRunning():
			logging.info("The client is already running")
			raise AssertionError("The client is already running")
	
		msg = {
			"guid": self.guid,
			"name": self.pyUser.name,
			"session": self.session,
			"color": self.pyUser.color_str
			}
		result = self.__send__('POST', "/ws/v1/users.json", msg=msg)
		if result["Code"] == 200 and result["dev_message"] != "user already exists":
			self.is_client_running = True
			# TODO throw some local event since the name could be different
			# from the proposed name
			self.pyUser.name = result["name"]
			self.pyUser.is_gm = result["Is GM"]
			self.log_marker = result["log_marker"]

			# There seems to be a delay in the database side of things
			# add a small pause to let the database actually populate

			# might be moot if memcache is working
			time.sleep(1)
			logging.info("Starting polling thread loop")
			self.poll_thread = thread.start_new_thread(self.__poll_thread__, ())
			return True
		else:
			logging.info("Error starting client")
			raise IOError("Error starting client: " + str(result))

	@_client_running
	def StopClient(self):
		msg = {"guid": self.guid, "session": self.session}
		result = self.__send__('DELETE', "/ws/v1/users.json/" + self.guid, msg=msg)
		if result["Code"] == 200:
			self.is_client_running = False
			self.pyUser.is_gm = False
			time.sleep(1)
			logging.info("Client was stopped")
			return True
		else:
			logging.info("Error stopping client")
			raise IOError("Error stopping client: " + str(result))

	def StartReactor(self, inMain=True):
		return True

	@_client_running
	def ClientSend(self, msg_, white="*", black=""):
		msg = {
			"session": self.session, 
			"is_from": self.pyUser.name,
			"white": white,
			"black": black,
			"message": json.dumps(msg_)
			}
		result = self.__send__('PUT', msg=msg)
		if result["Code"] == 200:
			logging.info("Client sent msg")
			return True
		else:
			logging.info("Error sending message")
			raise IOError("Error sending message: " + str(result))

	def ServerSend(self, action, who, line):
		raise NotImplementedError("No server side comments for AppEngine")

	def SetSession(self, session):
		if not self.isClientRunning():
			logging.info("Client not running, setting local session name only")
			self.session = session
			return True
		# Seems a bit hack-ish, see if this works or if we need a better solution
		result = self.StopClient()
		time.sleep(2)
		self.session = session
		result2 = self.StartClient()
		if result2:
			logging.info("Changed session to {0}".format(session))
			return True
		raise RuntimeError("Failed to restart client")

	# this doesn't work with complex objects because of referencing
	def SetUser(self, pyUser):
		if pyUser.name != self.pyUser.name:
			self.SetName(pyUser.name)
		if pyUser.color_str != self.pyUser.color_str:
			self.SetColor(pyUser.color_str)
		return True

	def GetUser(self):
		return copy.deepcopy(self.pyUser)

	def SetName(self, name):
		if not self.isClientRunning():
			logging.info("Client not running, setting local name only")
			self.pyUser.name = name
			return True
		# Consider putting this through a thread
		# As is, this blocks when changing your name
		msg = {"name": name, "session": self.session, "guid": self.guid}
		result = self.__send__('PUT', "/ws/v1/users.json/" + self.guid, msg)
		if result["Code"] == 200:
			self.pyUser.name = result["New User"]
			logging.info("Setting name: " + str(self.pyUser.name))
			return True
		else:
			logging.info("Error sending name change request")
			raise IOError("Error sending name change request: " + str(result))

	def SetColor(self, color):
		if not self.isClientRunning():
			logging.info("Client not running, setting local color only")
			self.pyUser.color_str = color
			return True

		msg = {"color": color, "session": self.session, "guid": self.guid}
		result = self.__send__('PUT', "/ws/v1/users.json/" + self.guid, msg)
		if result["Code"] == 200:
			self.pyUser.color_str = result["New Color"]
			logging.info("Setting color: " + str(self.pyUser.color_str))
			return True
		else:
			logging.info("Error sending color change request")
			raise IOError("Error sending color change request: " + str(result))
	
	def SetMetaData(self, data):
		pass

	def RequestMetaData(self):
		return None

	@_client_running
	def GetUsers(self):
		msg = {"guid": self.guid, "session": self.session}
		result = self.__send__('GET', "/ws/v1/users.json", msg)
		if result["Code"] == 200:
			logging.info("Sending request for the user list")
			return True
		else:
			logging.info("Error sending request for the user list")
			raise IOError("Error sending request for the user list: " + str(result))

	@_client_running
	def GetGM(self):
		result = self.__send__('GET', "/ws/v1/gms.json/" + self.session, {"guid": self.guid})
		if result["Code"] == 200:
			logging.info("Got GM: " + str(result["GM"]))
			return True
		else:
			logging.info("Error getting GM")
			raise IOError("Error sending request for the current GM: " + str(result))

	@_client_running
	def SetGM(self, guid):
		msg = {"session": self.session, "guid": self.guid, "new_gm": guid}
		result = self.__send__('PUT', "/ws/v1/gms.json/" + self.session, msg)
		if result["Code"] == 200:
			# TODO this should always be someone else, but we need be safe if we set ourself
			self.pyUser.is_gm = result["Is GM"]
			logging.info("Set the GM: " + str(guid))
			return True
		else:
			logging.info("Error setting new GM")
			raise IOError("Error setting new GM: " + str(result))

	@_client_running
	def MsgGM(self, msg_):
		result = self.__send__('GET', "/ws/v1/gms.json/" + self.session, {"guid": self.guid})
		if result["Code"] == 200:
			white = result["GM"] + ";" + self.pyUser.name
			return self.ClientSend(msg_, white=white)
		else:
			logging.info("Error messaging the GM")
			raise IOError("Error messaging the GM: " + str(result))

	@_client_running
	def RollGM(self, roll_name, equation, widgets):
		result = self.__send__('GET', "/ws/v1/gms.json/" + self.session, {"guid": self.guid})
		if result["Code"] == 200:
			white = result["GM"] + ";" + self.pyUser.name
			return self.Roll(roll_name, equation, widgets, white, hide=True)
		else:
			logging.info("Error private rolling with the GM")
			raise IOError("Error private rolling with the GM: " + str(result))


	@_client_running
	def Roll(self, roll_name, equation, widgets, white="*", black="", hide=False):
		msg = {
			"equation": equation,
			"widgets": widgets,
			"roll name": roll_name,
			"session": self.session,
			"name": self.pyUser.name,
			"white": white,
			"black": black,
			"hide": "true" if hide else "false"
			}
		result = self.__send__('POST', "/ws/v1/roll.json", msg)
		if result["Code"] == 200:
			logging.info("Got roll result: " + str(result["Result"]))
			return result["Result"]
		else:
			logging.info("Error sending the request for roll")
			raise IOError("Error sending request for roll")

	@_client_running
	def Kick(self, guid):
		msg = {"guid": self.guid, "session": self.session}
		result = self.__send__('DELETE', "/ws/v1/users.json/" + guid, msg=msg)
		if result["Code"] == 200:
			logging.info("Kicked user: " + str(guid))
			return True
		else:
			logging.info("Error kicking user")
			raise IOError("Error kicking user: " + str(result))
