import os

__QtLib__ = os.environ['QT_API']
if __QtLib__ == 'pyqt':
    from PyQt5.QtCore import pyqtSignal as Signal, Qt
    from PyQt5.QtGui import QTextCursor
    from PyQt5.QtWidgets import QPlainTextEdit
else:
    from PySide.QtCore import Signal, Qt
    from PySide.QtGui import QPlainTextEdit, QTextCursor

import net_dice_helpers as nd_helpers
from dice_roller import DiceRoller




# Adds tab complete and re-defines tab and delete
class rollTextEdit(QPlainTextEdit):
    accepted = Signal()

    def __init__(self, parent=None):
        #if we are passed a parent, this window can be a sheet. Neat!
        QPlainTextEdit.__init__(self, parent)

        self.valid = nd_helpers.color_text("Valid", "#00BB00")
        self.invalid = nd_helpers.color_text("Invalid", "#FF1111") + ": "

        self.roller = DiceRoller()
        self.last_valid = ''
        self.widgets = {}

        self.widget_names = []
        self.validLabel = []
        self.tab_complete = True

        self.textChanged.connect(self._validate_roll)


    def _setup(self, widget_name, widget_txt, widgets, validLabel):
        self.last_valid = widget_txt
        self.widgets = widgets
        self.validLabel = validLabel

        self.widget_names = [name for name in self.widgets]
        self.widget_names.remove(widget_name)

        self.setPlainText(widget_txt)
        self.validLabel.setText(self.valid)


    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Enter or event.key() == Qt.Key_Return:
            if str(self.textCursor().selectedText()):
                self.complete()
            else:
                self.accepted.emit()
        elif event.key() == Qt.Key_Tab:
            self.complete()
        elif (event.key() == Qt.Key_Delete or event.key() == Qt.Key_Backspace) and str(self.textCursor().selectedText()):
            self.tab_complete = False
            QPlainTextEdit.keyPressEvent(self, event)
        else:
            QPlainTextEdit.keyPressEvent(self, event)


    def complete(self):
        # sets the cursor to the end of the selected text
        cursor = self.textCursor()
        cursor.setPosition(self.textCursor().position())
        self.setTextCursor(cursor)


    def _validate_roll(self):
        roll_txt = str(self.toPlainText())

        if self.tab_complete:
            cursor_pos = self.textCursor().position()
            wid = self.get_curr_widget(roll_txt, cursor_pos)

            if wid:
                comp = self.get_completion(wid)
                if comp:
                    roll_txt = roll_txt[:cursor_pos] + comp + roll_txt[cursor_pos:]
                    old_state = self.blockSignals(True)
                    self.revert_roll(roll_txt, validate=False)

                    # select the tab-complete text
                    cursor = self.textCursor()
                    cursor.movePosition(QTextCursor.NextCharacter, QTextCursor.KeepAnchor, len(comp))
                    self.setTextCursor(cursor)
                    self.blockSignals(old_state)
        else:
            self.tab_complete = True

        try:
            self.roller.evalRoll(roll_txt, self.widgets)
        except Exception as e:
            err = e
            if ':' in err:
                err = err[err.find(':') + 2:]

            self.validLabel.setText(self.invalid + err)
            return False

        self.last_valid = roll_txt
        self.validLabel.setText(self.valid)
        return True


    def validate_roll(self):
        roll_txt = str(self.toPlainText())

        try:
            self.roller.evalRoll(roll_txt, self.widgets)
        except:
            return False
        return True


    def get_curr_widget(self, text, pos):
        # tries to detect the current widget the user is editing.
        # algo is a little shaky
        start_pos = text.rfind('[', 0, pos)
        if start_pos == -1:
            return ''

        pre_text = text[start_pos+1:pos]
        post_text = text[pos:text.find(']', pos)]
        if '[' in post_text or ']' in pre_text:
            return ''

        for char in ':=':
            if char in pre_text or char in post_text:
                    return ''

        return pre_text + post_text


    def revert_roll(self, txt='', validate=True):
        if not txt:
            txt = self.last_valid
        cursor_pos = self.textCursor().position()
        self.setPlainText(txt)
        cursor = self.textCursor()
        cursor.setPosition(cursor_pos)
        self.setTextCursor(cursor)
        if validate:
            self.validate_roll()


    def get_completion(self, text):
        poss = []

        for name in self.widget_names:
            if name.startswith(text):
                poss.append(name)

        if not poss:
            return ''
        elif len(poss) == 1:
            return poss[0][len(text):]

        common = poss[0][len(text):]

        # we want to get the prefix common to all the widget names we found
        for word in poss[1:]:
            word = word[len(text):]
            min_len = min(len(common), len(word))
            common = common[:min_len]
            for i in range(min_len):
                if word[i] != common[i]:
                    common = common[:i]
                    break

        return common
