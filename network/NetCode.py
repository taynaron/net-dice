import pickle
import logging
import queue

from twisted.internet import reactor, protocol
from twisted.protocols.basic import NetstringReceiver

from . import Logger

'''
Constants
'''
SLEEP_INCREMENT = .01		 # how often twisted will check for new outbound messages
_DEFAULT_HOST = "localhost"	 # default to local
_DEFAULT_PORT = 12345		 # default port to use

'''
Globals for the NetDice program
'''
# public
userName = ""
clientOutbox = queue.Queue()	# outgoing messages from the client
clientInbox = queue.Queue()		# received messages for the client
serverOutbox = queue.Queue()	# outgoing messages from the server

# "private"
_HARBOR = queue.Queue()			# flag for determining if the server is running
								# !empty means there's a connection
								# empty means that there's no connection
_HARBOR2 = queue.Queue()		# flag for determining if the client is running
								# !empty means there's a connection
								# empty means that there's no connection
_errorBox = queue.Queue()		# all the error messages thus far

'''
Provides a way of responding to different events that can occur

More specific types of functions for applications are intended for here
'''
clientConnectEvents = []		# callbacks for when a client connects
clientDisconnectEvents = []		# callbacks for when a client disconnects
clientNameEvents = []			# callbacks for when setting a client name

serverConnectEvents = []		# callbacks for when a server gets a connection
serverDisconnectEvents = []		# callbacks for when a server loses a connection
serverReceiveEvents = []		# callbacks for when a server receives a string

#--------------------------------------------------------------

'''
Note that Twisted runs its own event loop.  Given that this code is being used
with another program that has an event loop, and in another thread, the only
safe way to transmit information is with the queue structure.
'''

def _QueryKeepAlive(clientfactory):
	ClientSend({"keepalive":True})
	reactor.callLater(10, _QueryKeepAlive, clientfactory)

#--------------------------------------------------------------

def _QueryForTextToSendClient(clientfactory):
	if not clientOutbox.empty():
		results = clientOutbox.get()
		clientfactory.sendMessage(results)
	reactor.callLater(SLEEP_INCREMENT, _QueryForTextToSendClient, clientfactory)




class _ClientProtocol(NetstringReceiver):
	name = ""

	def getName(self):
		global userName
		if len(userName) == 0:
			return str(self.transport.getPeer().host) + ":" + str(self.transport.getPeer().port)
		return userName

	def connectionMade(self):
		global userName
		logging.info("Client: Connection Made")
		userName = self.getName()
		self.factory.clientProtocols.append(self)
		_HARBOR2.put(True)
		_runCallbacks(self, clientConnectEvents)

	# TODO consider passing the results to an event handler
	def stringReceived(self, line):
		global userName
		results = pickle.loads(line)
		if _isCmd(results, "name"):
			old_username = userName
			userName = results["name"]
			logging.info("Client: Set Name to: " + userName)
			_runCallbacks(self, clientNameEvents, old_username)
		elif _isCmd(results, "keepalive"):
			pass
		else:
			logging.info("Client: Received Message: " + str(results))
			clientInbox.put(results)

	def connectionLost(self, reason):
		logging.info("Client: Connection Lost: " + reason.getErrorMessage())
		_HARBOR2.get()
		_errorBox.put(reason)
		_runCallbacks(self, clientDisconnectEvents, reason)




class _ClientProtocolFactory(protocol.ClientFactory):
	protocol = _ClientProtocol

	def __init__(self):
		self.clientProtocols = []

	def sendMessage(self, msg):
		_foreach(self.clientProtocols, _transmit, msg)

	def disconnectClient(self):
		_foreach(self.clientProtocols, _loseConnection)


#--------------------------------------------------------------

'''
Messages that are originating from the Server.  The only place that can be
multiplexed out to all connected note all server logging will use the pickled
values
'''
def _QueryForTextToSendServer(serverfactory):
	if not serverOutbox.empty():
		action, who, results = serverOutbox.get()
		if action == "all":
			serverfactory.sendMessageToAllClients(results)
		elif action == "only":
			serverfactory.sendMessageToOnly(who, results)
		elif action == "not":
			serverfactory.sendMessageToAllBut(who, results)
	reactor.callLater(SLEEP_INCREMENT, _QueryForTextToSendServer, serverfactory)




class _ServerProtocol(NetstringReceiver):
	name = ""
	metadata = None

	def getName(self):
		return self.name or (str(self.transport.getPeer().host) + ":" + str(self.transport.getPeer().port))

	def getMetadata(self):
		return self.metadata

	def connectionMade(self):
		logging.info("Server: New Connection: " + self.getName())

		self.factory.clientProtocols.append(self)
		_runCallbacks(self, serverConnectEvents)
		# ServerSend("only", self.getName(), {"name": self.getName()})

	def connectionLost(self, reason):
		logging.info("Server: Lost Connection: " + self.getName() + " - " + reason.getErrorMessage())
		self.factory.clientProtocols.remove(self)
		_runCallbacks(self, serverDisconnectEvents, reason)

	def _checkProposedName(self, name):
		# python 3 doesn't have reduce natively, and the for loop reads better anyway
		# return reduce(lambda x, y: x or y.getName() == name, self.factory.clientProtocols, False)
		for user in self.factory.clientProtocols:
			if user.getName() == name:
				return name
		return False

	def _possibleNameExtensions(self):
		for a in range(99999):
			yield "" if a == 0 else " " + str(a)

	# TODO consider passing results to an event handler
	def stringReceived(self, line):
		results = pickle.loads(line)
		logging.info("Server: Received Message: " + str(results))

		if _isCmd(results, "name"):
			proposedNames = results["name"]
			for a in self._possibleNameExtensions():
				if not self._checkProposedName(proposedNames + a):
					logging.info("Server: " + self.getName() + " changed name to " + proposedNames)
					self.name = proposedNames + a
					break
			ServerSend("only", self.getName(), {"name":self.name})

		elif _isCmd(results, "keepalive"):
			ServerSend("only", self.getName(), {"keepalive":True})

		elif _isCmd(results, "host"):
			ServerSend("only", self.getName(), {"host":self.getName()})

		elif _isCmd(results, "getmetadata"):
			ServerSend("only", results["getmetadata"], {"metadata": self.metadata})

		elif _isCmd(results, "metadata"):
			self.metadata = results["metadata"]

		elif len(serverReceiveEvents) == 0:
			ServerSend("all", self.getName(), results)

		else:
			_runCallbacks(self, serverReceiveEvents, results)




class _ServerProtocolFactory(protocol.ServerFactory):
	protocol = _ServerProtocol

	def __init__(self):
		self.clientProtocols = []

	def emptyServer(self):
		_foreach(self.clientProtocols, _loseConnection)

	def kickClient(self, name):
		_foreach([a for a in self.clientProtocols if a.getName() == name], _loseConnection)

	def sendMessageToAllClients(self, msg):
		_foreach(self.clientProtocols, _transmit, msg)

	def sendMessageToAllBut(self, name, msg):
		_foreach([a for a in self.clientProtocols if a.getName() != name], _transmit, msg)

	def sendMessageToOnly(self, name, msg):
		_foreach([a for a in self.clientProtocols if a.getName() == name], _transmit, msg)

	def sendAllMetadata(self):
		metadata = {a.getName(): a.getMetadata() for a in self.clientProtocols}
		ServerSend("all", "", {"metadata": metadata})

#--------------------------------------------------------------

_serverfactory = _ServerProtocolFactory()
_clientfactory = _ClientProtocolFactory()

#--------------------------------------------------------------

def _foreach(iterable, func, value=None):
	for a in iterable:
		func(a, value)

def _loseConnection(a, value):
	a.transport.loseConnection()

def _transmit(a, value):
	a.sendString(value)

def _runCallbacks(protocol, array_to_call, other_messages = None):
	if other_messages == None:
		for func in array_to_call:
			func(protocol)
	else:
		for func in array_to_call:
			func(protocol, other_messages)

def _isCmd(results, key):
	return isinstance(results, dict) and key in results and len(results) == 1

#--------------------------------------------------------------

def isServerRunning():
	return not _HARBOR.empty()

def isClientRunning():
	return not _HARBOR2.empty()

def StartServer(server_port = _DEFAULT_PORT):
	if isServerRunning():
		logging.info("Server attempted to be started while still running")
		raise AssertionError("Server is already running")
	_HARBOR.put(reactor.listenTCP(server_port, _serverfactory))
	logging.info("Server: Starting Server on Port: " + str(server_port))
	return True

def StopServer():
	if not isServerRunning():
		logging.info("Server attempted to be stopped while not running")
		raise AssertionError("Server is not running")
	port = _HARBOR.get()
	port.factory.emptyServer()
	port.stopListening()
	logging.info("Server: Stopped server")
	return True

def StartClient(host = _DEFAULT_HOST, server_port = _DEFAULT_PORT):
	if isClientRunning():
		logging.info("Client attempted to be started while still running")
		raise AssertionError("The client is already running")
	reactor.connectTCP(host, server_port, _clientfactory)
	logging.info("Client: Starting Client on Host:Port : " + str(host) + ":" + str(server_port))
	return True

def StopClient():
	if not isClientRunning():
		logging.info("Client attempted to be stopped while not running")
		raise AssertionError("Client not running")
	_clientfactory.disconnectClient()
	logging.info("Client: Stopped client")
	return True

def StartReactor(inMain = True):
	Logger.SetupLog()
	# trying to connect too earlier can sometimes fail, so wait a second
	reactor.callLater(1, _QueryForTextToSendClient, _clientfactory)
	reactor.callLater(1, _QueryForTextToSendServer, _serverfactory)
	reactor.callLater(10, _QueryKeepAlive, _clientfactory)
	if inMain:
		reactor.run()
	else:
		reactor.run(installSignalHandlers = 0)
	logging.info("Netcode is running its reactor")

def ClientSend(line):
	clientOutbox.put(pickle.dumps(line, 2))
	logging.info("Client: Sending Message: " + str(line))
	return True

def ServerSend(action, who, line):
	serverOutbox.put((action, who, pickle.dumps(line, 2)))
	logging.info("Server: Sending Message: " + action + " " + who + " " + str(line))

def SetName(name):
	return ClientSend({"name": name})

def SetMetaData(data):
	return ClientSend({"metadata": data})

def RequestMetaData():
	global userName
	return ClientSend({"getmetadata": userName})

def SetSession(session):
	raise NotImplementedError("Only supported by AppEngine")

def GetUsers():
	raise NotImplementedError("Only supported by AppEngine")

def GetGM():
	raise NotImplementedError("Only supported by AppEngine")

def SetGM(guid):
	raise NotImplementedError("Only supported by AppEngine")

def MsgGM(msg):
	raise NotImplementedError("Only supported by AppEngine")

def Kick(guid):
	raise NotImplementedError("Only supported by AppEngine")
