import logging

def SetupLog(fileName = "./Logger.txt"):
	# Filename - time to the 10 millisecond - message
	FORMAT = '%(filename)s - %(levelname)s - %(asctime)-15s - %(message)s'
	logging.basicConfig(filename = fileName, level = logging.DEBUG, format = FORMAT)
	logging.info("-----")
