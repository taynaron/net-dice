# https://msdn.microsoft.com/en-us/library/aa375635(v=vs.85).aspx

manifest_base = """<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<assembly manifestVersion="1.0" xmlns="urn:schemas-microsoft-com:asm.v1">
  <assemblyIdentity name="{prog_name}" processorArchitecture="x86" type="win32" version="{prog_vers}"/>
  <dependency>
    <dependentAssembly>
      <assemblyIdentity language="*" name="Microsoft.Windows.Common-Controls"
	      processorArchitecture="x86" type="win32" version="6.0.0.0"
	      publicKeyToken="6595b64144ccf1df"/>
      <compatibility xmlns="urn:schemas-microsoft-com:compatibility.v1"/>
    </dependentAssembly>
  </dependency>
</assembly>"""



def make_vers_string(dot_vers):
	req_len = 4
	vers_list = dot_vers.split('.')
	if len(vers_list) > req_len:
		vers_list = vers_list[:req_len-1]
	elif len(vers_list) < req_len:
		vers_list.extend(['0' for i in range(req_len-len(vers_list))])

	return '.'.join(vers_list)


def make_manifest_file(args):
	file_name = args[0]
	file_base = file_name.split('.')[0]

	version_num = ''
	for line in open(file_name):
		if "VERSION_NUMBER" in line:
			print('NetDice version: ', line)
			version_num = line.split('=')[1].strip().strip("\'")
			break

	win_vers_string = make_vers_string(version_num)
	manifest_str = manifest_base.format(prog_name='NetDice', prog_vers=win_vers_string)

	f = open('netdice_manifest.xml', 'w')
	f.write(manifest_str)



if __name__ == "__main__":
	import sys
	make_manifest_file(sys.argv[1:])
