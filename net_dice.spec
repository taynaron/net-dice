# -*- mode: python -*-

import sys, shutil, os

# import net_dice_globals as NDG




root = '/Users/taynaron/Documents/DnD/New_Rolls/net_dice'
app_base = os.path.join(root, 'Net_Dice')
pyinstaller_dir = os.path.join(app_base, 'build')

app_name = 'NetDice'
app_console_name = 'net_dice'
icon_name = 'ui/icons/NetDice.icns'

# VERSION = NDG.VERSION_NUMBER  # pyinstaller's relative imports seem to be busted right now. Parse as a text file instead
VERSION = None
with open(os.path.join(app_base, 'net_dice_globals.py'), 'r') as ndg:
  for line in ndg:
    if line.startswith('VERSION_NUMBER'):
      # print(line, type(line))
      VERSION = line.split('=')[1].strip().strip("'")
      # print(VERSION)
      break



block_cipher = None

a = Analysis([app_console_name + '.py'],
             pathex = [pyinstaller_dir],
             binaries = None,
             datas = None,
             hiddenimports = [],
             hookspath = [],
             runtime_hooks = [],
             excludes = [],
             win_no_prefer_redirects = False,
             win_private_assemblies = False,
             cipher = block_cipher)

pyz = PYZ(a.pure, a.zipped_data,
          cipher = block_cipher)

a.datas += [('help_text.html', os.path.join(app_base, 'ui/help_text.html'), 'DATA'),
            ('change_log.txt', os.path.join(app_base, 'ui/change_log.txt'), 'DATA'),
            ('cacert.pem', os.path.join(app_base, 'network/cacert.pem'), 'DATA'),
           ]

widgets = 'widget_files'
a.datas.extend([(file, os.path.join(app_base, widgets, file), 'DATA') for file in os.listdir(widgets) if os.path.isfile(os.path.join(widgets, file)) and file.endswith('.json')])

exe = EXE(pyz,
          a.scripts,
          exclude_binaries = True,
          name = os.path.join('build/pyi.darwin/', app_console_name, app_console_name),
          debug = False,
          strip = False,
          # upx = True,  # macOS sierra breaks UPX. Re-enable once we move to UPX 3.92
          upx = False,
          append_pkg = True,
          console = False,
          icon = os.path.join(app_base, icon_name))

coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip = False,
               # upx = True,  # macOS sierra breaks UPX. Re-enable once we move to UPX 3.92
               upx = False,
               append_pkg = True,
               name = os.path.join('build', app_console_name))


app = BUNDLE(coll,
             name = os.path.join('dist', app_name+'.app'),
             icon = os.path.join(app_base, icon_name),
             bundle_identifier = None,
             version = VERSION,
             info_plist = {
                'NSHighResolutionCapable': 'True'
               }
             )
