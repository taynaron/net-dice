#!/usr/bin/env python
from google.appengine.ext import ndb

"""
DataModel for tracking a "session"
"""
class CounterModel(ndb.Model):
	"""
	A state variable to help different users track where they are in the
	stream of the log
	"""
	counter = ndb.IntegerProperty()

	"""
	A state variable to associate the counter with a session
	"""
	session = ndb.StringProperty()
