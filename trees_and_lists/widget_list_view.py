import logging, os

__QtLib__ = os.environ['QT_API']
if __QtLib__ == 'pyqt':
    from PyQt5.QtCore import pyqtSignal as Signal, Qt, QModelIndex, QPersistentModelIndex
    from PyQt5.QtWidgets import QMenu, QAction

else:
    from PySide.QtCore import Signal, Qt, QModelIndex, QPersistentModelIndex
    from PySide.QtGui import QMenu, QAction

from dice_roller import DiceRoller
from .base_tree_view import TreeView

from ui.ui_mods.group_dialog import groupDialog
from ui.ui_mods.widget_edit_dialog import widgetEditDialog
from ui import drag_drop as dd
from ui.ui_mods import ui_helpers

import net_dice_globals as NDG




# The UI portion of the Widget List
class WidgetView(TreeView):
    sendRolls = Signal(dict)

    def __init__(self, parent=None):
        TreeView.__init__(self, parent)

        self.roll_tester = DiceRoller()

        self.connection_mode = None

        self.setContextMenuPolicy(Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.treeContextMenu)

        self.ui = None
        self.group_win = None


    def giveUI(self, UI):
        self.ui = UI
        self._setup_connections()


    def _setup_connections(self):
        self.ui.addToRollButton.clicked.connect(self.add_widgets_to_roll)
        # self.ui.removeFromListButton.clicked.connect(self.remove_widgets)
        self.ui.rollSelectedButton.clicked.connect(self.roll_widgets)
        self.ui.groupButton.clicked.connect(self.group_widgets)


    def set_conn_mode(self, conn_mode):
        self.connection_mode = conn_mode


    def keyPressEvent(self, event):
        key = event.key()
        if key == Qt.Key_Delete:
            self.remove_widgets()
        elif key == Qt.Key_Backspace:
            self.remove_widgets()
        elif key == Qt.Key_E:
            self.edit_widgets()
        elif key == Qt.Key_R:
            self.roll_widgets()
        elif key == Qt.Key_G:
            self.group_widgets()
        elif key == Qt.Key_A:
            self.add_widgets_to_roll()
        elif key == Qt.Key_Insert:
            self.add_widgets_to_roll()
        else:
            TreeView.keyPressEvent(self, event)


    # Figures out what's selected, or if nothing's selected what's under the mouse.
    # Puts all selected in a list
    def makeSelectedList(self, selected=None):
        if not selected:
            selected = self.selectedIndexes()
        sel_list = []
        for index in selected:
            if index.column() == 0:
                sel_list.append(self.model().getItem(index).data(0))
        if selected and not sel_list:
            # if there's an item but nothing's been selected
            # only comes up when nothing's selected and the user clicked on an object
            sel_list.append(self.model().getItem(selected[0]).data(0))
        logging.info("selected" + str(sel_list))
        # raise NameError
        return sel_list


    def get_item_index(self, item, parent=QModelIndex()):
        return self.model().sourceModel().get_item_index([item, '*'], parent=parent)


    def add_widgets_to_roll(self, selected=None):
        if not selected:
            selected = self.makeSelectedList()
        self.ui.rollEdit.add_widget_names(selected)

        status_txt = "Widgets Added to Roll: " + ', '.join(selected)
        self.ui.statusBar.showMessage(status_txt)


    def roll_widgets(self, selected=None):
        tree_model = self.ui.chatTreeView.model()
        err_msg = ''
        if not selected:
            selected = self.makeSelectedList()
        widgets = self.model().sourceModel().get_widget_dict(flatten=True)
        base_txt = "Rolls Added: "
        roll_list = []
        for item in selected:
            try:
                roll_result = self.roll_tester.evalRoll(str(widgets[item]), widgets)
                logging.info(str(roll_result))
                if not roll_result:
                    raise Exception("roll_widgets failed; roller returned no reason")
            except Exception as e:
                err_msg += str(e) + " "
                continue
            if self.connection_mode not in NDG.ONLINE_STATES:
                roll_list.append(tree_model.add_rolls(roll_result, item))
            else:
                try:
                    logging.info(str(item))
                    needed_wid = self.roll_tester.getNeededWidgets(widgets[item], widgets)
                except:
                    raise
                roll_list.append({'Roll': str(widgets[item]), 'Roll Name': str(item), 'Roll Widgets': needed_wid})

        if roll_list:
            if self.connection_mode not in NDG.ONLINE_STATES:
                base_txt += ' / '.join(roll_list) or "None"
                self.ui.statusBar.showMessage(base_txt)
            else:
                send_list = {'Rolls': roll_list}
                self.sendRolls[dict].emit(send_list)
        else:
            self.ui.statusBar.showMessage("No Widgets could be rolled: " + err_msg)


    def edit_widgets(self, selected_indexes=None):
        if not selected_indexes:
            selected_indexes = self.selectedRows()

        model = self.model().sourceModel()

        for index in selected_indexes:
            item = model.getItem(QModelIndex(index))
            name = item.data(0)
            roll = item.data(1)

            edit_win = widgetEditDialog(name, roll, model.get_widget_dict(flatten=True), self)
            result = edit_win.exec_()

            if result:
                roll = edit_win.last_valid().strip()
                new_index = index.sibling(index.row(), 1)
                model.setData(new_index, roll)


    def remove_widgets(self, selected_indexes=None):
        if not selected_indexes:
            selected_indexes = self.selectedRows()

        model = self.model().sourceModel()
        name_list = []

        for index in selected_indexes:
            item = model.getItem(QModelIndex(index))
            name = item.data(0)
            if model.item_exists(['*', '[{name}]'.format(name=name)], substring_ok=True):
                title = "Really Delete?"
                message = "One or more widgets depends on this widget name. Do you really want to delete this widget?"
                really_del = ui_helpers.generic_ok_dialog(title, message)
                if not really_del:
                    continue

            name_list.append(item.data(0))
            row = item.child_number()
            model.remove_row(row, parent=index.parent())

        if name_list:
            status_txt = "Widgets Removed From List: " + ', '.join(name_list)
        else:
            status_txt = "No Widgets Removed From List."

        self.ui.statusBar.showMessage(status_txt)


    def selectedRows(self):
        selected = self.selectedIndexes()
        selected_rows = []
        for index in selected:
            if index.column() == 0:
                selected_rows.append(QPersistentModelIndex(self.model().mapToSource(index)))
        return selected_rows


    def group_widgets(self, selected=None):
        if not selected:
            selected = self.selectedRows()

        # Determine where to group them
        if selected:
            self.group_win = groupDialog(selected, self)
            self.group_win.addGroup[list, str].connect(self.group_dialog_accept)


    def group_dialog_accept(self, selected, group_name):
        if selected and group_name:
            model = self.model().sourceModel()

            folder_index = self.get_item_index(group_name)
            if not folder_index.isValid():
                if not model.add_item([group_name, '']):
                    return
                folder_index = self.get_item_index(group_name)
                if not folder_index.isValid():
                    logging.info("something very screwy")
                    return
            folder_index = QPersistentModelIndex(folder_index)

            self._group_indexes(selected, folder_index)
            self.group_win = None
            # self.selectionChanged(QItemSelection(), QItemSelection())
            self.clearSelection()


    def _group_indexes(self, indexes, folder_index):
        model = self.model().sourceModel()
        # for index in indexes:
        #    model.moveItem(index, folder_index)
        model.move_indexes(indexes, QModelIndex(folder_index))


    def mousePressEvent(self, event):
        if event.button() == Qt.MidButton:
            # handle left mouse button here
            selected = selected = self._get_selected_list(event.pos())

            self.roll_widgets(self.makeSelectedList(selected))
        else:
            # pass on other buttons to base class
            TreeView.mousePressEvent(self, event)


    # --- Drag and Drop ---
    def dragEnterEvent(self, event):
        if event.mimeData().hasFormat(dd.DD_MIME_ROLLS):
            event.acceptProposedAction()


    def dragMoveEvent(self, event):
        self.dragEnterEvent(event)


    def dropEvent(self, event):
        drop = event.mimeData()

        if not drop.hasRolls():
            return False

        event.acceptProposedAction()

        model = self.model().sourceModel()
        rolls = drop.rolls()
        subrolls = drop.subRolls()

        if event.source() != self:  # add widgets to the list
            added = []
            failed = []

            for roll in rolls:
                roll['Roll Widgets'] = subrolls
                status = model.add_replace_item([roll['Roll Name'], roll['Roll']])

                if status:
                    added.append(roll['Roll Name'])
                else:
                    failed.append(roll['Roll Name'])

            response_string = ''
            if len(added):
                response_string = "Widgets Created: " + ', '.join(added)
            if len(failed):
                response_string += "Widgets Failed: " + ', '.join(failed)
            self.ui.statusBar.showMessage(response_string)

        else:  # we're moving widgets around
            indexes = []

            for roll in rolls:
                key = roll['Row']
                if roll['Parent'] == 'None':
                    parent = QModelIndex()
                else:
                    parent = self.get_item_index(roll['Parent'])
                    logging.info("parent:" + str(parent))
                    if not parent.isValid():
                        logging.info("parent fail")
                        continue

                indexes.append(model.index(key, 0, parent))

            # For some reason, just using the QModelIndex provided by indexAt was crashing python.
            # We need to map from the sorted index to the base model index.
            mouse_index = self.model().mapToSource(self.indexAt(event.pos()))
            temp_index = None
            if mouse_index.column() != 0:  # column shouldn't matter, set it to 0
                temp_index = mouse_index  # if col isn't 0, it's 1, which we want in temp
                mouse_index = model.index(mouse_index.row(), 0, mouse_index.parent())

            if temp_index is None:
                temp_index = model.index(mouse_index.row(), 1, mouse_index.parent())

            temp_index_data = temp_index.data()
            if temp_index_data != '' and temp_index_data is not None:
                logging.info("up the stack" + str(temp_index_data))
                mouse_index = mouse_index.parent()

            # mouse_item_data = mouse_index.data()
            # folder = self.get_item_index(str(mouse_item_data), parent=indexes[0].parent())
            # if folder == False:
            #    folder = QModelIndex()
            if not mouse_index:
                mouse_index = QModelIndex()

            mouse_index = QPersistentModelIndex(mouse_index)

            logging.info(str(mouse_index) + ", " + str(QPersistentModelIndex(QModelIndex())))

            self._group_indexes(indexes, mouse_index)
            # self._group_indexes(indexes, folder)


    # Right click popup menu
    def treeContextMenu(self, pos):
        globalPos = self.mapToGlobal(pos)  # QPoint

        menu = QMenu()
        roll_txt = "&Roll Selected"
        roll_action = QAction(roll_txt, self)
        roll_action.setShortcut(Qt.Key_R)
        # roll_action.setData(selected)
        menu.addAction(roll_action)

        edit_txt = "&Edit Selected"
        edit_action = QAction(edit_txt, self)
        edit_action.setShortcut(Qt.Key_E)
        # edit_action.setData(selected)
        menu.addAction(edit_action)

        add_txt = "&Add to Roll"
        add_action = QAction(add_txt, self)
        add_action.setShortcut(Qt.Key_A)
        # add_action.setData(selected)
        menu.addAction(add_action)

        group_txt = "&Group Selected"
        group_action = QAction(group_txt, self)
        group_action.setShortcut(Qt.Key_G)
        # add_action.setData(selected)
        menu.addAction(group_action)

        rem_txt = "Remove From List"
        rem_action = QAction(rem_txt, self)
        rem_action.setShortcut(Qt.Key_Delete)
        # rem_action.setData(selected)
        menu.addAction(rem_action)

        selectedItem = menu.exec_(globalPos)  # QAction

        if selectedItem:
            if selectedItem.text() == roll_txt:
                self.roll_widgets()
            elif selectedItem.text() == edit_txt:
                self.edit_widgets()
            elif selectedItem.text() == add_txt:
                self.add_widgets_to_roll()
            elif selectedItem.text() == group_txt:
                self.group_widgets()
            elif selectedItem.text() == rem_txt:
                self.remove_widgets()
