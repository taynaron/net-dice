# NetDice #

*netdiceproject@gmail.com*

NetDice is a networked enabled D20 style dice rolling application. NetDice enables basic chat functionality along with D20 style rolls, which are standard math equations augmented with D20 specific operators. D% support has also been added.

NetDice is a cross-platform tool by virtue of being written in PyQt. Executable packages are built for OS X and Windows using py2app and PyInstaller respectively.
NetDice uses Twisted for its networking backend, with a client/server model. A server-less mode using AppEngine is currently in beta.

Please email netdiceproject@gmail.com with any inquiries. Any help is appreciated.