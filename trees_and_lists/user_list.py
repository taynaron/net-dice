import logging, os

__QtLib__ = os.environ['QT_API']
if __QtLib__ == 'pyqt':
    from PyQt5.QtCore import QObject
    from PyQt5.QtGui import QColor
else:
    from PySide.QtCore import QObject
    from PySide.QtGui import QColor

from .user_list_model import UserSort
from .user_list_model import UserModel




class UserList(QObject):
    def __init__(self, ui, local_user, use_server=True):
        QObject.__init__(self)
        self.ui = ui
        self.user_list = None
        self.user_sort = None
        self.ui.usersView.getUI(self.ui)

        self.reset_user_model(local_user, use_server=use_server)


    def reset_user_model(self, local_user, use_server=True):
        self.user_list = UserModel(local_user, use_server=use_server)
        self.user_sort = UserSort(source_model=self.user_list)

        self.ui.usersView.setModel(self.user_sort)


    def set_user_list(self, user_dict):
        self.user_list.clear_rows()
        self.add_users(user_dict)


    def set_gm(self, new_gm_name):
        return self.user_list.set_gm(new_gm_name)


    def add_users(self, user_dict):
        if isinstance(user_dict, dict):
            for name in user_dict:
                name_dict = {'Name': name}
                name_dict.update(user_dict[name])
                self.add_user(name_dict)
        elif isinstance(user_dict, list):
            for name in user_dict:
                name["GM"] = name.get("Is GM")
                self.add_user(name)


    def add_user(self, user_dict):
        logging.info("UserList: add to user list: " + str(user_dict))

        new_name = user_dict['Name'] if 'Name' in user_dict else user_dict['New Name']

        self._prep_user_dict(user_dict)
        new_user = [new_name, user_dict['Muted'], user_dict['GM']]
        if self.user_list.num_cols > 3:
            new_user.append(user_dict['Server'])
        status = self.user_list.add_replace_item(new_user, bg_color=QColor(user_dict['Color']))

        if status:
            self.ui.statusBar.showMessage('User "{name}" Added'.format(name=new_name))
            self.ui.usersView._set_num_users()
            self.ui.chatTreeView.model()._user_added(new_name)
            return True
        self.ui.statusBar.showMessage("User Add Failed")
        return False


    def remove_user(self, old_user):
        logging.info("UserList: removing user from list: " + str(old_user))
        name = ''
        if isinstance(old_user, dict):
            name = old_user['Name']
        elif isinstance(old_user, str):
            name = old_user
        logging.info("UserList: name: " + str(name))
        status = self.user_list.remove_users([name])

        if status:
            self.ui.statusBar.showMessage('User "{name}" Removed'.format(name=name))
            self.ui.usersView._set_num_users()
            self.ui.chatTreeView.model()._user_removed(name)
            return True
        self.ui.statusBar.showMessage("User Remove Failed")
        return False


    def clear(self):
        self.user_list.clear_rows()


    def _prep_user_dict(self, user_dict):
        if 'Muted' not in user_dict:
            user_dict['Muted'] = False
        if 'GM' not in user_dict:
            user_dict['GM'] = False
        if 'Server' not in user_dict:
            user_dict['Server'] = False


    def change_user_name(self, old_user, new_user):
        return self.user_list.change_user_name(old_user, new_user)


    def change_user_color(self, name, color):
        return self.user_list.change_user_color(name, color)


    def get_user_color(self, user_name):
        return self.user_list.get_user_color(user_name)
