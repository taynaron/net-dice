import os

__QtLib__ = os.environ['QT_API']
if __QtLib__ == 'pyqt':
    from PyQt5.QtCore import QMimeData
else:
    from PySide.QtCore import QMimeData




DD_MIME_ROLLS = "appication/rolls"
DD_MIME_SUBROLLS = "appication/subrolls"


# The handler for the chat window
# Stores all info in a tree structure
class DiceMimeData(QMimeData):
    def __init__(self):
        QMimeData.__init__(self)
        self._rolls = []
        # rolls format: [ {"User Name":"bob", Roll Name: Roll1, Roll:1d4, Result:3}, {"User Name":"bob", Roll Name: Roll2, Roll:2d4, Result:5} ]
        self._subrolls = {}
        # subrolls format: {roll1:d4, roll2:3d6-[roll1]}


    def formats(self):
        formats = QMimeData.formats(self)
        formats.insert(0, DD_MIME_ROLLS)
        formats.insert(0, DD_MIME_SUBROLLS)
        return formats


    def hasFormat(self, mimeType):
        if mimeType == DD_MIME_ROLLS:
            return self._rolls != []
        elif mimeType == DD_MIME_SUBROLLS:
            return self._subrolls != {}
        else:
            return QMimeData.hasFormat(self, mimeType)


    def hasRolls(self):
        return self.hasFormat(DD_MIME_ROLLS)


    def hasSubRolls(self):
        return self.hasFormat(DD_MIME_SUBROLLS)


    def data(self, mimeType):
        if mimeType == DD_MIME_ROLLS:
            return self._rolls
        elif mimeType == DD_MIME_SUBROLLS:
            return self._subrolls
        else:
            return QMimeData.data(self, mimeType)


    def rolls(self):
        return self.data(DD_MIME_ROLLS)


    def subRolls(self):
        return self.data(DD_MIME_SUBROLLS)


    def setData(self, mimeType, data):
        # const QString & mimeType, const QByteArray & data
        if mimeType == DD_MIME_ROLLS and isinstance(data, list):
            self._rolls = data
        elif mimeType == DD_MIME_SUBROLLS and isinstance(data, dict):
            self._subrolls = data
        else:
            QMimeData.setData(self, mimeType, data)


    def setRolls(self, rolls):
        self.setData(DD_MIME_ROLLS, rolls)


    def setSubRolls(self, sub_rolls):
        self.setData(DD_MIME_SUBROLLS, sub_rolls)


    def setTextFromRolls(self):
        text = []

        for roll in self._rolls:
            text.append("{name} :: {roll}".format(name=roll['Roll Name'], roll=roll['Roll']))

        self.setText(", ".join(text))
        # self.setText(", ".join(map(lambda x: x['Roll Name'] + " :: " + x['Roll'], self._rolls)))
