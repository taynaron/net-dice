import logging, sys, os

__QtLib__ = os.environ['QT_API']
if __QtLib__ == 'pyqt':
    from PyQt5.QtCore import Qt
    from PyQt5.QtWidgets import QMainWindow, QMessageBox, QTextEdit
    from PyQt5.QtGui import QIcon

    from ui.roller_pyqt import Ui_MainWindow
else:
    from PySide.QtCore import Qt
    from PySide.QtGui import QMainWindow, QMessageBox, QTextEdit, QIcon

    from ui.roller_pyside import Ui_MainWindow


import file_IO

from ui.ui_mods import ui_helpers

from roller_UI import RollerUI
from users.client_server_UI import UserView
from trees_and_lists.user_list import UserList
from network.net_layer import NetLayer
# from network.net_layer_ae import AELayer
from threads.update_thread import UpdateThread

import net_dice_helpers as nd_helpers
import net_dice_globals as NDG




class DiceWin(QMainWindow):
    def __init__(self, parent=None):
        logging.info("Starting up NetDice main")

        QMainWindow.__init__(self)
        self.app = parent

        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.setWindowTitle("{title} - {version}".format(title=self.windowTitle(), version=NDG.VERSION_NUMBER))

        if sys.platform != 'darwin':  # window icons aren't appropriate in macOS
            self.setWindowIcon(QIcon(nd_helpers.support_file_path('ui/icons/NetDice.ico')))

        self.init_geo = {}  # to set up window geo after window instantiation

        # hackey, lets us use option-modifiers on startup
        self.ui.userNameEdit.clearFocus()
        self.ui.serverPortEdit.clearFocus()
        self.ui.clientConnectToEdit.clearFocus()
        self.ui.rollNameEdit.clearFocus()
        self.ui.rollEdit.clearFocus()
        self.ui.broadcastEdit.clearFocus()
        self.ui.emailSenderEdit.clearFocus()

        self.version_thread = UpdateThread()

        init_help = self._init_prefs()
        self.user = self.user_view.user

        # Finally, start the net code up, then make all the app-wide connections.
        logging.info("Turning on the network thread")
        self.user_list = UserList(self.ui, self.user)
        self.net_layer = NetLayer(self.ui, self.user, self.user_list)
        # self.ae_layer = AELayer(self.ui, self.user, self.user_list)
        self._setup_connections()

        self.show()
        self.raise_()  # needed for pyinstaller, usually not necessary

        # need to set window geo after showing window or the frame (os stuff) won't be accounted for
        self.init_window_geo()

        self.version_thread.check_new_version()  # spawns a new thread to version check

        if init_help:
            self._beginner_help()


    def _init_prefs(self):
        prefs_dict = self.get_prefs()
        help_result = False

        if not (prefs_dict and isinstance(prefs_dict, dict)):
            prefs_dict = {}

            message = "You may not have used NetDice before.\nWould you like some help to get started?"
            buttons = ["Help me", "I'll be fine"]
            help_result = (ui_helpers.generic_multibutton_dialog(message, buttons) == 0)

        # Set up user info and UI stuff
        if 'User Prefs' in prefs_dict:
            logging.info("Loaded user preferences")
            self.user_view = UserView(self, prefs_dict['User Prefs'])
        else:
            logging.info("Loaded default preferences")
            self.user_view = UserView(self)

        # Set up the roller UI stuff
        if 'Roller Prefs' in prefs_dict:
            # Do stuff based on how long it's been since the user updated:
            try:
                if 'Widget Prefs' in prefs_dict['Roller Prefs']:
                    widgets = prefs_dict['Roller Prefs']['Widget Prefs']['Widget File']
                    # if 'Last Version' in prefs_dict:
                    #     self._version_changes(widgets, prefs_dict['Last Version'])
                    # else:
                    #     self._version_changes(widgets)
            except:
                raise

            self.roller = RollerUI(self, widgets)
        else:
            self.roller = RollerUI(self)

        if 'Last Version' in prefs_dict and nd_helpers.check_older_version(prefs_dict['Last Version']):
            self._show_change_log(prefs_dict['Last Version'])

        self.init_geo = prefs_dict['Window'] if 'Window' in prefs_dict else None

        return help_result


    def _setup_connections(self):
        self.ui.actionLocal_Mode.triggered.connect(self.save_prefs)
        self.ui.actionClient_Mode.triggered.connect(self.save_prefs)
        self.ui.actionServer_Mode.triggered.connect(self.save_prefs)
        self.ui.actionE_Mail_Mode.triggered.connect(self.save_prefs)
        # self.ui.actionInternet_Mode.triggered.connect(self.save_prefs)
        self.ui.actionRoll_Typing.triggered.connect(self.save_prefs)
        self.ui.actionRoll_Building.triggered.connect(self.save_prefs)

        self.version_thread.NewVersion[str, str].connect(self._update_window)
        self.version_thread.threadError[str, Exception].connect(self._update_error)

        self.user_view.ConnTypeChanged[str].connect(self.roller.set_conn_mode)
        self.user_view.ConnTypeChanged[str].connect(self.ui.chatTreeView.set_conn_mode)
        self.user_view.sync_UI()

        self.ui.actionAbout_Net_Dice.triggered.connect(self.about_net_dice)
        self.ui.actionAbout_Qt.triggered.connect(self.about_qt)

        self._setup_net_connections()
        # self._setup_ae_connections()

        self.app.aboutToQuit.connect(self._shutdown)


    def _setup_net_connections(self):
        logging.info("Setting up Qt signals for starting/stopping the client, starting the server")
        self.user_view.startClient.connect(self.net_layer.start_client)
        self.user_view.startServer.connect(self.net_layer.start_server)
        self.net_layer.reactor_thread.clientStarted[dict].connect(self.user_view.client_started)
        self.net_layer.reactor_thread.clientStopped[dict].connect(self.user_view.client_stopped)
        self.net_layer.reactor_thread.clientStopped[dict].connect(self.net_layer.disconnect_from_server)
        self.net_layer.serverStarted[dict].connect(self.user_view.server_started)

        logging.info("Setting up PyQT for connection errors")
        self.net_layer.clientStartFailed[dict].connect(self.user_view.start_failed)
        self.net_layer.serverStartFailed[dict].connect(self.user_view.start_failed)

        logging.info("Setting PyQT for disconnection events")
        self.user_view.clientDisconnect.connect(self.net_layer.disconnect_from_server)
        self.user_view.serverDisconnect.connect(self.net_layer.stop_server)
        self.net_layer.clientDisconnected.connect(self.user_view.client_stopped)
        self.net_layer.serverDisconnected.connect(self.user_view.server_stopped)

        self.ui.userBox.boxCollapseChanged[bool].connect(self.user_view.refresh_user_ui)
        self.ui.connectedUsersBox.boxCollapseChanged[bool].connect(self.user_view.refresh_user_ui)

        logging.info("Setting up PyQT for handling the rolls, and chats")
        self.roller.sendRoll[dict].connect(self.net_layer.send_roll_to_server)
        self.ui.widgetView.sendRolls[dict].connect(self.net_layer.send_rolls_to_server)
        self.ui.chatTreeView.sendRoll[dict].connect(self.net_layer.send_roll_to_server)
        self.ui.chatTreeView.sendRolls[dict].connect(self.net_layer.send_rolls_to_server)
        self.net_layer.rollResultReceived[dict].connect(self.roller.roll_received)
        self.net_layer.chatReceived[dict].connect(self.roller.chat_received)


    # def _setup_ae_connections(self):
    #     logging.info("Setting up Qt signals for starting/stopping the client, starting the server")
    #     self.user_view.startAE.connect(self.ae_layer.start_client)
    #     self.ae_layer.aeStarted[dict].connect(self.user_view.client_started)
    #     self.ae_layer.aeStopped[dict].connect(self.user_view.client_stopped)
    #     self.ae_layer.aeStopped[dict].connect(self.ae_layer.disconnect_from_server)

    #     logging.info("Setting up PyQT for connection errors")
    #     self.ae_layer.aeStartFailed[dict].connect(self.user_view.start_failed)

    #     logging.info("Setting PyQT for disconnection events")
    #     self.user_view.aeDisconnect.connect(self.ae_layer.disconnect_from_server)
    #     self.ae_layer.aeDisconnected.connect(self.user_view.client_stopped)

    #     logging.info("Setting up PyQT for handling the rolls, and chats")
    #     self.roller.sendRoll[dict].connect(self.ae_layer.send_roll_to_server)
    #     self.ui.widgetView.sendRolls[dict].connect(self.ae_layer.send_rolls_to_server)
    #     self.ui.chatTreeView.sendRoll[dict].connect(self.ae_layer.send_roll_to_server)
    #     self.ui.chatTreeView.sendRolls[dict].connect(self.ae_layer.send_rolls_to_server)
    #     self.ae_layer.rollResultReceived[dict].connect(self.roller.roll_received)
    #     self.ae_layer.chatReceived[dict].connect(self.roller.chat_received)


    def _beginner_help(self):
        # open help text
        self.about_net_dice()

        message = """I've opened up the Help file for you. It has all the information you should need.
You can access it at any time through the Menu Bar, in "About NetDice".

Some basics:
You can create rolls (like 3d20+6) in the upper-left area, under 'rolls'. You can give them a name for easier identification.
If you roll a roll the result will show up in the right panel. If you save a roll as a widget it will go to the list in the lower left.
Some d20-style roller widgets are in the widgets pane by default.

You are in "Client" mode. This allows you to connect to another version of NetDice running in Server mode. When in client mode, you must connect to a server before rolling.
If you would like to roll dice locally, be a server, or send a roll-based email, change modes via the "Modes" Menu Bar item.

Please direct any questions to netdiceproject@gmail.com. I'm happy to help!"""
        buttons = ["Ok"]
        ui_helpers.generic_multibutton_dialog(message, buttons, self.ui.centralwidget)


    # Gets called when program is quitting
    def _shutdown(self):
        logging.info("Shutting down")
        # self.ae_layer.disconnect_from_server()
        self.net_layer.stop_all()
        self.save_prefs()
        self.roller.save_widgets()
        self.deleteLater()


    def _show_change_log(self, vers):
        logging.info("Getting change log information for netdice")

        log = nd_helpers.open_support_file("ui/change_log.txt")

        find_base = '-- v'
        find_vers = find_base + str(vers)
        log = log[:log.find(find_vers)]
        log_list = log.split("\n\n")
        log = "\n\n".join(log_list[:min(len(log_list) - 1, 5)])
        log = "Changes since you last used NetDice:\n\n" + log

        QMessageBox.information(self, "Change Log", log, buttons=QMessageBox.StandardButtons(QMessageBox.Ok))


    def _update_window(self, vers, url):
        title = "New Version Available"
        vers_msg = "New Version Available!\n\nYou have: {old}\nNewest version: {new}\nGet Update?".format(NDG.VERSION_NUMBER, vers)

        update = QMessageBox.warning(self, title, vers_msg, buttons=QMessageBox.StandardButtons(QMessageBox.Ok | QMessageBox.Cancel))
        if update == QMessageBox.Ok:
            logging.info("Needed to download a new version: " + NDG.VERSION_NUMBER)
            import webbrowser
            webbrowser.open_new_tab(url)


    def _update_error(self, where, err):
        msg = where + ": " + str(err)
        self.ui.statusBar.showMessage(msg)
        text = "Update attempt failed. You may wish to check the <a href='" + NDG.website + "'>NetDice website</a> for an update."
        self.ui.chatTreeView.model().add_broadcast(text, htmlInput=True)
        logging.error(msg)


    # def _version_changes(self, widgets_file, last_vers=None):
    #     widgets_dict = file_IO.read_file(widgets_file)


    def init_window_geo(self):
        if not self.init_geo:
            logging.info("Didn't receive any geometry info. Skipping window geometry intitialization.")
            return

        if 'geometry' in self.init_geo:
            self.restoreGeometry(self.init_geo['geometry'])
        if 'state' in self.init_geo:
            # print("state", self.init_geo['state'])
            self.restoreState(self.init_geo['state'])
        if 'splitter positions' in self.init_geo:
            if 'main' in self.init_geo['splitter positions']:
                self.ui.mainSplitter.restoreState(self.init_geo['splitter positions']['main'])
            if 'chat' in self.init_geo['splitter positions']:
                self.ui.chatSplitter.restoreState(self.init_geo['splitter positions']['chat'])


    def get_prefs(self):
        prefs_dict = None
        prefs_text = False

        if os.path.isfile("prefs.pkl"):
            prefs_dict = file_IO.read_file("prefs.pkl")
        elif os.path.isfile("prefs.txt"):
            prefs_dict = file_IO.read_file("prefs.txt")
            prefs_text = True
        else:
            logging.info("No preference file found")
            return {}

        assert prefs_dict

        if not prefs_dict or __QtLib__ != 'pyqt' and ('Preferences Version' not in prefs_dict or prefs_dict['Preferences Version'] != 2):
            self.ui.statusBar.showMessage('Preference file is an older format. Resetting.')
            return {}

        # file_IO.attrs_to_bytes(prefs_dict, ['Window Geometry', 'Window State', 'Main Splitter Position', 'Chat Splitter Position'])

        self.ui.statusBar.showMessage('Preference File Loaded')

        if prefs_text:
            os.remove("prefs.txt")
            # self.save_prefs(prefs_dict)

        return prefs_dict


    def save_prefs(self, prefs_dict=None):
        curr_prefs_version = 3
        if prefs_dict and isinstance(prefs_dict, dict):
            for attr in ['Preferences Version', 'User Prefs', 'Roller Prefs', 'Window', 'Last Version']:
                assert attr in prefs_dict, "{attr} is missing from prefs_dict".format(attr=attr)
                for attr in ['geometry', 'state', 'splitter positions']:
                    assert attr in prefs_dict['Window'], "{attr} is missing from prefs_dict['Window']".format(attr=attr)
            assert prefs_dict['Preferences Version'] == curr_prefs_version, \
                "incorrect prefs version, expecting {curr} got {other}".format(curr=curr_prefs_version, other=prefs_dict['Preferences Version'])

            prefs_out_dict = prefs_dict
        else:
            prefs_out_dict = {
                'Preferences Version': curr_prefs_version,
                'User Prefs': self.user_view.get_user_info(),
                'Roller Prefs': self.roller.get_roller_info(),
                'Window': {
                    'geometry': self.saveGeometry(),
                    'state': self.saveState(),
                    'splitter positions': {
                        'main': self.ui.mainSplitter.saveState(),
                        'chat': self.ui.chatSplitter.saveState(),
                    },
                },
                'Last Version': NDG.VERSION_NUMBER
            }

        file_IO.save_file("prefs.pkl", prefs_out_dict, serialize=True)
        self.ui.statusBar.showMessage("Preference File Saved")


    def about_net_dice(self):
        logging.info("Getting about information for netdice")

        help = QTextEdit(self)
        help.setWindowFlags(Qt.Window)  # or Qt::Tool, Qt::Dialog if you like
        help.setReadOnly(True)
        new_geom = help.geometry()
        new_geom.setHeight(500)
        new_geom.setWidth(500)
        help.setGeometry(new_geom)

        about = nd_helpers.open_support_file("ui/help_text.html")
        help.setHtml(about)

        help.show()


    def about_qt(self):
        QMessageBox.aboutQt(self)
