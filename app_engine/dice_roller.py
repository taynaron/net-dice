import random
import inspect
import re
import copy


# Token Generator, the default type
def token(value, type, depth):
	return {"value": value, "type": type, "stack": [], "depth": depth}

def step(step, trigger):
	return {"step": step, "trigger": trigger}

class Tokenizer():
	__machine = {}
	"""
	c -> h -> end
	  -> l -> end
	  -> r -> i -> t -> end
	d -> end
	  -> % -> end
	f -> a -> i -> l -> end
	  -> l -> o -> o -> r -> end
	r -> e -> g -> e -> end
	          l -> e -> end
	m -> a -> x -> end
	     i -> n -> end
	"""

	# The different states and what's an acceptable trigger to that state
	__unknown = step("", "")
	__digit = step("digit", "0123456789")
	__c = step("c", "cC")
	__ch = step("ch", "hH")
	__cl = step("cl", "lL")
	__cr = step("cr", "rR")
	__cri = step("cri", "iI")
	__crit = step("crit", "tT")
	__d = step("d", "dD")
	__d_mod = step("d_mod", "%")
	__f = step("f", "fF")
	__fa = step("fa", "aA")
	__fai = step("fai", "iI")
	__fail = step("fail", "lL")
	__fl = step("fl", "lL")
	__flo = step("flo", "oO")
	__floo = step("floo", "oO")
	__floor = step("floor", "rR")
	__r = step("r", "rR")
	__re = step("re", "eE")
	__reg = step("reg", "gG")
	__rege = step("rege", "eE")
	__rel = step("rel", "lL")
	__rele = step("rele", "eE")
	__m = step("m", "mM")
	__mi = step("mi", "iI")
	__min = step("min", "nN")
	__ma = step("ma", "aA")
	__max = step("max", "xX")
	__space = step("space", " ")
	__negative = step("negative", "-")
	__variable_open = step("variable_open", "[")
	__variable = step("variable", "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 (),+-*/_!@#$%^&*.\\{}:;\"'=~`")
	__variable_close = step("variable_close", "]")
	__op = step("op", "+-*/")
	__comma = step("comma", ",")
	__paren_open = step("paren_open", "(")
	__paren_close = step("paren_close", ")")

	# The state machine and what's an acceptable state to go to given your current state
	# Based on the triggers for a given state create specific paths and allowable equations
	# TODO figure out if there's a way to make this only allow solvable equations
	# some validation is required after stepping through the state machine
	__machine["unknown"] = [ __digit, __c, __d, __f, __r, __m, __space, __negative, __variable_open, __paren_open ]
	__machine["negative"] = [ __digit, __paren_open ]
	__machine["digit"] = [ __digit, __c, __d, __f, __r, __m, __space, __op, __paren_close, __comma, __variable_open ]
	__machine["space"] = [ __digit, __c, __d, __f, __r, __m, __space, __op, __paren_open, __paren_close, __comma, __variable_open, __negative ]
	__machine["op"] = [ __digit, __c, __d, __f, __r, __m, __space, __paren_open, __variable_open ]
	__machine["comma"] = [ __digit, __c, __d, __f, __r, __m, __space, __paren_open, __variable_open, __negative ]

	__machine["c"] = [ __ch, __cl, __cr ]
	__machine["ch"] = [ __c, __digit, __space, __op, __paren_open, __comma, __variable_open ]
	__machine["cl"] = [ __c, __digit, __space, __op, __paren_open, __comma, __variable_open ]
	__machine["cr"] = [ __cri ]
	__machine["cri"] = [ __crit ]
	__machine["crit"] = [ __f, __digit, __space, __op, __paren_open, __comma, __variable_open ]
	__machine["d"] = [ __d, __d_mod, __c, __f, __digit, __space, __op, __paren_open, __comma, __variable_open ]
	__machine["d_mod"] = [ __space, __op, __paren_open, __comma, __variable_open ]
	__machine["f"] = [ __fa, __fl ]
	__machine["fa"] = [ __fai ]
	__machine["fai"] = [ __fail ]
	__machine["fail"] = [ __digit, __space, __op, __paren_open, __comma, __variable_open ]
	__machine["fl"] = [ __flo ]
	__machine["flo"] = [ __floo ]
	__machine["floo"] = [ __floor ]
	__machine["floor"] = [ __digit, __space, __op, __paren_open, __comma, __variable_open ]
	__machine["r"] = [ __re ]
	__machine["re"] = [ __reg, __rel ]
	__machine["reg"] = [ __rege ]
	__machine["rege"] = [ __digit, __space, __op, __paren_open, __comma, __variable_open ]
	__machine["rel"] = [ __rele ]
	__machine["rele"] = [ __digit, __space, __op, __paren_open, __comma, __variable_open ]
	__machine["m"] = [ __mi, __ma ]
	__machine["mi"] = [ __min ]
	__machine["min"] = [ __digit, __space, __op, __paren_open, __comma, __variable_open ]
	__machine["ma"] = [ __max ]
	__machine["max"] = [ __digit, __space, __op, __paren_open, __comma, __variable_open ]

	__machine["paren_open"] = [ __digit, __c, __d, __f, __r, __m, __space, __negative, __variable_open, __paren_open ]
	__machine["paren_close"] = [ __c, __d, __f, __r, __m, __space, __op, __comma, __variable_open, __paren_close ]

	__machine["variable_open"] = [ __variable ]
	__machine["variable"] = [ __variable, __variable_close ]
	__machine["variable_close"] = [ __digit, __c, __d, __f, __r, __m, __space, __op, __variable_open, __paren_close, __comma ]


	def __init__(self, depth=0):
		self.state = "unknown" # Need to be able to track the state as you walk through an equation
		self.value = None # Tokens that require multiple characters you need a place to temporarily store the partial value
		self.parsed = [] # Output variable
		self.depth = depth # Tracks the depth of expressions inside (), only needed to correctly comma expand


	# Take the equation one character at a time
	# The gist of the algorithm is to record a token when the state changes from the previous state
	# note that you need to do a final check outside this function to grab the last character and
	# perform some validation
	# 
	# State is the previous state and feed is the new state
	def handle(self, feed):
		for a in self.__machine[self.state]:
			if feed in a["trigger"]:
				if self.state == "unknown":
					self.value = feed
				elif self.state in ["digit"] and a["step"] in ["digit"] or \
					self.state in ["variable"] and a["step"] in ["variable"] or \
					self.state in ["d"] and feed == "%" or \
					self.state in [ "c", "cr", "cri", "f", "fa", "fai", "fl", "flo", "floo", "r", "re", "reg", "rel", "m", "mi", "ma" ]:
					self.value = self.value + feed
				else:
					self.parsed.append(token(self.value, self.state, self.depth))
					if self.value in [")"]:
						self.depth -= 1
					self.value = feed

				self.state = a["step"]

				if feed in ["("]:
					self.depth += 1
				break
		else:
			toss(feed)


def Tokenize(equation, depth=0):
	t = Tokenizer(depth)

	# TODO add statemachine code to handle this
	equation = equation.replace("//","/")
	for a in equation:
		t.handle(a)
	if t.value != None:
		t.parsed.append(token(t.value, t.state, t.depth))
		if t.parsed[-1]["type"] == "paren_close":
			t.depth -= 1
	if t.depth != depth:
		toss("depth issue")
	if t.parsed[-1]["type"] in ["unknown", "negative", "paren_open", "variable", "variable_open", "comma", "op"]:
		toss(t.parsed[-1]["value"])
	
	return [a for a in t.parsed if (not (a["type"] in ["variable_open", "variable_close", "space"]) and not (a["value"] in ["floor"]))]


class Interpreter():
	def __init__(self, tokens, variables):
		self.tokens = tokens # Local copies of the parameters for the equation
		self._variables = variables
		self.history = [] # Output variable


	# given an array grab the elements that are to the left and right of a given location
	def __getLR(self, index, offsetLeft, offsetRight):
		return self.tokens[0: index - offsetLeft], self.tokens[index + 1 + offsetRight:]


	# Designed to iterate over the elements looking for a specific item
	# to evaluate
	def whileeach(self, func, save=True, incifdirty=False):
		dirty = False
		i = 0
		while i < len(self.tokens):
			temp_dirty = func(i, self.tokens[i])
			if temp_dirty and incifdirty:
				i += 1
			dirty = temp_dirty or dirty
			i += 1
		if save and self.tokens != self.history[-1]:
			self.history.append(self.tokens)
		return dirty

	
	def handler_purge_identities(self, i, v):
		if 0 < i < i + 1 < len(self.tokens) and [x["type"] for x in self.tokens[i - 1: i + 2]] == ["digit", "op", "digit"]:
			[x, op, y] = self.tokens[i - 1: i + 2]
			if op["value"] == "+" and int(x["value"]) == 0 or int(y["value"]) == 0:
				return self._add(i, v)
			if op["value"] == "+" and int(y["value"]) == 0:
				return self._sub(i, v)
			if op["value"] == "*" and int(x["value"]) == 1 or int(y["value"]) == 1:
				return self._mul(i, v)
			if op["value"] == "/" and int(y["value"]) == 1:
				return self._div(i, v)
		return False

	def handler_comma_expansions(self, i, v):
		if v["type"] == "comma":
			return self.handleCommaExpansions(i, v)
		return False

	def handler_negative_sign(self, i, v):
		if i + 1 < len(self.tokens) and [x["type"] for x in self.tokens[i: i + 2]] == ["negative", "digit"]:
			left, right = self.__getLR(i, 0, 1)
			middle = [token(str(-1 * int(self.tokens[i + 1]["value"])), "digit", self.tokens[i + 1]["depth"])]
			self.tokens = left + middle + right
			return True
		return False

	# TODO this works better if this also pushes the i forward
	def handler_variable_substitution(self, i, v):
		dirty = False
		if len(self.tokens) >= 1 and i < len(self.tokens) and v["type"] == "variable":
			left, right = self.__getLR(i, 0, 0)
			middle = Tokenize(str(self._variables[v["value"]]), v["depth"] + 1)
			for i2, v2 in enumerate(middle):
				if middle[i2]["type"] == "variable" and middle[i2]["value"] in v["stack"]:
					break
				elif v2["type"] == "variable":
					middle[i2]["stack"] = v["stack"] + [v["value"]]
			else:
				self.tokens = left + [token("(", "paren_open", v["depth"] + 1)] + middle + [token(")", "paren_close", v["depth"] + 1)] + right
				dirty = True
		return dirty

	def handler_number_in_parenthesis(self, i, v):
		if len(self.tokens) > 1 and i < len(self.tokens) and [x["type"] for x in self.tokens[i - 1: i + 2]] == ["paren_open", "digit", "paren_close"]:
			left, right = self.__getLR(i, 1, 1)
			middle = copy.deepcopy([v])
			middle[0]["depth"] -= 1
			self.tokens = left + middle + right
			return True
		return False

	def handler_standalone_arrays(self, i, v):
		if len(self.tokens) >= 1 and i < len(self.tokens) and v["type"] == "array" and (i + 1 >= len(self.tokens) or (i + 1 < len(self.tokens) and not self.tokens[i + 1]["type"] in ["letter"])):
			left, right = self.__getLR(i, 0, 0)
			self.tokens = left + [token(str(sum(v["value"])), "digit", v["depth"])] + right
			return True
		return False

	def handler_min_max(self, i, v):
		if v["value"] in ["max", "min"] and i + 5 <= len(self.tokens) and [x["type"] for x in self.tokens[i + 1: i + 6]] == ["paren_open", "digit", "comma", "digit", "paren_close"]:
			left, right = self.__getLR(i, 0, 5)
			middle = [token(str(eval("".join([str(self.tokens[i + z]["value"]) for z in range(0, 6)]))), "digit", v["depth"])]
			self.tokens = left + middle + right
			return True
		return False

	def getXLeft(self, i, left):
		if i - 1 < 0: # if start with d
			return 1, []
		elif self.tokens[i - 1]["value"] in ["(", ",", "+", "-", "*", "/"]: # if the beginning of an expression
			return 1, self.tokens[0: i]
		elif self.tokens[i - 1]["type"] == "digit":
			return int(self.tokens[i - 1]["value"]), left
		elif self.tokens[i - 1]["type"] == "array":
			return sum(self.tokens[i - 1]["value"]), left
		return None, left

	def handler_d_mod(self, i, v): 
		if v["value"] == "d%":
			left, right = self.__getLR(i, 1, 0)
			x, left = self.getXLeft(i, left)
			middle = [token(sorted([random.randint(0, 99) for a in range(0, x)]), "array", v["depth"])]
			middle[0]["range"] = (0, 99)
			middle[0]["fail"] = middle[0]["value"][0] == 99
			middle[0]["double"] = (middle[0]["value"][0] % 11 == 0)
			middle[0]["crit"] = middle[0]["value"][0] == 0
			self.tokens = left + middle + right
			return True
		return False

	def handler_d(self, i, v):
		if v["value"] == "d":
			left, right = self.__getLR(i, 1, 1)
			x, left = self.getXLeft(i, left)
			y = None
			if i + 1 >= len(self.tokens): # if end with d
				y = 1
				right = []
			elif self.tokens[i + 1]["type"] in ["d", "ch", "cl", "crit", "fail", "rege", "rele"] or self.tokens[i + 1]["value"] in "),+-*/": # if the end of an expression
				y = 1
				right = self.tokens[i + 1:]
			elif self.tokens[i + 1]["type"] == "digit":
				y = int(self.tokens[i + 1]["value"])

			if x != None and y != None:
				middle = [token(sorted([random.randint(1, y) for a in range(0, x)]), "array", v["depth"])]
				middle[0]["range"] = (x, y)
				if middle[0]["range"] == (1, 20):
					middle[0]["fail"] = middle[0]["value"][0] == 1
					middle[0]["crit"] = middle[0]["value"][0] == 20
				self.tokens = left + middle + right
				return True
		return False

	def isAfterArray(self, i, v, value):
		return v["value"] == value and self.tokens[i - 1]["type"] == "array"

	def x_implicit_y(self, i, y, right):
		if i + 1 < len(self.tokens) and self.tokens[i + 1]["value"].isdigit():
			return self.tokens[i - 1]["value"], int(self.tokens[i + 1]["value"]), right
		else:
			return self.tokens[i - 1]["value"], y, self.tokens[i + 1:]

	def handler_ch_cl(self, i, v, rang):
		left, right = self.__getLR(i, 1, 1)
		x, y, right = self.x_implicit_y(i, 1, right)
		middle = [token(rang(x, y), "array", v["depth"])]
		middle[0]["range"] = self.tokens[i - 1]["range"]
		self.tokens = left + middle + right

	def handler_ch(self, i, v):
		if self.isAfterArray(i, v, "ch"):
			self.handler_ch_cl(i, v, lambda x, y: x[-y:])
			return True
		return False

	def handler_cl(self, i, v):
		if self.isAfterArray(i, v, "cl"):
			self.handler_ch_cl(i, v, lambda x, y: x[:y])
			return True
		return False

	def handler_crit_fail(self, i, v, name, other_name, default, comp):
		left, right = self.__getLR(i, 1, 1)
		x, y, right = self.x_implicit_y(i, default, right)
		if not (0 < y <= self.tokens[i - 1]["range"][1]):
			toss(name + " range")
		middle = [token(x, "array", v["depth"])]
		if self.tokens[i - 1]["range"][0] == 1:
			middle[0][name] = comp(x[-1], y)
			if other_name in self.tokens[i - 1]:
				middle[0][other_name] = self.tokens[i - 1][other_name]
		middle[0]["range"] = self.tokens[i - 1]["range"]
		self.tokens = left + middle + right

	def handler_crit(self, i, v):
		if self.isAfterArray(i, v, "crit"):
			if self.tokens[i - 1]["range"] == (0, 99):
				left, right = self.__getLR(i, 1, 1)
				x, y, right = self.x_implicit_y(i, 0, right)
				middle = self.tokens[i - 1]
				self.tokens = left + middle + right
			else:
				self.handler_crit_fail(i, v, "crit", "fail", self.tokens[i - 1]["range"][1], lambda x, y: x >= y)
			return True
		return False

	def handler_fail(self, i, v):
		if self.isAfterArray(i, v, "fail"):
			if self.tokens[i - 1]["range"] == (0, 99):
				left, right = self.__getLR(i, 1, 1)
				x, y, right = self.x_implicit_y(i, 0, right)
				middle = self.tokens[i - 1]
				self.tokens = left + middle + right
			else:
				self.handler_crit_fail(i, v, "fail", "crit", 1, lambda x, y: x <= y)
			return True
		return False

	def handler_re(self, i, v, comp, rando):
		left, right = self.__getLR(i, 1, 1)
		x, y, right = self.x_implicit_y(i, 1, right)
		for i2, v2 in enumerate(x):
			if comp(int(v2), y):
				x[i2] = rando(1, y, self.tokens[i - 1]["range"][1])
		middle = [{"value": sorted(x), "type": "array", "range": self.tokens[i - 1]["range"], "depth": v["depth"]}]
		middle[0]["range"] = self.tokens[i - 1]["range"]
		self.tokens = left + middle + right

	def handler_rele(self, i, v):
		if self.isAfterArray(i, v, "rele"):
			self.handler_re(i, v, lambda x, y: x <= y, lambda a, b, c: random.randint(b + 1, c))
			return True
		return False

	def handler_rege(self, i, v):
		if self.isAfterArray(i, v, "rege"):
			self.handler_re(i, v, lambda x, y: x >= y, lambda a, b, c: random.randint(a, b - 1))
			return True
		return False

	def _op(self, i, v, o):
		if v["type"] == "op" and v["value"] in o and self.tokens[i - 1]["type"] == "digit" and self.tokens[i + 1]["type"] == "digit":
			left, right = self.__getLR(i, 1, 1)
			[x, op, y] = [str(z["value"]) for z in self.tokens[i - 1: i + 2]]
			middle = [token(eval(x + op + y), "digit", v["depth"])]
			self.tokens = left + middle + right
			return True
		return False

	def _add(self, i, v):
		return self._op(i, v, "+")

	def _sub(self, i, v):
		return self._op(i, v, "-")

	def _mul(self, i, v):
		return self._op(i, v, "*")

	def _div(self, i, v):
		return self._op(i, v, "/")


	def scan(self):
		self.history.append(self.tokens)
		dirty = False

		while len(self.tokens) > 0:
			dirty = self.whileeach(self.handler_purge_identities, save=False) or dirty
			dirty = self.whileeach(self.handler_comma_expansions) or dirty
			dirty = self.whileeach(self.handler_negative_sign) or dirty
			dirty = self.whileeach(self.handler_variable_substitution, incifdirty=True) or dirty
			if not dirty:
				break
			else:
				dirty = False

		while len(self.tokens) > 0:
			dirty = self.whileeach(self.handler_number_in_parenthesis, save=False) or dirty
			if not dirty:
				break
			else:
				dirty = False

		# This loops is meant for making passes through the equation
		# since solving the equation can't be handled in one step
		# loops inside are meant to handle the precedence of different operators
		while len(self.tokens) > 0:
			dirty = self.whileeach(self.handler_comma_expansions) or dirty
			dirty = self.whileeach(self.handler_negative_sign) or dirty
			dirty = self.whileeach(self.handler_number_in_parenthesis, save=False) or dirty
			dirty = self.whileeach(self.handler_min_max, save=False) or dirty
			for a in [ self.handler_d, self.handler_d_mod, self.handler_ch, self.handler_cl, self.handler_crit, self.handler_fail, self.handler_rele, self.handler_rege]:
				dirty = self.whileeach(a) or dirty
			for a in [ self._mul, self._div, self._add, self._sub ]:
				dirty = self.whileeach(a, save=False) or dirty
			dirty = self.whileeach(self.handler_standalone_arrays) or dirty
			if not dirty:
				break
			else:
				dirty = False

		return self.history


	def printResults(self, tokens):
		return "".join([x["value"] for x in tokens])


	# The algorithm for comma expansion takes the expression with commas between parenthesis (assuming the deepest level)
	# and then splits equation into 5 parts
	#	1 - the equation from beginning to the comma at the 2nd deepest point 
	#	2 - the equation from the comma at the 2nd deepest point to the left parenthesis
	#	3 - the expression in the parenthesis with the deepest commas
	#	4 - the equation from the right parenthesis to the comma at the 2nd deepest point
	#	5 - the equation from the comma at the 2nd deepest point to the end
	#
	# Example: 1, 2 + (3, 4) + 5, 6 > [1], [2 +], [3, 4], [+ 5], [6]
	# part 1: is only repeated once
	# part 2: is repeated for each comma separated item in part 3
	# part 3: is split by comma then iterated over and concatenated to part 2 and part 4
	# part 4: is repeated for each comma separated item in part 3
	# part 5: is only repeated once
	def handleCommaExpansions(self, i, v):
		if v["depth"] < max([z["depth"] for z in self.tokens if z["type"] == "comma"]):
			return False
		if v["depth"] <= 0:
			return False
		if self.tokens[i - 1]["value"] == ")" and self.tokens[i + 1]["value"] == "(":
			return False
		left, right = self.__getLR(i, 0, 0)
		leftmost = rightmost = leftcomma = rightcomma = i

		def search(items, index, value, condition):
			for i, v in enumerate(items):
				if condition(v, value):
					return i
			return index

		left.reverse()
		for i2, v2 in enumerate(left):
			if v2["type"] == "paren_open":
				if i2 + 1 < len(left) and left[i2 + 1]["value"] in ["max", "min"]:
					return False
		leftmost = search(left, i, v, lambda x, y: x["depth"] < y["depth"])
		leftcomma = search(left, i, v, lambda x, y: x["type"] in ["paren_open", "comma"] and x["depth"] == y["depth"] - 1)
		left.reverse()
		rightmost = search(right, len(self.tokens) - i - 1, v, lambda x, y: x["depth"] < y["depth"])
		rightcomma = search(right, len(self.tokens) - 1, v, lambda x, y: x["type"] in ["paren_close", "comma"] and x["depth"] == y["depth"] - 1)

		leftcommon = self.tokens[0: i - leftcomma]
		left = self.tokens[i - leftcomma: i - leftmost]
		middle = self.splitByComma(self.tokens[i - leftmost + 1: i + rightmost])
		right = self.tokens[i + rightmost + 1: i + rightcomma + 1]
		rightcommon = self.tokens[i + rightcomma + 1:]
		#print "searching away from ',' at", i, ":"
		#print "^ to ,", i - leftcomma, ":", leftcommon
		#print ", to (", i - leftmost, ":", left
		#print "( to )", i, ":", middle
		#print ") to ,", i + rightmost, ":", right
		#print ", to $", i + rightcomma, ":", rightcommon
		#print

		buff = []
		buff = buff + leftcommon
		for a in middle:
			buff = buff + left + [token("(", "paren_open", v["depth"])] + a + [token(")", "paren_close", v["depth"])] + right + [token(",", "comma", v["depth"] - 1)]
		buff = buff[:-1] + rightcommon
		#print "".join(map(lambda x: x["value"], buff))
		#print buff

		self.tokens = buff
		return True


	# Since we have custom dicts, we need a custom search function to search on a particular property
	def findComma(self, toSearch):
		for i, v in enumerate(toSearch):
			if v["type"] == "comma":
				return i
		return -1


	# Since we have custom dicts, we need to split on a particular property
	def splitByComma(self, middle):
		index = self.findComma(middle)
		if index == -1:
			return [middle]
		return [middle[: index]] + self.splitByComma(middle[index + 1:])


def toss(args):
	raise NameError(inspect.stack()[1][3] + ": ran into bad token: " + str(args))


def EvalRoll(equation, variables):
	parsed = Tokenize(equation)
	#print equation
	#print [x["value"] for x in parsed]
	currentTokens = Interpreter(parsed, variables)
	return currentTokens.scan()

################################################################################

# TODO deprecate
def ValueToString(t):
	out = [str(t["value"])]
	if t["type"] in ["variable"]:
		out = ["["] + out + ["]"]
	elif t["type"] in ["comma"]:
		out = out + [" "]
	elif "double" in t and t["double"]:
		out = out + ["'double'"]
	elif "crit" in t and t["crit"]:
		out = out + ["'crit'"]
	elif "fail" in t and t["fail"]:
		out = out + ["'crit fail'"]
	return "".join(out)


# The original roller was string based instead of object based
# this function takes the objects and converts them to the equivalent strings
# TODO deprecate
def ConvertToOldStyle(history):
	out = []
	for i, a in enumerate(history):
		temp_line = {"Step": "".join(map(ValueToString, a))}
		if (i == 0 or (i == len(history) - 1 and temp_line != out[-1]) or (temp_line != out[-1] and ("[" in temp_line["Step"] or "d" in temp_line["Step"] or "c" in temp_line["Step"] or "a" in temp_line["Step"] or "m" in temp_line["Step"]))):
			out.append(temp_line)
	return {"Base History": out}


# The old interface from the previous version,
# kept around for the quick turnaround in integrating this version
# TODO deprecate
class DiceRoller():
	def __init__(self):
		self._CONTAINS_SQUARE_BRACKETS = re.compile(r'(\[[^\[\]]*\])')


	# used as a method of reducing the number of widgets needed to solve an equation
	# and lighten the network load
	def getNeededWidgets(self, expr, widgets, callee_stack = "|"):
		compress_dict = {}
		for a in self._CONTAINS_SQUARE_BRACKETS.findall(expr):
			a_str = str(a)
			compress_dict[a[1: -1]] = widgets[a_str[1: -1]]
			compress_dict.update(self.getNeededWidgets(compress_dict[a_str[1: -1]], widgets, callee_stack + a_str[1: -1] + "|"))
		return compress_dict


	def checkRoll(self, equation="1d20", widgets={}):
		return ConvertToOldStyle(EvalRoll(equation, widgets))
