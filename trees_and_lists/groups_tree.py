import logging, os

__QtLib__ = os.environ['QT_API']
if __QtLib__ == 'pyqt':
    from PyQt5.QtCore import Qt, QModelIndex
else:
    from PySide.QtCore import Qt, QModelIndex

from .base_tree_model import TreeModel




# Simplifies a TreeModel into a GroupModel.
# Qt Supplies a GroupModel, but this was easier to do since I have so much exp with Groups.
# Also, we can be a bit more flexible with some things this way.
class GroupModel(TreeModel):
    def __init__(self, headers, editable=False, item_colors=False, parent=None):
        TreeModel.__init__(self, headers, editable, item_colors, parent)

        if __QtLib__ != 'pyqt':
            self.setSupportedDragActions(Qt.MoveAction)


    if __QtLib__ == 'pyqt':
        def supportedDragActions(self):
            return Qt.MoveAction


    def add_item(self, item, fg_color=None, bg_color=None, parent=QModelIndex()):
        # items should be a Group of the cols to add to a single row
        if len(item) == self.rootItem.columnCount():
            return self.insert_row_to_end(item, fg_color=fg_color, bg_color=bg_color, parent=parent)
        return False


    def add_replace_item(self, item, fg_color=None, bg_color=None, parent=QModelIndex()):
        if len(item) == self.rootItem.columnCount():
            key = ['*' for row in range(self.rootItem.columnCount())]
            key[0] = item[0]
            replacement = self.get_item_index(key, parent=parent)
            if not replacement.isValid():
                return self.add_item(item, fg_color=fg_color, bg_color=bg_color, parent=parent)

            success = self.setRowData(replacement, item)
            if success:
                rep_item = self.getItem(replacement)
                rep_item.set_background(bg_color)
                rep_item.set_foreground(fg_color)
                return True
        return False


    def remove_item(self, item, parent=QModelIndex()):
        # items should be a Group of the cols to add to a single row
        rm_index = self.get_item_index(item, parent=parent)

        if rm_index.isValid():
            return self.remove_row(rm_index.row(), parent=parent)
        return False


    # Turn the Group into a dictionary, with each value == {first_col:[other_cols]}
    # Is only used by Widget Group, which optimizes the Group a bit
    def get_group_dict(self, parent=QModelIndex()):
        group_dict = {}
        parent_item = self.getItem(parent)
        for row_num in range(parent_item.child_count()):
            row_item = parent_item.child(row_num)
            row_data = row_item.all_data()
            if row_item.child_count() > 0:
                child_data = self.get_group_dict(self.index(row_num, 0, parent=parent))
                group_dict[row_data[0]] = child_data
            else:
                group_dict[row_data[0]] = row_data[1:] or []
            if self.item_colors:
                group_dict[row_data[0]].append(self.bgColor)
        return group_dict


    def set_from_group_dict(self, group_dict):
        self.clear_rows()
        self.add_to_list_from_dict(group_dict)


    def add_to_list_from_dict(self, group_dict, duplicates_behavior=None, parent=QModelIndex()):
        # duplicates_behavior defaults to None, can be 'Overwrite' or 'Ignore'
        for key in group_dict:
            if duplicates_behavior == 'Ignore':
                if self.item_exists([key, '*'], parent=parent):
                    continue
            elif duplicates_behavior == 'Overwrite':
                existing_index = self.get_item_index([key, '*'], parent)
                if existing_index.isValid():
                    existing_item = self.getItem(existing_index)
                    new_row = [key]
                    if isinstance(group_dict[key], dict):
                        new_row.append('')
                        if not self.add_to_list_from_dict(group_dict[key], duplicates_behavior, existing_index):
                            return False
                    else:
                        new_row.append(group_dict[key])
                    existing_item.setAllData(new_row)
                    continue

            if isinstance(group_dict[key], dict):
                success = self.add_item([key, ''], parent=parent)
                if success:
                    row = self.index(self.getItem(parent).child_count() - 1, 0, parent)
                    if not self.add_to_list_from_dict(group_dict[key], duplicates_behavior, row):
                        return False
                else:
                    logging.info("add_to_list_from_dict: Big fail")
            else:
                # if not self.add_item(item, parent=parent):
                logging.info("add_to_list_from_dict: input item wasn't a dictionary")
                return False
        return True


    def flags(self, index):
        return Qt.ItemIsDropEnabled | Qt.ItemIsDragEnabled | TreeModel.flags(self, index)
