import os

__QtLib__ = os.environ['QT_API']
if __QtLib__ == 'pyqt':
    from PyQt5.QtCore import pyqtSignal as Signal
    from PyQt5.QtWidgets import QToolButton
else:
    from PySide.QtCore import Signal
    from PySide.QtGui import QToolButton




# Adds an identifier to the QToolButton so we know which line to remove when the Remove button is clicked
class builderRemoveButton(QToolButton):
    deleteLine = Signal(str)

    def __init__(self, parent=None):
        QToolButton.__init__(self, parent)
        self.ident = ''
        self._setup_connections()


    def set_ident(self, id):
        self.ident = id


    def _setup_connections(self):
        self.clicked.connect(self.emitDelete)


    def emitDelete(self):
        self.deleteLine[str].emit(self.ident)
