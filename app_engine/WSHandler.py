import json
import logging
import sys
import traceback
import webapp2
from google.appengine.ext import ndb

from CommonFunctions import *
from CounterModel import *
from GMModel import *
from UserModel import *


'''
						POST		GET			PUT				DELETE
/collection				make new	get all		bulk update		delete all
/collection/item		none		get one		update/error	delete one
'''

class WSHandler(webapp2.RequestHandler):
	_key_gm = lambda self, out: "gm_{0}".format(out["session"])
	_key_user = lambda self, out: "user_{0}".format(out["guid"])
	_key_users = lambda self, out: "users_{0}".format(out["session"])
	_key_counter_model = lambda self, out: "counter_model_{0}".format(out["session"])

	_dev_msg = lambda self, msg: {"dev_message": msg}

	_query_gm = lambda self, out: GetQuery(self._key_gm(out), "SELECT * FROM GMModel WHERE session = :1", (out["session"],))
	_query_user = lambda self, out: GetQuery(self._key_user(out), "SELECT * FROM UserModel WHERE guid = :1 AND session = :2", (out["guid"], out["session"],))
	_query_users = lambda self, out: FetchQuery(self._key_users(out), "SELECT * FROM UserModel WHERE session = :1", (out["session"],))

	_cmp_usermodels = lambda self, luser, ruser: luser.guid == ruser.guid

	_BASE_PARAMS = [ ("guid", ""), ("name", ""), ("session", "default") ]
	_GM_PARAMS = _BASE_PARAMS + [ ("new_gm", "") ]
	_USER_PARAMS = _BASE_PARAMS + [ ("is_from", ""), ("white", "*"), ("black", ""), ("message", ""), ("color", "") ]

	def setupHelper(self, params):
		self.response.headers['Content-Type'] = 'application/json'

		out = {}
		for a in params:
			if a[0] in self.request.params:
				out[a[0]] = self.request.params[a[0]]
			else:
				out[a[0]] = a[1]
		return out
	
	def _errorOut(self, msg, code):
		logging.info(msg)
		logging.info("Traceback: {0}".format(traceback.format_exc()))
		self._writejson(msg)
		self.response.set_status(code)

	def _writejson(self, msg):
		self._write(json.dumps(msg))

	def _write(self, msg):
		self.response.write(msg)

	@staticmethod
	def _try_decorator(func):
		def w(self, *args, **kargs):
			try:
				func(self, *args, **kargs)
			except Exception as e:
				self._errorOut(e.message, 500)
		return w

class WSHandlerUsers_Base(WSHandler):
	_get_gm_users = lambda self, out: [z(out) for z in [self._query_gm, self._query_user, self._query_users]]
	_get_gm_and_user = lambda self, out: [z(out) for z in [self._query_gm, self._query_user]]
	_get_query_user = lambda self, query: GetQuery("user_{0}".format(query), "SELECT * FROM UserModel WHERE guid = :1", (query,))
	_is_gm = lambda self, gm, user: gm and user and self._cmp_usermodels(gm, user)
	
	
	def __setup__(self):
		return self.setupHelper(self._USER_PARAMS)

	def _SetName(self, out, proposed_names):
		for a in xrange(0, sys.maxint):
			proposed_name = proposed_names + (" {0}".format(a) if a else "")
			if None == ndb.gql("SELECT * FROM UserModel WHERE session = :1 AND name = :2", out["session"], proposed_name).get():
				return proposed_name

class WSHandleGM_base(WSHandler):
	def __setup__(self):
		return self.setupHelper(self._GM_PARAMS)
