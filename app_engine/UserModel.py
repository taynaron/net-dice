#!/usr/bin/env python
from google.appengine.ext import ndb

"""
DataModel for tracking a connected user
"""
class UserModel(ndb.Model):
	"""
	State variable for a unique identifier for this user, note only visible to
	the GM and the current user
	"""
	guid = ndb.StringProperty()

	"""
	State variable for a name for the this user
	"""
	name = ndb.StringProperty()
	"""
	State variable for an alias for the current user
	TODO figure out if this is needed
	"""
	alias = ndb.StringProperty()

	"""
	State variable for tracking the color for the current user
	TODO use
	"""
	color = ndb.StringProperty()
	"""
	State variable for tracking who this user mutes
	TODO use
	"""
	mutes = ndb.StringProperty()

	"""
	State variable for associating this user with a session
	"""
	session = ndb.StringProperty()

	"""
	State variable for associating widgets with this user
	TODO use
	"""
	tags = ndb.StringProperty()
