import os

__QtLib__ = os.environ['QT_API']
if __QtLib__ == 'pyqt':
    from PyQt5.QtWidgets import QLineEdit
else:
    from PySide.QtGui import QLineEdit

from ui import drag_drop as dd




# Adds useful Drag and Drop functionality to the standard QLineEdit
class LineDropEdit(QLineEdit):
    def __init__(self, parent=None):
        QLineEdit.__init__(self, parent)
        self.setAcceptDrops(True)


    def dragEnterEvent(self, event):
        if event.mimeData().hasFormat(dd.DD_MIME_ROLLS) or event.mimeData().hasText():
            event.acceptProposedAction()


    def dragMoveEvent(self, event):
        self.dragEnterEvent(event)


    def dropEvent(self, drop_event):
        drop = drop_event.mimeData()

        pos = self.cursorPositionAt(drop_event.pos())

        if drop.hasFormat(dd.DD_MIME_ROLLS):
            drop_event.acceptProposedAction()
            dict_val = 'Roll Name'
            new_str = self.parse_dict(drop.rolls(), dict_val, widgetize=True, pos=pos)
            self.setText(new_str)

        else:
            QLineEdit.dropEvent(self, drop_event)


    def add_widget_names(self, roll_list):
        roll_list = self._list_to_widget_names(roll_list)

        new_str = self._get_new_text(roll_list)
        self.setText(new_str)


    def _list_to_widget_names(self, roll_list):
        for roll_num in range(len(roll_list)):
            roll_list[roll_num] = '[' + roll_list[roll_num] + ']'

        return roll_list


    def _get_new_text(self, roll_list, base_txt=None, pos=0):
        if not base_txt:
            base_txt = str(self.text())

        dict_str = ", ".join(roll_list)

        if base_txt:
            if pos > 0:
                dict_str = ", " + dict_str
            if pos < len(base_txt) - 1:
                dict_str = dict_str + ", "
            dict_str = base_txt[:pos] + dict_str + base_txt[pos:]

        return dict_str


    def parse_dict(self, dicts, dict_val, base_txt=None, widgetize=False, pos=0):
        if not dicts:
            return

        if pos is None:
            pos = self.cursorPosition()

        dict_list = []

        for dict in dicts:
            dict_list.append(dict[dict_val])

        if widgetize:
            dict_list = self._list_to_widget_names(dict_list)

        return self._get_new_text(dict_list, base_txt, pos)
