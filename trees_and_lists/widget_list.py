import logging, os

__QtLib__ = os.environ['QT_API']
if __QtLib__ == 'pyqt':
    from PyQt5.QtCore import QObject
    from PyQt5.QtWidgets import QFileDialog

else:
    from PySide.QtCore import QObject
    from PySide.QtGui import QFileDialog

from .base_tree_model import TreeSort
from .widget_list_model import WidgetModel
import file_IO

from ui.ui_mods import ui_helpers
import net_dice_helpers as nd_helpers




# How Roller_UI interacts with the widget list.
class WidgetList(QObject):
    def __init__(self, UI, prefs_dict={}):
        QObject.__init__(self)
        self.ui = UI
        self.widget_list = WidgetModel()
        self.widget_sort = TreeSort(source_model=self.widget_list)

        self.default_file = "widgets.json"
        self.old_default_file = "widgets.txt"
        self.widget_file = self.default_file

        self.set_widget_info(prefs_dict)

        self._setup_connections()

        if not self.load_widgets():
            self.make_widgets()


    def _setup_connections(self):
        self.ui.widgetView.setModel(self.widget_sort)

        self.ui.actionSave_Widgets.triggered.connect(self.save_widgets_slot)
        self.ui.actionSave_Widgets_As.triggered.connect(self.save_widgets_as)
        self.ui.actionLoad_Widgets.triggered.connect(self.load_widgets_as)
        self.ui.actionImport_Additional_Widgets.triggered.connect(self.import_widgets)
        self.ui.actionImport_Widgets_From_CSV_File.triggered.connect(self.import_csv)
        self.ui.actionRevert_to_Default_Widgets.triggered.connect(self.check_make_widgets)

        self.widget_list.showMessage[str].connect(self.ui.statusBar.showMessage)


    def set_conn_mode(self, conn_mode):
        self.ui.widgetView.set_conn_mode(conn_mode)


    def get_widget_info(self):
        prefs_dict = {
            'Widget File': self.widget_file,
            'Widget Header Settings': str(self.ui.widgetView.header().saveState())
        }
        return prefs_dict


    def set_widget_info(self, prefs_dict):
        if 'Widget File' in prefs_dict:
            self.widget_file = prefs_dict['Widget File']

        if 'Widget Header Settings' in prefs_dict:
            file_IO.attrs_to_bytes(prefs_dict, ['Widget Header Settings'])
            self.ui.widgetView.header().restoreState(prefs_dict['Widget Header Settings'])


    def load_widgets_as(self):
        file_name = QFileDialog.getOpenFileName(caption="Load Widgets", directory=".", filter="JSON (*.json)")
        file_loaded = self.load_widgets(file_name)
        if file_loaded:
            self.widget_file = file_name


    def load_widgets_slot(self):
        self.load_widgets()


    def load_widgets(self, file_name=None):
        if not file_name:
            file_name = self.widget_file

        # sadly, can't pickle most Qt objects. We'll do this the long way.
        try:
            widgets_dict = file_IO.read_file(file_name)
        except FileNotFoundError:
            if self.widget_file == self.default_file:
                try:
                    widgets_dict = file_IO.read_file(self.old_default_file)
                    file_name = self.old_default_file
                except FileNotFoundError:
                    widgets_dict = False
            else:
                widgets_dict = False
        if widgets_dict is False:
            self.ui.statusBar.showMessage("Loading Widgets File '{0}' Failed.".format(file_name))
            return False

        is_saved = self.widget_list.set_from_widget_dict(widgets_dict)
        if is_saved:
            self.ui.statusBar.showMessage("Widgets File '{0}' Loaded.".format(file_name))
            return True
        return False


    def check_dict_dupes(self, comp_dict):
        for key in comp_dict:
            item = comp_dict[key]
            if isinstance(item, dict):
                if self.check_dict_dupes(item):
                    return True
            elif self.widget_list.item_exists([key, '*']):
                return True
        return False


    def import_widgets(self):
        file_name = QFileDialog.getOpenFileName(caption="Import Widgets", directory=".", filter="JSON (*.json)")

        # sadly, can't pickle most Qt objects. We'll do this the long way.
        widgets_dict = file_IO.read_file(file_name)
        if widgets_dict is False:
            self.ui.statusBar.showMessage("Loading Widgets File '{0}' Failed.".format(file_name))
            return False

        if self.check_dict_dupes(widgets_dict):
            message = "Duplicate widget names were found between your widgets and the proposed import file. What would you like to do with the duplicates?"
            buttons = ["Use My Widgets", "Do Not Import File", "Use Imported Widgets"]
            state = ui_helpers.generic_multibutton_dialog(message, buttons, parent=self.ui.centralwidget)

        if state == 0:
            is_saved = self.widget_list.add_to_list_from_dict(widgets_dict, duplicates_behavior="Ignore")
        elif state == 2:
            is_saved = self.widget_list.add_to_list_from_dict(widgets_dict, duplicates_behavior="Overwrite")
        else:
            return False

        if is_saved:
            self.ui.statusBar.showMessage("Widgets File '{0}' Loaded.".format(file_name))
            return True


    def import_csv(self):  # , file_name = None):
        import csv
        # if not file_name:
        file_name = QFileDialog.getOpenFileName(caption="Import Widgets From CSV", directory=".", filter="Text (*.csv)")

        widgets_csv = csv.reader(open(file_name, 'rU'))
        if widgets_csv is False:
            self.ui.statusBar.showMessage("Loading CSV File '{0}' Failed.".format(file_name))
            return False

        widgets_dict = {}
        # indent_lvl = 0
        parent_path = []
        curr_parent = widgets_dict
        keys = []
        for line in widgets_csv:
            text_pos = 0
            for i in line:
                if i != '':
                    break
                text_pos += 1

            if text_pos >= len(line):
                self.ui.statusBar.showMessage("Loading File Failed; error with line '{0}'.".format(line))
                return False

            if line[text_pos] in keys:
                self.ui.statusBar.showMessage("Loading File Failed; duplicate key '{0}'.".format(line[text_pos]))
                return False

            keys.append(line[text_pos])

            # line can start on any position <= len(parent_path)
            # < means shift back on parent path
            # == means add to current pos
            # if only 1 value, put it on the parent path
            if text_pos > len(parent_path):    # We're outside of the established tree
                self.ui.statusBar.showMessage("Loading File Failed; line '{0}' is improperly grouped.".format(line[text_pos:]))
                return False

            elif text_pos == len(parent_path):
                curr_parent = self._csv_line(parent_path, curr_parent, line, text_pos)

            else:  # We have to go back down the tree
                parent_path = parent_path[:text_pos]
                logging.info("change parent back: " + str(parent_path))
                curr_parent = widgets_dict
                for path_lvl in parent_path:
                    curr_parent = curr_parent[path_lvl]

                curr_parent = self._csv_line(parent_path, curr_parent, line, text_pos)

            logging.info(str(text_pos) + ", " + str(line))
        is_saved = self.widget_list.set_from_widget_dict(widgets_dict)
        if is_saved:
            self.ui.statusBar.showMessage("CSV Imported Successfully.")
        logging.info(widgets_dict)


    def _csv_line(self, parent_path, curr_parent, line, text_pos):
        curr_parent[line[text_pos]] = ''
        if len(line) < text_pos or line[text_pos + 1] == '':
            curr_parent[line[text_pos]] = {}
            parent_path.append(line[text_pos])
            curr_parent = curr_parent[line[text_pos]]
            logging.info("change parent to: " + str(line[text_pos]) + ", " + str(parent_path))
        else:
            logging.info("currently at: " + str(parent_path))
            curr_parent[line[text_pos]] = line[text_pos + 1]
        return curr_parent


    def save_widgets_as(self):
        file_name = QFileDialog.getSaveFileName(caption="Save Widgets", directory=".", filter="JSON (*.json)")
        if file_name and isinstance(file_name, tuple):
            file_name = file_name[0]

        if file_name:
            if not file_name.endswith('.json'):
                file_name += ".json"

            file_saved = self.save_widgets(file_name)
            if file_saved:
                self.widget_file = file_name
        else:
            self.ui.statusBar.showMessage("Couldn't Save Widgets File")


    def save_widgets_slot(self):
        self.save_widgets()


    def save_widgets(self, file_name=None):
        try:
            if not file_name or file_name == self.old_default_file:
                file_name = self.default_file
            elif file_name.endswith('.txt'):
                ary_name = file_name.split('.')
                ary_name[-1] = 'json'
                file_name = '.'.join(ary_name)

            if not file_name.endswith('.json'):
                file_name += ".json"

            self.ui.statusBar.showMessage("Saving Widgets File '{0}' Failed.".format(file_name))
            # put widgets into dict
            widgets_dict = self.widget_list.get_widget_dict()
            # save widgets out
            is_saved = file_IO.save_file(file_name, widgets_dict)
            if is_saved:
                self.ui.statusBar.showMessage("Widgets File '{0}' Saved.".format(file_name))
                return True
        except Exception as e:
            logging.info(str(e))
            self.ui.statusBar.showMessage("Couldn't Save Widgets File")
        return False


    def addWidget(self, roll_name, roll_txt):
        try:
            self.widget_list.check_widget_name(roll_name)
        except SyntaxError as err1:
            # if not valid and err1 != "Duplicate Widget Name":
            self.ui.statusBar.showMessage(err1)
            return False

        try:
            self.widget_list.check_widget_roll(roll_txt)
        except Exception as err2:
            self.ui.statusBar.showMessage(err2)
            return False

        status = self.widget_list.add_replace_item([roll_name.strip(), roll_txt.strip()])

        if status:
            self.ui.statusBar.showMessage('Widget "{0}" Created'.format(roll_name))
            return True
        self.ui.statusBar.showMessage("Widget Creation Failed")
        return False


    def check_make_widgets(self):
        message = "Are you sure you want to reset your widgets list?"
        buttons = ["Cancel", "Reset Widgets"]
        state = ui_helpers.generic_multibutton_dialog(message, buttons, parent=self.ui.centralwidget)

        if state == 1:
            self.make_widgets()


    def make_widgets(self):
        # makes a default list of widgets for the user to populate
        widget_file = "widget_files/pathfinder.json"
        widgets_dict = file_IO.read_file(nd_helpers.support_file_path(widget_file))

        if not widgets_dict:
            self.ui.statusBar.showMessage("Couldn't load widget file: " + widget_file)
            return

        is_saved = self.widget_list.set_from_widget_dict(widgets_dict)
        if is_saved:
            self.ui.statusBar.showMessage("New Widgets File Created.")


    def get_widget_dict(self):
        return self.widget_list.get_widget_dict()


    def get_flat_widget_dict(self):
        return self.widget_list.get_widget_dict(flatten=True)
