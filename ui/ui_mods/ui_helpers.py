import os

__QtLib__ = os.environ['QT_API']
if __QtLib__ == 'pyqt':
    from PyQt5.QtCore import Qt
    from PyQt5.QtWidgets import QMessageBox
else:
    from PySide.QtCore import Qt
    from PySide.QtGui import QMessageBox


def _to_mb_role(int_role):
    button_roles = [QMessageBox.AcceptRole, QMessageBox.RejectRole,
                    QMessageBox.DestructiveRole, QMessageBox.ActionRole,
                    QMessageBox.HelpRole, QMessageBox.YesRole, QMessageBox.NoRole,
                    QMessageBox.ApplyRole, QMessageBox.ResetRole, QMessageBox.InvalidRole]
    return button_roles[int_role]



def generic_ok_dialog(title, message, parent=None):
    dialog_box = QMessageBox.warning(parent, title, message, buttons=QMessageBox.StandardButtons(QMessageBox.Ok | QMessageBox.Cancel))
    if dialog_box == QMessageBox.Ok:
        return True
    return False
    # return dialog_box == QMessageBox.Ok


'''def generic_warning_dialog(title, message, parent=None):
    from PyQt5.QtWidgets import QMessageBox
    dialog_box = QMessageBox.warning(parent, title, message, buttons = QMessageBox.StandardButtons(QMessageBox.Ok))
    if dialog_box == QMessageBox.Ok:
        return True
    return False
'''

def generic_multibutton_dialog(message, button_name_list, parent=None):
    # creates a dialog with a bunch of buttons provided by button_name_list
    # returns the number of the clicked button
    # The first button will be the default button, the second the 'escape' option
    if parent:
        dialog_box = QMessageBox(QMessageBox.NoIcon, '', message, parent=parent)
        dialog_box.setWindowFlags(Qt.Dialog | Qt.MSWindowsFixedSizeDialogHint | Qt.Sheet)
        dialog_box.setWindowModality(Qt.WindowModal)
    else:
        dialog_box = QMessageBox()
        dialog_box.setText(message)

    button_list = []
    for button_num in range(len(button_name_list)):
        button_list.append(dialog_box.addButton(button_name_list[button_num], _to_mb_role(button_num)))

    dialog_box.setDefaultButton(button_list[0])

    dialog_box.exec_()
    clicked = dialog_box.clickedButton()

    for num in range(len(button_list)):
        if clicked == button_list[num]:
            return num
    return -1
